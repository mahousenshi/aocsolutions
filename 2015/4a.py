import hashlib

inp = 'ckczppom'
i = 0

while True:
    if hashlib.md5(f'{inp}{i}'.encode()).hexdigest().startswith('0' * 5):
        break
    i += 1

print(i)