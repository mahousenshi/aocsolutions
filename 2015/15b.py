from itertools import product
import re

with open('15.txt', 'r') as f:
    inp = f.readlines()

ingredients = []
props = ['capacity', 'durability', 'flavor', 'texture', 'calories']

for ingredient in inp:
    values = [int(n) for n in re.findall(r'-?\d+', ingredient)]
    ingredients.append({prop: val for prop, val in zip(props, values)})

props.pop()
maxscore = 0

for spoons in product(range(101), repeat=4):
    if sum(spoons) == 100:
        ingr_score = 0

        for i, ingr in enumerate(ingredients):
            ingr_score += spoons[i] * ingr['calories']

        if ingr_score == 500:
            score = 1

            for prop in props:
                ingr_score = 0

                for i, ingr in enumerate(ingredients):
                    ingr_score += spoons[i] * ingr[prop]

                if ingr_score > 0:
                    score *= ingr_score
                else:
                    score = 0
                    break

            maxscore = max(maxscore, score)

print(maxscore)