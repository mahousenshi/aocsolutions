inp = '1321131112'
old = inp

for _ in range(40):
    new = ''
    
    n = old[0]
    t = 1

    for d in old[1:]:
        if d == n:
            t += 1
        else:
            new += f'{t}{n}'
            n = d
            t = 1

    new += f'{t}{n}'
    old = new

print(len(old))