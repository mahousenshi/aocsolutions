from itertools import combinations

def battle(player, enemy):
    while True:
        # Player turn
        enemy['hp'] -= max(player['damage'] - enemy['armor'], 1)

        if enemy['hp'] <= 0:
            break

        # Enemy turn
        player['hp'] -= max(enemy['damage'] - player['armor'], 1)

        if player['hp'] <= 0:
            break

    return player['hp'] > enemy['hp']

with open('21.txt', 'r') as f:
    inp = f.readlines()

items = {}
min_cost = 0

with open('21items.txt', 'r') as f:
    for l in f.readlines():
        l = l.split()

        if len(l) == 1:
            item  = l[0].strip()

            items[item] = []
        else:
            items[item].append({
                'name': l[0],
                'cost': int(l[1]),
                'damage': int(l[2]),
                'armor': int(l[3])
            })

max_cost = 0

for weapon in items['weapons']:
    for armor in items['armors']:
        # No rings
        player = {
            'hp': 100,
            'damage': weapon['damage'],
            'armor': armor['armor']
        }

        enemy = {
            'hp': int(inp[0].split(':')[1]),
            'damage': int(inp[1].split(':')[1]),
            'armor': int(inp[2].split(':')[1])
        }

        if not battle(player, enemy):
            max_cost = max(max_cost, weapon['cost'] + armor['cost'])

        for ring1, ring2 in combinations(items['rings'], 2):
            player = {
                'hp': 100,
                'damage': weapon['damage'] + ring1['damage'] + ring2['damage'],
                'armor': armor['armor'] + ring1['armor'] + ring2['armor'],
            }

            enemy = {
                'hp': int(inp[0].split(':')[1]),
                'damage': int(inp[1].split(':')[1]),
                'armor': int(inp[2].split(':')[1])
            }

            if not battle(player, enemy):
                max_cost = max(max_cost, weapon['cost'] + armor['cost'] + ring1['cost'] + ring2['cost'])

print(max_cost)
