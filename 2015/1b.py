with open('1.txt', 'r') as f:
    inp = f.read()

floor = 0
for i, c in enumerate(inp, start=1):
    floor += 1 if c == '(' else -1

    if floor == -1:
        break

print(i)