import re

with open('8.txt', 'r') as f:
    inp = [s.strip() for s in f.readlines()]

total = 0

for string in inp:
    escaped = len(re.findall(r'\\\\|\\"', string))
    hexa = len(re.findall(r'\\x([0-9,a-f]{2})', string))

    total += 2 + escaped + 3*hexa

print(total)