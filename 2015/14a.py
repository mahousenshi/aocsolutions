import re

with open('14.txt', 'r') as f:
    inp = f.readlines()

distances = []

for reindeer in inp:
    velocity, awake, resting = [int(n) for n in re.findall(r'\d+', reindeer)]

    ticks, remainder = divmod(2503, awake + resting)
    
    if remainder > awake:
        distances.append(velocity * awake * (ticks + 1))
    else:
        distances.append(velocity * (awake * ticks + remainder))
    
print(max(distances))