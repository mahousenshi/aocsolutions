class Reindeers:
    def __init__(self, velocity, awake, resting):
        self.velocity = velocity
        self.awake = awake
        self.resting = resting

        self.timer = awake
        self.woke = 1
        self.distance = 0

        self.points = 0

    def tick(self):
        self.distance += self.velocity * self.woke

        self.timer -= 1
        if self.timer == 0:
            self.woke = not(self.woke)

            if self.woke:
                self.timer = self.awake
            else:
                self.timer = self.resting

    def addpoint(self):
        self.points += 1

import re

with open('14.txt', 'r') as f:
    inp = f.readlines()

reindeers = []

for reindeer in inp:
    velocity, awake, resting = [int(n) for n in re.findall(r'\d+', reindeer)]

    reindeers.append(Reindeers(velocity, awake, resting))

for step in range(2503):
    distances = []

    for reindeer in reindeers:
        reindeer.tick()
        distances.append(reindeer.distance)
    
    for reindeer in reindeers:
        if reindeer.distance == max(distances):
            reindeer.addpoint()

print(max([r.points for r in reindeers]))