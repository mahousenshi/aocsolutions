import re

my_sue = {
    'children': 3,
    'cats': 7,
    'samoyeds': 2,
    'pomeranians': 3,
    'akitas': 0,
    'vizslas': 0,
    'goldfish': 5,
    'trees': 3,
    'cars': 2,
    'perfumes': 1,
}

with open('16.txt', 'r') as f:
    inp = f.readlines()

for sue in inp:
    props = re.findall(r'(\w+)', sue)
    sue = {key: int(value) for key, value in zip(props[2::2], props[3::2])}

    if all([my_sue[key] == value for key, value in sue.items()]):
        print(props[1])
        break