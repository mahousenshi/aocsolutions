import re

with open('6.txt', 'r') as f:
    lines = [l.strip() for l in f.readlines()]

grid = [[0 for j in range(1000)] for i in range(1000)]

for inst in lines:
    command, ax, ay, bx, by = re.match(r'(.*) (\d+),(\d+) through (\d+),(\d+)', inst).groups()

    for x in range(int(ax), int(bx)+1):
        for y in range(int(ay), int(by)+1):
            if command == 'turn on':
                grid[x][y] = 1

            if command == 'turn off':
                grid[x][y] = 0
            
            if command == 'toggle':
                grid[x][y] = 0 if grid[x][y] else 1

print(sum(sum(line) for line in grid))