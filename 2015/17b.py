from itertools import combinations

with open('17.txt', 'r') as f:
    inp = f.readlines()

containers = sorted(list(map(int, inp)))

fit = []

for i in range(len(containers)):
    for container in combinations(containers, i+1):
        if sum(container) == 150:
            fit.append(container)

fit_min = min(map(len, fit))

print(len(list(filter(lambda x: len(x) == fit_min, fit))))