inp = 33100000
n = inp // 10

houses = [0 for _ in range(n)]

for elf in range(1, n):
    for h in range(elf, n, elf):
        houses[h] += elf * 10

i = 0
while houses[i] < inp:
    i += 1

print(i)