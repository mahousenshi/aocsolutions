with open('2.txt', 'r') as f:
    inp = f.readlines()

total = 0

for present in inp:
    l, w, h = [int(n) for n in present.split('x')]

    total += min(2*(l+w), 2*(w+h), 2*(h+l)) + l*w*h

print(total)