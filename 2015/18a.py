with open('18.txt', 'r') as f:
    inp = f.readlines()

old = []

for l in inp:
    old.append(list(map(int, l.strip().replace('.', '0').replace('#', '1'))))

my = len(old)
mx = len(old[0])

dirs = [[-1, -1], [0, -1], [1, -1], [-1, 0], [1, 0], [-1, 1], [0, 1], [1, 1]]

for _ in range(100):
    new = []

    for y, l in enumerate(old):
        t = []

        for x, c in enumerate(l):
            lights = 0

            for d in dirs:
                dx = x + d[0]
                dy = y + d[1]

                if 0 <= dy and dy < my and 0 <= dx and dx < mx and old[dy][dx]:
                    lights += 1

            if c:
                if lights == 2 or lights == 3:
                    t.append(1)
                else:
                    t.append(0)
            else:
                if lights == 3:
                    t.append(1)
                else:
                    t.append(0)
        
        new.append(t)
    
    old = new

print(sum(map(sum, old)))
