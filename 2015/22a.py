spells = [
    {
        'name': 'zero',
        'mana': 0,
        'damage': 0,
        'timer': 0
    },
    {
        'name': 'magic missile',
        'mana': 53,
        'damage': 4,
        'timer': 0
    },
    {
        'name': 'drain',
        'mana': 73,
        'damage': 2,
        'timer': 0
    },
    {
        'name': 'shield',
        'mana': 113,
        'damage': 0,
        'timer': 6,
    },
    {
        'name': 'poison',
        'mana': 173,
        'damage': 3,
        'timer': 6,
    },
    {
        'name': 'recharge',
        'mana': 229,
        'damage': 0,
        'timer': 5
    }
]

ms = []

def timersTick(wizard, boss, timers):
    for timer in timers:
        time = timers[timer] 

        if time:
            time -= 1

            if timer == 'shield' and time == 0:
                wizard['armor'] = 0
            
            if timer == 'poison':
                boss['hp'] -= 3

            if timer == 'recharge':
                wizard['mana'] += 101
        else:
            continue

def nextSpell(wizard, boss, timers, spell, mana_spent):
    if wizard['mana'] < 53:
        if timers['poison'] or timers['recharge']:
            f = turn(wizard, boss, timers, spell[0], mana_spent)
        
        else:
            print('wizard dead')
        
    else:
        for spell in spells[1:]:
            if wizard['mana'] >= spell['mana']:
                
                if spell['name'] in timers and timers[spell['name']]:
                    continue

                f = turn(wizard, boss, timers, spell, mana_spent)
            
            else:
                continue

def turn(wizard, boss, timers, spell, mana_spent):
    # Wizard Turn
    timersTick(wizard, boss, timers)

    wizard['mana'] -= spell['mana']
    mana_spent += spell['mana']
      
    if spell['name'] == 'magic missile':
        boss['hp'] -= 4

    elif spell['name'] == 'drain':
        timers['hp'] -= 2
        wizard['hp'] += 2

    else:
        timers[spell['name']] == spell['timer']

    if boss['hp'] <= 0:
        ms.append(mana_spent)
        print('boss dead')
        return 0
    
    else:
        # Boss Turn
        timersTick(wizard, boss, timers)

        if boss['hp'] <= 0:
            ms.append(mana_spent)
            print('boss dead')
            return 0

        else:
            wizard['hp'] -= boss['damage'] - wizard['armor']

            if wizard['hp'] <= 0:
                print('wizard dead')
                return 0
    
    return 1

#turn 0
for spell in spells[1:1]:
    wizard = {
        'hp': 50,
        'armor': 0,
        'mana': 500
    }

    boss = {
        'hp': 55,
        'damage': 8
    }

    timers = {
        'shield': 0,
        'poison': 0,
        'recharge': 0,
    }

    mana_spent = 0

    turn(wizard, boss, timers, spell, mana_spent)
