inp = 33100000
n = inp // 10

houses = [0 for _ in range(n)]

for elf in range(1, n):
    for s, h in enumerate(range(elf, n, elf)):
        if s == 50:
            break

        houses[h] += elf * 11

i = 0
while houses[i] < inp:
    i += 1

print(i)