from itertools import combinations

def battle(player, enemy):
    win = 0

    while True:
        # Player turn
        enemy['hp'] -= max(player['damage'] - enemy['armor'], 1)

        if enemy['hp'] <= 0:
            win = 1
            break

        # Enemy turn
        player['hp'] -= max(enemy['damage'] - player['armor'], 1)

        if player['hp'] <= 0:
            break

    return win

with open('21.txt', 'r') as f:
    inp = f.readlines()

items = {}
min_cost = 0

with open('21items.txt', 'r') as f:
    for l in f.readlines():
        l = l.split()

        if len(l) == 1:
            item  = l[0].strip()

            items[item] = []
        else:
            items[item].append({
                'name': l[0],
                'cost': int(l[1]),
                'damage': int(l[2]),
                'armor': int(l[3])
            })

            min_cost += int(l[1])

for weapon in items['weapons']:
    if weapon['cost'] > min_cost:
        continue

    for armor in items['armors']:
        if weapon['cost'] + armor['cost'] > min_cost:
            continue

        # No rings
        player = {
            'hp': 100,
            'damage': weapon['damage'],
            'armor': armor['armor']
        }

        enemy = {
            'hp': int(inp[0].split(':')[1]),
            'damage': int(inp[1].split(':')[1]),
            'armor': int(inp[2].split(':')[1])
        }

        if battle(player, enemy):
            min_cost = min(min_cost, weapon['cost'] + armor['cost'])

        for ring1, ring2 in combinations(items['rings'], 2):
            if weapon['cost'] + armor['cost'] + ring1['cost'] + ring2['cost'] > min_cost:
                continue

            player = {
                'hp': 100,
                'damage': weapon['damage'] + ring1['damage'] + ring2['damage'],
                'armor': armor['armor'] + ring1['armor'] + ring2['armor'],
            }

            enemy = {
                'hp': int(inp[0].split(':')[1]),
                'damage': int(inp[1].split(':')[1]),
                'armor': int(inp[2].split(':')[1])
            }

            if battle(player, enemy):
                min_cost = min(min_cost, weapon['cost'] + armor['cost'] + ring1['cost'] + ring2['cost'])

print(min_cost)
