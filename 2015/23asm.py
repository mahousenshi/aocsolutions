a = 1
b = 0

if a == 0:
    a = 9663
else:
    a = 77671

while a != 1:
    b += 1

    if a % 2 == 0:
        a = a // 2
    else:
        a = 3*a + 1 

print(b)