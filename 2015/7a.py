from collections import deque
import re

with open('7.txt', 'r') as f:
    inp = [s.strip() for s in f.readlines()]

memory = {}
commands = deque([])
final = ''

for line in inp:
    opr = re.findall(r'^(\d+) -> (\w+)', line)

    if opr:
        opr = opr[0]

        memory[opr[1]] = int(opr[0])
    else:
        opr = re.findall(r'NOT (\w+) -> (\w+)', line)

        if opr:
            opr = opr[0]

            commands.append(('NOT', opr[0], opr[1]))
        else:
            opr = re.findall(r'(\w+) (\w+) (\d+|\w+) -> (\w+)', line)

            if opr:
                opr = opr[0]

                commands.append((opr[1], opr[0], opr[2], opr[3]))
            else:
                final = line.split()[0]

memory['1'] = 1

while commands:
    line = commands.popleft()

    if line[0] == 'NOT' and line[1] in memory:
        memory[line[2]] = (~memory[line[1]]) % 2**16

    elif line[0] == 'LSHIFT' and line[1] in memory:
        memory[line[3]] = (memory[line[1]] << int(line[2])) % 2**16

    elif line[0] == 'RSHIFT' and line[1] in memory:
        memory[line[3]] = memory[line[1]] >> int(line[2])

    elif line[0] == 'AND' and line[1] in memory and line[2] in memory:
        memory[line[3]] = memory[line[1]] & memory[line[2]]

    elif line[0] == 'OR' and line[1] in memory and line[2] in memory:
        memory[line[3]] = memory[line[1]] | memory[line[2]]

    else:
        commands.appendleft(line)
        commands.rotate()

print(memory[final])