from string import ascii_lowercase

def one(word):
    for i in range(len(ascii_lowercase)-2):
        if ascii_lowercase[i:i+3] in word:
            return False

    return True

def two(word):
    return any([x in word for x in ['i', 'o', 'l']])

def pair(word):
    for i, c in enumerate(word):
        if word[i:i+2] == f'{c}{c}':
            return i

    return -1

def three(word):
    i = pair(word)

    if i+1 and pair(word[i+2:])+1:
        return False

    return True

def next(word):
    next = []
    carry = 1

    for c in reversed(list(word)):
        if c == 'z' and carry:
            next.append('a')
            carry = 1

        else:
            next.append(ascii_lowercase[ord(c) - 97 + carry])
            carry = 0

    if carry:
        next.append('a')

    return ''.join(reversed(next))

word = 'vzbxkghb'

while any([one(word), two(word), three(word)]):
    word = next(word)

word = next(word)

while any([one(word), two(word), three(word)]):
    word = next(word)

print(word)