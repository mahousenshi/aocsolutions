import re

with open('5.txt', 'r') as f:
    lines = [l.strip() for l in f.readlines()]

nice = 0

for string in lines:
    if len(re.findall(r'a|e|i|o|u', string)) < 3:
        continue

    if not re.search(r'(\w)\1+', string):
        continue

    if re.search(r'ab|cd|pq|xy', string):
        continue

    nice += 1

print(nice)