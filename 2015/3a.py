with open('3.txt', 'r') as f:
    inp = f.read()

houses = set()

rose = {
    '^': (0, 1),
    '>': (1, 0),
    'v': (0, -1),
    '<': (-1, 0)
}

x, y = 0, 0

for c in inp:
    x += rose[c][0]
    y += rose[c][1]

    houses.add((x, y))

print(len(houses))