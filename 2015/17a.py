from itertools import combinations

with open('17.txt', 'r') as f:
    inp = f.readlines()

containers = sorted([int(n) for n in inp])

fit = []

for i in range(len(containers)):
    for container in combinations(containers, i+1):
        if sum(container) == 150:
            fit += 1

print(fit)