from collections import defaultdict

with open('3.txt', 'r') as f:
    inp = f.read()

houses = set()

rose = {
    '^': (0, 1),
    '>': (1, 0),
    'v': (0, -1),
    '<': (-1, 0)
}

# Santa
x, y = 0, 0
for d in inp[0::2]:
    x += rose[d][0]
    y += rose[d][1]

    houses.add((x, y))

# Robo Santa
x, y = 0, 0
for d in inp[1::2]:
    x += rose[d][0]
    y += rose[d][1]

    houses.add((x, y))

print(len(houses))