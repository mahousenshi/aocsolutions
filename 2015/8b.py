with open('8.txt', 'r') as f:
    inp = [s.strip() for s in f.readlines()]

total = 0

for string in inp:
    new_string = string.replace('\\', '\\\\').replace('"', '\\"')
    
    total += (len(new_string) + 2) - len(string)

print(total)
