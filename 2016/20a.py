with open('20.txt', 'r') as f:
    inp = f.readlines()

intervals = []

for interval in inp:
    a, b = map(int, interval.split('-'))

    intervals.append((a, b))

intervals.sort(key = lambda k: k[0])

max_ip = 1

ip = 0

for interval in intervals:
    a, b = interval

    min_ip = min(min_ip, a)
    max_ip = max(max_ip, b)

    print(min_ip, max_ip)