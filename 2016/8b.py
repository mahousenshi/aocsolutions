from itertools import chain
from collections import deque
import re

with open('8.txt', 'r') as f:
    inp = f.readlines()

grid = [[0 for j in range(50)] for i in range(6)]

for inst in inp:
    a, b = list(map(int, re.findall(r'\d+', inst)))

    if 'rect' in inst:
        for x in range(b):
            for y in range(a):
                grid[x][y] = 1

    if 'column' in inst:
        t = deque([grid[x][a] for x in range(6)])
        t.rotate(b)
        
        for x in range(6):
            grid[x][a] = t[x]
    
    if 'row' in inst:
        t = deque(grid[a])
        t.rotate(b)
        grid[a] = list(t)

for line in grid:
    print(''.join(map(str, line)).replace('0', '.').replace('1', '#'))