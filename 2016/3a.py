with open('3.txt', 'r') as f:
    inp = f.readlines()

possible = 0

for triangule in inp:
    a, b, c = list(map(int, triangule.split()))

    if a + b > c and c + a > b and b + c > a:
       possible += 1

print(possible)