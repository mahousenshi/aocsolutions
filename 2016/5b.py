import hashlib

inp = 'ojvtpuvg'

password = ['' for _ in range(8)]

i = 0

h = hashlib.md5(str.encode(inp))

while True:
    g = h.copy()
    g.update(str.encode(str(i)))

    if g.hexdigest()[:5] == '0' * 5:
        p = int(g.hexdigest()[5], 16)

        if p < 8 and password[p] == '':
            password[p] = g.hexdigest()[6]

        if len(''.join(password)) == 8:
            break

    i += 1

print(''.join(password))