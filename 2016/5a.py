import hashlib

inp = 'ojvtpuvg'

password = ''

i = 0

h = hashlib.md5(str.encode(inp))

while True:
    g = h.copy()
    g.update(str.encode(str(i)))

    if g.hexdigest()[:5] == '0' * 5:
        password += g.hexdigest()[5]

        if len(password) == 8:
            break

    i += 1

print(password)