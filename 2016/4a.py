from collections import Counter

with open('4.txt', 'r') as f:
    inp = f.readlines()

real = 0

for room in inp:
    room = room.split('-')

    freq = Counter(''.join(room[:-1])).most_common()
    freq.sort(key = lambda x: x[0])
    freq.sort(key = lambda x: x[1], reverse = True)
    
    checksum = ''

    for elem in freq[:5]:
        checksum += elem[0]

    if checksum == room[-1][4:9]:
        real += int(room[-1][0:3])
    
print(real)