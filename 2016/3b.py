def isPossible(triangule):
    a, b, c = triangule

    return a + b > c and c + a > b and b + c > a

with open('3.txt', 'r') as f:
    inp = f.readlines()

possible = 0

triangules = [[], [], []]

for i, line in enumerate(inp):
    for triangule, l in zip(triangules, map(int, line.split())):
        triangule.append(l)

    if i % 3 == 2:
        for triangule in triangules:
            if isPossible(triangule):
                possible += 1
            
        triangules = [[], [], []]

print(possible)