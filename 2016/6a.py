from collections import Counter

with open('6.txt', 'r') as f:
    inp = f.readlines()

columns = ['' for _ in range(8)]

for word in inp:
    for i, c in enumerate(word.strip()):
        columns[i] += c

message = ''

for column in columns:
    message += Counter(column).most_common(1)[0][0]

print(message)