from collections import defaultdict
from itertools import cycle
import re

with open('10.txt', 'r') as f:
    inp = f.readlines()

out = defaultdict(int)
bots = defaultdict(list)

for line in inp:
    line = line.split()

    if 'value' in line:
        val = int(line[1])
        bot = line[-1]

        bots[bot].append(val)

for line in cycle(inp):
    line = line.split()

    if 'gives' in line:
        bot = line[1]

        if len(bots[bot]) == 2:
            bots[bot].sort()

            high = bots[bot].pop()
            low = bots[bot].pop()

            if line[5] == 'output':
                out[line[6]] = low
            else:
                bots[line[6]].append(low)

            if line[10] == 'output':
                out[line[11]] = high
            else:
                bots[line[11]].append(high)

    if out['0'] * out['1'] * out['2']:
        break

print(out['0'] * out['1'] * out['2'])
