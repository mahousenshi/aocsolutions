from collections import deque

# not done

password = deque('abcde')

with open('21.txt', 'r') as f:
    inp = f.readlines()

for inst in inp:
    if 'swap' in inst:
        a = inst.split()[2]
        b = inst.split()[5]

        if 'position' in inst:
            a = int(a)
            b = int(b)

        else:
            a = password.index(a)
            b = password.index(b)

        t = password[a]
        password[a] = password[b]
        password[b] = t

    if 'rotate' in inst:
        if 'position' in inst:
            a = inst.split()[-1]

            i = password.index(a)

            if i >= 4:
                i += 2
            else:
                i += 1

            password = password.rotate(i)
        
        else:
            a = int(inst.split()[2])

            if 'right' in inst:
                password = password.rotate(a)
            else:
                password = password.rotate(-a)

    if 'reverse' in inst:
        a = int(inst.split()[2])
        b = int(inst.split()[4])

        password = list(password)

        print(password[:a] + )
        print(password[a:b+1][::-1])

        t = list(password)[:a-1] + list(password)[a-1:b:-1] + list(password)[b:]
        password = deque(t)

    if 'move' in inst:
        a = int(inst.split()[2])
        b = int(inst.split()[5])

        password = password.remove(password[a]).insert(a, b)

    print(password)


    