with open('18.txt', 'r') as f:
    inp = f.read()

traps = ['^^.', '.^^', '^..', '..^']

s = inp
safe = s.count('.')

for _ in range(400000 - 1):
    s = '.' + s + '.'
    ns = ''

    for i in range(1, len(s)-1):
        t = s[i-1:i+2]

        if t in traps:
            ns += '^'
        else:
            ns += '.'

    s = ns
    safe += s.count('.')

print(safe)