with open('1.txt', 'r') as f:
    inp = f.read().split(', ')

dirs = [[0, 1], [1, 0], [0, -1], [-1, 0]]
d = 0

x, y = 0, 0

coords = []
f = 0

for coord in inp:
    if coord[0] == 'L':
        d = (d - 1) % 4
    else:
        d = (d + 1) % 4
    
    for _ in range(int(coord[1:])):
        x += dirs[d][0]
        y += dirs[d][1]

        s = f'{x},{y}'

        if s in coords:
            f = 1
            break
        else:
            coords.append(f'{x},{y}')

    if f:
        break

print(abs(x) + abs(y))