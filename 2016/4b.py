with open('4.txt', 'r') as f:
    inp = f.readlines()

for room in inp:
    room = room.split('-')
    name = '-'.join(room[:-1])

    sid = int(room[-1][0:3])

    decrypted = ''

    for c in name:
        if c == '-':
            decrypted += ' '
        else:
            decrypted += chr(((ord(c) - 97 + sid) % 26) + 97)

    if decrypted == 'northpole object storage':
        break

print(sid)