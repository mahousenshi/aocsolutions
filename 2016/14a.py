import hashlib
import re

inp = 'abcabc'

keys = []
ckeys = []

i = 0

h = hashlib.md5(str.encode(inp))

while True:
    g = h.copy()
    g.update(str.encode(str(i)))
    
    for key in ckeys:
        pattern = '({})\1{{4,}}'.format(key['char'])

        if i < key['i'] + 1001 and re.search(pattern, g.hexdigest()):
            keys.append(key['i'])

            if len(keys) == 64:
                break
    
    print(len(ckeys))
    if len(keys) == 64:
        break

    candidate = re.search(r'(\w)\1{2,}', g.hexdigest())
    
    if candidate:
        ckeys.append({
            'i': i,
            'char': g.hexdigest()[candidate.start()]
        })
    
    i += 1

print(keys[-1])

