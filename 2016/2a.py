with open('2.txt', 'r') as f:
    inp = f.readlines()

keyboard = '123456789'

dirs = {
    'U': [0, -1],
    'R': [1, 0],
    'D': [0, 1],
    'L': [-1, 0]
}

x, y = 1, 1

password = ''

for line in inp:
    for d in line.strip():
        x = min(max(x + dirs[d][0], 0), 2)
        y = min(max(y + dirs[d][1], 0), 2)

    password += keyboard[x + 3 * y]

print(password)