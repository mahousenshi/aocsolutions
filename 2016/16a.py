inp = '11011110011011101'

disk_size = 272

s = inp

# dragon curve
while len(s) < disk_size:
    a = s
    b = a[::-1].replace('0', '2').replace('1', '0').replace('2', '1')

    s = a + '0' + b

s = s[:disk_size]

# checksum 
while len(s) % 2 == 0:
    ns = ''

    for pair in [s[i:i + 2] for i in range(0, len(s), 2)]:
        ns += str(int(pair[0] == pair[1]))

    s = ns

print(s)