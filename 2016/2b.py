with open('2.txt', 'r') as f:
    inp = f.readlines()

keyboard = '000000000010000023400056789000ABC00000D0000000000'

dirs = {
    'U': [0, -1],
    'R': [1, 0],
    'D': [0, 1],
    'L': [-1, 0]
}

x, y = 1, 3

password = ''

for line in inp:
    for d in line.strip():
        x += dirs[d][0]
        y += dirs[d][1]

        if keyboard[x + 7 * y] == '0':
            x -= dirs[d][0]
            y -= dirs[d][1]

    password += keyboard[x + 7 * y]

print(password)
