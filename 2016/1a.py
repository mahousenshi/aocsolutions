with open('1.txt', 'r') as f:
    inp = f.read().split(', ')

dirs = [0, 0, 0, 0]
d = 0

for coord in inp:
    if coord[0] == 'L':
        d = (d - 1) % 4
    else:
        d = (d + 1) % 4
    
    dirs[d] += int(coord[1:])

print(abs(dirs[0] - dirs[2]) + abs(dirs[1] - dirs[3]))