def cpy(r, a):
    if a.isdigit():
        return int(a)
    else:
        return r[a]

# --

def inc(r, a):
    return r[a] + 1

def dec(r, a):
    return r[a] - 1

# --

def jnz(r, a, b):
    return b if a else 0

with open('12.txt', 'r') as f:
    program = f.readlines()
    
instructions = [cpy, inc, dec, jnz]
instructions = {f.__name__: f for f in instructions }
register = {'a': 0, 'b': 0, 'c': 0, 0]

i = 0

while 0 <= i < len(program):
    line = program[i].split()

    opcode = line[0]

    if opcode in ['hlf', 'tpl', 'inc']:
        a = ord(line[1]) - 97

        register[a] = instructions[opcode](register, a)

    elif opcode == 'jmp':
        i += int(line[1]) - 1

    elif opcode in ['jie', 'jio']:
        a = ord(line[1][0]) - 97
        b = int(line[2])

        i += instructions[opcode](register, a, b)

    i += 1

print(register[1])