from collections import defaultdict

def write(pointer, offset, mode):
    if mode == 0:
        pointer = intcode[pointer]

    if mode == 2:
        pointer = intcode[pointer] + offset

    return pointer

def read(pointer, offset, mode):
    return intcode[write(pointer, offset, mode)]

intcode = defaultdict(int)

with open('11.txt', 'r') as f:
    for i, elem in enumerate(list(map(int, f.read().split(',')))):
        intcode[i] = elem

x = 0
y = 0

grid = defaultdict(int)
grid['0,0'] = 0
inp = grid['0,0']

dire = [[-1, 0], [0, -1], [1, 0], [0, 1]]
d = 0

pointer = 0
offset = 0

f = 1

while True:
    opcode = f'{intcode[pointer]:05}'

    mode = list(map(int, opcode[:3]))[::-1]
    inst = int(''.join(opcode[3:]))


    # adds
    if inst == 1:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        intcode[c] = a + b

        pointer += 4

    # multiplies
    elif inst == 2:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        intcode[c] = a * b

        pointer += 4

    # input
    elif inst == 3:
        a = write(pointer+1, offset, mode[0])

        intcode[a] = inp

        pointer += 2

    # output
    elif inst == 4:
        a = write(pointer+1, offset, mode[0])

        if f:
            # color
            grid[f'{x},{y}'] = intcode[a]

            f = 0
        else:
            # direction
            if intcode[a]:
                d = (d - 1) % 4
            else:
                d = (d + 1) % 4

            x += dire[d][0]
            y += dire[d][1]

            inp = grid[f'{x},{y}']

            f = 1

        pointer += 2

    # jump-if-true
    elif inst == 5:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])

        if a:
            pointer = b
        else:
            pointer += 3

    # jump-if-false
    elif inst == 6:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])

        if a:
            pointer += 3
        else:
            pointer = b

    # less than
    elif inst == 7:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        if a < b:
            intcode[c] = 1
        else:
            intcode[c] = 0

        pointer += 4

    # equals
    elif inst == 8:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        if a == b:
            intcode[c] = 1
        else:
            intcode[c] = 0

        pointer += 4

    # relative
    elif inst == 9:
        a = read(pointer+1, offset, mode[0])

        offset += a

        pointer += 2

    elif inst == 99:
        break

print(len(grid))