from itertools import combinations, cycle
from math import sqrt, atan2

def dist(a, b):
    return sqrt((b[0] - a[0])**2 + (b[1] - a[1])**2)

with open('10.txt', 'r') as f:
    inp = f.readlines()

astros = []

def unique(l):
    u = []

    for _ in l:
        if _ not in u:
            u.append(_)

    return u

for x, l in enumerate(inp):
    for y, c in enumerate(l):
        if c == "#":
            astros.append([y, x])

seen = []

for a in astros:
    minus = astros.copy()
    minus.remove(a)

    remove = []

    for b, c in combinations(minus, 2):
        if a[0]*b[1] + a[1]*c[0] + b[0]*c[1] - c[0]*b[1] - c[1]*a[0] - b[0]*a[1] == 0:
            if dist(a, c) > dist(a, b) and dist(c, a) > dist(c, b):
                remove.append(c)

            if dist(a, b) > dist(a, c) and dist(b, a) > dist(b, c):
                remove.append(b)

    seen.append(len(astros) - len(unique(remove)) - 1)

center = astros[seen.index(max(seen))]
astros.remove(center)

astros.sort(key = lambda x: dist(center, x))
astros.sort(key = lambda x: atan2(x[0] - center[0], x[1] - center[1]), reverse = True)

vaporized = []
m = 0

for i, astro in enumerate(cycle(astros)):
    if astros in vaporized:
        continue
    
    if i:
        m_new = atan2(astro[0] - center[0], astro[1] - center[1])

        if m_new != m:
            vaporized.append(astro)
            m = m_new
            
    else:
        m  = atan2(astro[0] - center[0], astro[1] - center[1])
        vaporized.append(astro)

    if len(vaporized) == 200:
        break

print(f'{vaporized[199][0]}{vaporized[199][1]:02}')