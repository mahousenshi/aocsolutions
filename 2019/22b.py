import re

cards = 119315717514047

def deal(card):
    return cards - (card + 1)

def cut(card, n):
    return (card - n) % cards

def deal_incr(card, n):
    return (card * n) % cards

with open('22.txt', 'r') as f:
    inp = f.readlines()

card = 2020
i = 0


    for comm in inp:
        comm = comm.strip()

        if re.fullmatch(r'deal into new stack', comm):
            card = deal(card)

        if re.fullmatch(r'cut -?\d+', comm):
            n = int(comm.split(' ')[-1])

            card = cut(card, n)

        if re.fullmatch(r'deal with increment \d+', comm):
            n = int(comm.split(' ')[-1])

            card = deal_incr(card, n)

    i += 1

print(i)