with open('8.txt', 'r') as f:
    image = f.read().strip()

x = 25
y = 6

dim = x * y

layers = []

for i in range(0, len(image), dim):
    layers.append(image[i:i + dim])

layers.reverse()

final = list(map(int, layers[0]))

for layer in layers[1:]:
    layer = list(map(int, layer))

    for i, pixel in enumerate(layer):
        if pixel != 2:
            final[i] = pixel

dots = ['⚫', '⚪', ' ']

final = ' '.join(map(lambda  x: dots[x], final))

for i in range(0, len(final), 2*x):
    print(final[i:i+2*x])