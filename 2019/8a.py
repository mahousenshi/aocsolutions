from collections import Counter

with open('8.txt', 'r') as f:
    image = f.read().strip()

x = 25
y = 6

dim = x * y

zeros = dim
multi = 0

for i in range(0, len(image), dim):
    counter = Counter(image[i:i+dim])

    if counter['0'] < zeros:
        zeros = counter['0']

        multi = counter['1'] * counter['2']

print(multi)