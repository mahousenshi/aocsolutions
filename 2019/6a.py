from collections import defaultdict

with open('6.txt', 'r') as f:
    nodes = f.readlines()

planets = defaultdict(list)

def orb_count(nodes, node_list, s):
    for node in nodes:
        if node != 'COM':
            s += orb_count(node_list[node], node_list, s) + 1

    return s

for node in nodes:
    a, b = node.strip().split(')')

    planets[b].append(a)

n_orbits = 0

for planet in planets:
    n_orbits += len(planets[planet]) + orb_count(planets[planet], planets, 0)

print(n_orbits)
