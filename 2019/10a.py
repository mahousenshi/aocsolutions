from itertools import combinations
from math import sqrt

with open('10.txt', 'r') as f:
    inp = f.readlines()

astros = []

def dist(a, b):
    return sqrt((b[0] - a[0])**2 + (b[1] - a[1])**2)

def unique(l):
    u = []

    for _ in l:
        if _ not in u:
            u.append(_)

    return u

for x, l in enumerate(inp):
    for y, c in enumerate(l):
        if c == "#":
            astros.append([y, x])

seen = []

for a in astros:
    minus = astros.copy()
    minus.remove(a)

    remove = []

    for b, c in combinations(minus, 2):
        if a[0]*b[1] + a[1]*c[0] + b[0]*c[1] - c[0]*b[1] - c[1]*a[0] - b[0]*a[1] == 0:
            if dist(a, c) > dist(a, b) and dist(c, a) > dist(c, b):
                remove.append(c)

            if dist(a, b) > dist(a, c) and dist(b, a) > dist(b, c):
                remove.append(b)

    seen.append(len(astros) - len(unique(remove)) - 1)

print(max(seen))
