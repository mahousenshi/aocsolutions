from collections import defaultdict

DEBUG = 0

intcode = defaultdict(int)

with open('9.txt', 'r') as f:
    for i, elem in enumerate(list(map(int, f.read().split(',')))):
        intcode[i] = elem

def read(pointer, offset, mode):
    if mode == 0:
        pointer = intcode[pointer]

    if mode == 2:
        pointer = intcode[pointer] + offset

    return intcode[pointer]

def write(pointer, offset, mode):
    if mode == 0:
        pointer = intcode[pointer]

    if mode == 2:
        pointer = intcode[pointer] + offset

    return pointer

pointer = 0
offset = 0

while True:
    opcode = f'{intcode[pointer]:05}'

    mode = list(map(int, opcode[:3]))[::-1]
    inst = int(''.join(opcode[3:]))

    # adds
    if inst == 1:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        if DEBUG:
            print(f'{inst} => [{c}] = {a} + {b}')

        intcode[c] = a + b

        pointer += 4

    # multiplies
    elif inst == 2:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        if DEBUG:
            print(f'{inst} => [{c}] = {a} * {b}')

        intcode[c] = a * b

        pointer += 4

    # input
    elif inst == 3:
        a = write(pointer+1, offset, mode[0])

        if DEBUG:
            print(f'{inst} => [{a}]')

        intcode[a] = int(input('> '))

        pointer += 2

    # output
    elif inst == 4:
        a = write(pointer+1, offset, mode[0])

        if DEBUG:
            print(f'{inst} => [{a}]')

        print(intcode[a])

        pointer += 2

    # jump-if-true
    elif inst == 5:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])

        if DEBUG:
            print(f'{inst} => {a} ? p = {b} | p += 3')

        if a:
            pointer = b
        else:
            pointer += 3

    # jump-if-false
    elif inst == 6:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])

        if DEBUG:
            print(f'{inst} => {a} ? p += 3 | p = {b}')

        if a:
            pointer += 3
        else:
            pointer = b

    # less than
    elif inst == 7:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        if DEBUG:
            print(f'{inst} => {a} < {b} ? [{c}] = 1 | [{c}] = 0')

        if a < b:
            intcode[c] = 1
        else:
            intcode[c] = 0

        pointer += 4

    # equals
    elif inst == 8:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        if DEBUG:
            print(f'{inst} => {a} == {b} ? [{c}] = 1 | [{c}] = 0')

        if a == b:
            intcode[c] = 1
        else:
            intcode[c] = 0

        pointer += 4

    # relative
    elif inst == 9:
        a = read(pointer+1, offset, mode[0])

        if DEBUG:
            print(f'{inst} => offset = {offset + a}')

        offset += a

        pointer += 2

    elif inst == 99:
        break