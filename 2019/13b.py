from collections import defaultdict

def write(pointer, offset, mode):
    if mode == 0:
        pointer = intcode[pointer]

    if mode == 2:
        pointer = intcode[pointer] + offset

    return pointer

def read(pointer, offset, mode):
    return intcode[write(pointer, offset, mode)]

def screen(grid):
    pixels = [' ', '🧱', '⬛', '🏓', '⚪']

    grid = list(map(lambda x: pixels[x], grid))
    
    for i in range(0, 37):
        dd = i * 20

        print(grid[dd:dd+20])

intcode = defaultdict(int)

with open('13.txt', 'r') as f:
    for i, elem in enumerate(list(map(int, f.read().split(',')))):
        intcode[i] = elem

intcode[0] = 2

pointer = 0
offset = 0

x = 0
y = 0
score = 0

joystick = 0

grid = [0 for _ in range(740)]

d = 0
f = 0

while True:
    opcode = f'{intcode[pointer]:05}'

    mode = list(map(int, opcode[:3]))[::-1]
    inst = int(''.join(opcode[3:]))

    # adds
    if inst == 1:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        intcode[c] = a + b

        pointer += 4

    # multiplies
    elif inst == 2:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        intcode[c] = a * b

        pointer += 4

    # input
    elif inst == 3:
        a = write(pointer+1, offset, mode[0])

        screen(grid)
        
        if d != 2:
            intcode[a] = 1
            print(score)
        else:
            intcode[a] = 0
        #intcode[a] = int(input('> '))

        d += 1
        pointer += 2

    # output
    elif inst == 4:
        a = write(pointer+1, offset, mode[0])

        if f == 0:
            x = intcode[a]
        if f == 1:
            y = intcode[a]

        if f == 2:
            if x == -1 and y == 0:
                score = intcode[a]
            else:
                grid[x + y * 20] = intcode[a]

        f = (f + 1) % 3

        pointer += 2


    # jump-if-true
    elif inst == 5:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])

        if a:
            pointer = b
        else:
            pointer += 3

    # jump-if-false
    elif inst == 6:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])

        if a:
            pointer += 3
        else:
            pointer = b

    # less than
    elif inst == 7:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        if a < b:
            intcode[c] = 1
        else:
            intcode[c] = 0

        pointer += 4

    # equals
    elif inst == 8:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        if a == b:
            intcode[c] = 1
        else:
            intcode[c] = 0

        pointer += 4

    # relative
    elif inst == 9:
        a = read(pointer+1, offset, mode[0])

        offset += a

        pointer += 2

    elif inst == 99:
        break

