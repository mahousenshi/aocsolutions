from itertools import combinations
from math import sqrt
import re

with open('12.txt', 'r') as f:
    inp = f.readlines()

moons = []

for moon in inp:
    s = re.split(r'<x=(-?\d+), y=(-?\d+), z=(-?\d+)>', moon)

    moons.append({'x': int(s[1]), 'y': int(s[2]), 'z': int(s[3]), 'dx': 0, 'dy': 0, 'dz': 0})

for i in range(1000):
    # Velocity
    for moona, moonb in combinations(moons, 2):
        for c in 'xyz':
            if moona[c] > moonb[c]:
                moona[f'd{c}'] -= 1
                moonb[f'd{c}'] += 1
            elif moona[c] < moonb[c]:
                moona[f'd{c}'] += 1
                moonb[f'd{c}'] -= 1

    # Step
    for moon in moons:
        for c in 'xyz':
            moon[c] += moon[f'd{c}']

potencial = []
kinect = []

t = 0

for moon in moons:
    p = 0
    k = 0

    for c in 'xyz':
        p += abs(moon[c])
        k += abs(moon[f'd{c}'])

    t += p * k

print(t)