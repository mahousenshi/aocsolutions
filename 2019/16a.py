from itertools import cycle

with open('16.txt', 'r') as f:
    inp = list(map(int, f.read()))

base = [0, 1, 0, -1]

for c in range(100):
    new_inp = []

    for i in range(len(inp)):
        n = i + 1
        pattern = [j for j in base for _ in range(n)]
        pattern = pattern[1:] + [0]

        m = 0

        for a, b in zip(inp, cycle(pattern)):
            m += a * b

        new_inp.append(abs(m) % 10)
    
    inp = new_inp.copy()

print(''.join(map(str, inp[:8])))