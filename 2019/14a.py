from itertools import cycle

with open('14.txt', 'r') as f:
    nodes = f.readlines()

def pp_print(elements):
    for elem in elements:
        print(f'{elem} =>')
        print(f'per_reaction => {elements[elem]["per_reaction"]}')
        print(f'needed => {elements[elem]["needed"]}')
        print(f'ingredients =>')
        for ing in elements[elem]["ingredients"]:
            print(f'\t{ing} = {elements[elem]["ingredients"][ing]}')
        print()

def to_make(name, qtd):
    n = 1
    needed = qtd
    per_reaction = elements[name]['per_reaction']

    while n * per_reaction < needed:
        n += 1

    for ingredient in elements[name]['ingredients']:
        qtd = n * elements[name]['ingredients'][ingredient]
        elements[ingredient]['needed'] += qtd
        to_make(ingredient, qtd)

elements = {}
ore = []

for node in nodes:
    a, b = node.strip().split(' => ')
    quantity, name = b.split(' ')

    elements[name] = {
        'per_reaction': int(quantity),
        'needed': 0,
        'raw': 0, #per reaction
        'ingredients': {}
    }

    for ingredient in a.split(', '):
        quantity, ingredient = ingredient.split(' ')

        if ingredient == 'ORE':
            elements['raw'] = int(quantity)
            ore.append(name)
        
        else:
            elements[name]['ingredients'][ingredient] = int(quantity)

to_make('FUEL', 1)

elements_raw = elements.copy()
print(elements)


for name in cycle(elements):
    remove = []
    #print(name)
    #print(elements_raw[name]['ingredients'].items())

    for ingredient in elements_raw[name]['ingredients'].items():
        if ingredient in ore:        
            n = 1
            needed = elements_raw[name]['ingredients'][ingredient]
            per_reaction = elements_raw[name]['per_reaction']

            while n * per_reaction < needed:
                n += 1

            elements[name]['raw'] += elements_raw[ingredient]['raw'] * n

            remove.append(ingredient)
    
    for r in remove:
        del elements_raw[name]['ingredients'][r]

    if len(elements_raw[name]['ingredients']) == 0:
        ore.append(name)

    if len(ore) == len(elements_raw):
        break