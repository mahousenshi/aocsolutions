with open('2.txt', 'r') as f:
    intcode_gold = list(map(int, f.read().split(',')))

for noun in range(100):
    for verb in range(100):
        intcode = intcode_gold.copy()
        intcode[1] = noun
        intcode[2] = verb

        p = 0

        while intcode[p] != 99:
            # add
            if intcode[p] == 1:
                a, b, c = intcode[p+1:p+4]
                intcode[c] = intcode[a] + intcode[b]
                p += 4
            
            # multi
            elif intcode[p] == 2:
                a, b, c = intcode[p+1:p+4]
                intcode[c] = intcode[a] * intcode[b]
                p += 4

            else:
                break
        
        if intcode[0] == 19690720:
            print(f'{noun}{verb}')
            break