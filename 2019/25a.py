from collections import defaultdict

def write(pointer, offset, mode):
    if mode == 0:
        pointer = intcode[pointer]

    if mode == 2:
        pointer = intcode[pointer] + offset

    return pointer

def read(pointer, offset, mode):
    return intcode[write(pointer, offset, mode)]

intcode = defaultdict(int)

with open('25.txt', 'r') as f:
    for i, elem in enumerate(list(map(int, f.read().split(',')))):
        intcode[i] = elem

with open('25prg.txt', 'r') as f:
    comms = f.readlines()

comms.reverse()
comm = ''

pointer = 0
offset = 0

screen = ''

while True:
    opcode = f'{intcode[pointer]:05}'

    mode = list(map(int, opcode[:3]))[::-1]
    inst = int(''.join(opcode[3:]))

    # adds
    if inst == 1:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        intcode[c] = a + b

        pointer += 4

    # multiplies
    elif inst == 2:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        intcode[c] = a * b

        pointer += 4

    # input
    elif inst == 3:
        a = write(pointer+1, offset, mode[0])

        if screen:
            print(screen)
            screen = ''

        if not comm:
            if comms:
                comm = list(comms.pop())
                comm.reverse()
            
            else:
                comm = list(input('> ').strip() + '\n')
                comm.reverse()
        
        ss = comm.pop()
        print(ss)


        intcode[a] = ord(ss)

        pointer += 2

    # output
    elif inst == 4:
        a = write(pointer+1, offset, mode[0])

        screen += chr(intcode[a])

        pointer += 2

    # jump-if-true
    elif inst == 5:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])

        if a:
            pointer = b
        else:
            pointer += 3

    # jump-if-false
    elif inst == 6:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])

        if a:
            pointer += 3
        else:
            pointer = b

    # less than
    elif inst == 7:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        if a < b:
            intcode[c] = 1
        else:
            intcode[c] = 0

        pointer += 4

    # equals
    elif inst == 8:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        if a == b:
            intcode[c] = 1
        else:
            intcode[c] = 0

        pointer += 4

    # relative
    elif inst == 9:
        a = read(pointer+1, offset, mode[0])

        offset += a

        pointer += 2

    elif inst == 99:
        print(screen)
        break