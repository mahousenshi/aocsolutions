from itertools import permutations
import queue

with open('7.txt', 'r') as f:
    intcode_gold = list(map(int, f.read().split(',')))

def amplifier(name, phase):
    intcode = intcode_gold.copy()

    p = 0
    f = 0
    output = 0

    while True:
        opcode = f'{intcode[p]:05}'

        mode = list(map(int, opcode[:3]))[::-1]
        inst = int(''.join(opcode[3:]))

        # adds
        if inst == 1:
            a = intcode[p+1] if mode[0] else intcode[intcode[p+1]]
            b = intcode[p+2] if mode[1] else intcode[intcode[p+2]]
            c = intcode[p+3]

            intcode[c] = a + b
            p += 4

        # multiplies
        elif inst == 2:
            a = intcode[p+1] if mode[0] else intcode[intcode[p+1]]
            b = intcode[p+2] if mode[1] else intcode[intcode[p+2]]
            c = intcode[p+3]

            intcode[c] = a * b
            p += 4

        # input
        elif inst == 3:
            a = intcode[p+1]

            if f:
                intcode[a] = signal
            else:
                intcode[a] = phase
                f = 1

            p += 2

        # output
        elif inst == 4:
            await intcode[p+1] if mode[0] else intcode[intcode[p+1]]
            p += 2

        # jump-if-true
        elif inst == 5:
            a = intcode[p+1] if mode[0] else intcode[intcode[p+1]]
            b = intcode[p+2] if mode[1] else intcode[intcode[p+2]]

            if a:
                p = b
            else:
                p += 3

        # jump-if-false
        elif inst == 6:
            a = intcode[p+1] if mode[0] else intcode[intcode[p+1]]
            b = intcode[p+2] if mode[1] else intcode[intcode[p+2]]

            if a:
                p += 3
            else:
                p = b

        # less than
        elif inst == 7:
            a = intcode[p+1] if mode[0] else intcode[intcode[p+1]]
            b = intcode[p+2] if mode[1] else intcode[intcode[p+2]]
            c = intcode[p+3]

            if a < b:
                intcode[c] = 1
            else:
                intcode[c] = 0

            p += 4

        # a == b
        elif inst == 8:
            a = intcode[p+1] if mode[0] else intcode[intcode[p+1]]
            b = intcode[p+2] if mode[1] else intcode[intcode[p+2]]
            c = intcode[p+3]

            if a == b:
                intcode[c] = 1
            else:
                intcode[c] = 0

            p += 4

        elif inst == 99:
            return output

signal_max = 0

for phases in permutations('01234', 5):
    work_queue = queue.Queue()

    work_queue.put(0)

    



    q = asyncio.Queue()


    signal = 0

    
    
    modulators = [asyncio.create_task(intcode(phase)) for n in range(5)]

    if signal >= signal_max:
        signal_max = signal

print(signal_max)