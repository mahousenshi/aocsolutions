with open('1.txt', 'r') as f:
    inp = map(int, f.readlines())

s = 0

def module(fuel):
    return int(fuel / 3) - 2

for f in inp:
    f = module(f)

    while f >= 0:
        s += f
        f = module(f)

print(s)