def biome(eco):
    for l in eco:
        print(''.join(map(str, l)).replace('0', '.').replace('1', '#'))

with open('24.txt', 'r') as f:
    inp = f.readlines()

old = []

for l in inp:
    old.append(list(map(int, l.strip().replace('.', '0').replace('#', '1'))))

my = len(old)
mx = len(old[0])

dire = [[0, -1], [1, 0], [0, 1], [-1, 0]]

patterns = []

while True:
    new = []

    s = ''.join([''.join(map(str, l)) for l in old])
    
    if s in patterns:
        break
    else:
        patterns.append(s)

    for y, l in enumerate(old):
        t = []

        for x, c in enumerate(l):
            bugs = 0

            for d in dire:
                dx = x + d[0]
                dy = y + d[1]

                if 0 <= dy and dy < my and 0 <= dx and dx < mx and old[dy][dx]:
                    bugs += 1

            if c:
                if bugs == 1:
                    t.append(1)
                else:
                    t.append(0)
            else:
                if bugs == 1 or bugs == 2:
                    t.append(1)
                else:
                    t.append(0)
        
        new.append(t)

    old = new
    
print(sum([x * 2 ** i for i, x in enumerate(map(int, s))]))