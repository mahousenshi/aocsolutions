from itertools import combinations
from math import gcd
import re

with open('12.txt', 'r') as f:
    inp = f.readlines()

moons = []

for moon in inp:
    s = re.split(r'<x=(-?\d+), y=(-?\d+), z=(-?\d+)>', moon)

    moons.append({'x': int(s[1]), 'y': int(s[2]), 'z': int(s[3]), 'dx': 0, 'dy': 0, 'dz': 0})

flags = {'x': 0, 'y': 0, 'z': 0}
kinect = {'x': 0, 'y': 0, 'z': 0}

i = 0

while flags['x'] * flags['y'] * flags['z'] == 0:
    coords = 'xyz'
    s = ''

    for c in coords:
        if not flags[c]:
            s += c

    coords = s

    # Velocity
    for moona, moonb in combinations(moons, 2):
        for c in coords:
            if moona[c] > moonb[c]:
                moona[f'd{c}'] -= 1
                moonb[f'd{c}'] += 1
            elif moona[c] < moonb[c]:
                moona[f'd{c}'] += 1
                moonb[f'd{c}'] -= 1

    # Step
    for moon in moons:
        for c in coords:
            moon[c] += moon[f'd{c}']
            kinect[c] += abs(moon[f'd{c}'])
    i += 1

    for c in coords:
        if kinect[c] == 0:
            flags[c] = i

        kinect[c] = 0

a = flags['x']
b = flags['y']
c = flags['z']

print(a,b,c)

bc = b * c // gcd(b, c)
print((a * bc // gcd(a, bc)) * 2)