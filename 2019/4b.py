from collections import Counter

a, b = list(map(int, '245182-790572'.split('-')))

s = 0

for n in range(a, b + 1):
    n = [int(x) for x in str(n)]

    f = 0

    for i in range(5):
        if n[i] > n[i+1]:
            f = 1
            break

    if f:
        continue

    if not 2 in Counter(n).values():
        continue

    s += 1

print(s)
