dire = {
    'R': [1, 0],
    'L': [-1 , 0],
    'U': [0, 1],
    'D': [0, -1]
}

def path2coords(path):
    path = path.split(',')
    
    coords = []

    x, y = 0, 0

    for turns in path:
        d = turns[0]
        s = int(turns[1:])

        for _ in range(s):
            x += dire[d][0]
            y += dire[d][1]

            coords.append(f'{x},{y}')

    return(coords)

def manhattan(coords):
    return sum(map(abs, map(int, coords.split(','))))

with open('3.txt', 'r') as f:
    wire1, wire2 = map(path2coords, f.readlines())

inters = list(set(wire1).intersection(wire2))

print(min(map(manhattan, inters)))