import re

def deal(deck):
    return deck[::-1]

def cut(deck, n):
    return deck[-n:] + deck[:-n]

def deal_incr(deck, n):
    l = len(deck)

    new_deck = [0 for _ in range(l)]
    
    i = 0

    while deck:
        new_deck[i] = deck.pop()
        i = (i + n) % l
    
    new_deck.reverse()

    return new_deck

with open('22.txt', 'r') as f:
    inp = f.readlines()

deck = [i for i in range(10007)]
deck.reverse()

for i, comm in enumerate(inp):
    comm = comm.strip()

    if deck.count(0) > 1:
        print(deck.count(0))

    if re.fullmatch(r'deal into new stack', comm):
        deck = deal(deck)

    if re.fullmatch(r'cut -?\d+', comm):
        n = int(comm.split(' ')[-1])
        
        deck = cut(deck, n)
    
    if re.fullmatch(r'deal with increment \d+', comm):
        n = int(comm.split(' ')[-1])

        deck = deal_incr(deck, n)
    
deck.reverse()
print(deck[2019])

