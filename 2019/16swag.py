from itertools import cycle, compress

with open('16.txt', 'r') as f:
    inp = list(f.read() * 10000)

for c in range(100):
    print(c)
    new_inp = []

    for i in range(len(inp)):
        n = i + 1
        pos = [j for j in [0, 1, 0, 0] for _ in range(n)]
        neg = [j for j in [0, 0, 0, 1] for _ in range(n)]

        pos = pos[1:] + [0]
        neg = neg[1:] + [0]

        print(n)
        new_inp.append(abs(sum(map(int, compress(inp, cycle(pos)))) - sum(map(int, compress(inp, cycle(neg))))) % 10)
    
    inp = new_inp

print(''.join(inp[:8]))
