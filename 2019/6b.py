from collections import defaultdict

class Node:
    def __init__(self, name):
        self.name = name
        self.parent = None
        self.nodes = []
        self.value = 0

def remove_orphan(node, orphans):
    for name in orphans[node.name]:
        new = Node(name)

        node.nodes.append(new)
        new.parent = node

        remove_orphan(new, orphans)

def find_node(name, node):
    if node.name == name:
        return node

    else:
        temp = Node('temp')

        for temp in node.nodes:
            temp = find_node(name, temp)

            if temp.name == name:
                break

        return temp

def values(node, visited):
    visited.append(node.name)

    nodes = node.nodes + [node.parent] if node.name != root.name else []

    for temp in nodes:
        if temp.name not in visited:
            temp.value = node.value + 1
            values(temp, visited)

with open('6.txt', 'r') as f:
    nodes = f.readlines()

orphans = defaultdict(list)

for node in nodes:
    a, b = node.strip().split(')')
    orphans[a].append(b)

root = Node('COM')
remove_orphan(root, orphans) 

values(find_node('YOU', root), [])

print(find_node('SAN', root).value - 2)