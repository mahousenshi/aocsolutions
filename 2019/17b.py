from collections import defaultdict
import curses

def list_commands(grid):
    grid = grid.splitlines()[:-2]

    f = 0

    for y, l in enumerate(grid):
        for x, c in enumerate(l) in y:
            if c == '^':
                d = 0

            if c == '>':
                d = 1
                f = 1
                break
            
            if c == 'v':
                d = 2
                f = 1
                break
                
            
            if c == '<':
                d = 3
                f = 1
                break
        
        if f:
            break

    print(x, y, d)
    quit(0)

    forward = [[0, -1], [1, 0], [0, 1], [-1, 0]]
    left = [[-1, 0], [0, -1], [1, 0], [0, 1]]
    right = [[1, 0], [0, 1], [-1, 0], [0, -1]]

    my = len(grid) - 1
    mx = len(grid[0]) - 1

    t = ''
    c = 0

    while True:
        fx = x + forward[d][0]
        fy = y + forward[d][1]

        if my < fy or fy < 0 or mx < fx or fx < 0 or grid[fy][fx] == '.':
            lx = x + left[d][0]
            ly = y + left[d][1]

            rx = x + right[d][0]
            ry = y + right[d][1]

            if my < ly or ly < 0 or mx < lx or lx < 0:
                lg = '.'
            else:
                lg = grid[ly][lx]

            if my < ry or ry < 0 or mx < rx or rx < 0:
                rg = '.'
            else:
                rg = grid[ry][rx]

            if lg == '.' and rg == '.':
                print(f'{t}{c}')
                break

            if lg == '.':
                d = (d + 1) % 4
                print(f'{t}{c}')

                t = 'R'

                c = 0

            if rg == '.':
                d = (d - 1) % 4
                print(f'{t}{c}')

                t = 'L'

                c = 0

        else:
            x, y = fx, fy
            c += 1

    quit()

def write(pointer, offset, mode):
    if mode == 0:
        pointer = intcode[pointer]

    if mode == 2:
        pointer = intcode[pointer] + offset

    return pointer

def read(pointer, offset, mode):
    return intcode[write(pointer, offset, mode)]

intcode = defaultdict(int)

with open('17.txt', 'r') as f:
    for i, elem in enumerate(list(map(int, f.read().split(',')))):
        intcode[i] = elem

intcode[0] = 2

pointer = 0
offset = 0

grid = ''
screen = curses.initscr()
screen.addstr('10')

with open('17prg.txt', 'r') as f:
    commands = list(f.read())

commands.reverse()

while True:
    opcode = f'{intcode[pointer]:05}'

    mode = list(map(int, opcode[:3]))[::-1]
    inst = int(''.join(opcode[3:]))


    # adds
    if inst == 1:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        intcode[c] = a + b

        pointer += 4

    # multiplies
    elif inst == 2:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        intcode[c] = a * b

        pointer += 4

    # input
    elif inst == 3:
        a = write(pointer+1, offset, mode[0])

        for l in grid.splitlines():
            screen.addstr(l)       

        if len(commands):
            intcode[a] = ord(commands.pop())
    

        pointer += 2

    # output
    elif inst == 4:
        a = write(pointer+1, offset, mode[0])

        if intcode[a] > 127:
            str(intcode[a])
        else:
            screen.addstr(chr(intcode[a]))

        pointer += 2

    # jump-if-true
    elif inst == 5:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])

        if a:
            pointer = b
        else:
            pointer += 3

    # jump-if-false
    elif inst == 6:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])

        if a:
            pointer += 3
        else:
            pointer = b

    # less than
    elif inst == 7:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        if a < b:
            intcode[c] = 1
        else:
            intcode[c] = 0

        pointer += 4

    # equals
    elif inst == 8:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        if a == b:
            intcode[c] = 1
        else:
            intcode[c] = 0

        pointer += 4

    # relative
    elif inst == 9:
        a = read(pointer+1, offset, mode[0])

        offset += a

        pointer += 2

    elif inst == 99:
        break

curses.endwin()
