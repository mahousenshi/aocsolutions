from itertools import cycle

with open('16.txt', 'r') as f:
    inp = list(map(int, f.read() * 10000))

base = [0, 1, 0, -1]

 0 1 1 0 0 -1 -1 0
 0 1 1 0 0 -1 -1 0
for c in range(100):
    print(c)

    new_inp = []

    for i in range(len(inp)):
        n = i + 1
        pattern = [j for j in base for _ in range(n)]
        pattern = pattern[1:] + [0]

        print(n)

        m = 0

        for a, b in zip(inp, cycle(pattern)):
            m += a * b

        new_inp.append(abs(m) % 10)
    
    inp = new_inp.copy()

number = ''.join(map(str, inp))

offset = int(number[:8])

print(number[offset:offset + 8])