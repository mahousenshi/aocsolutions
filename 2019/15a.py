from collections import defaultdict

def write(pointer, offset, mode):
    if mode == 0:
        pointer = intcode[pointer]

    if mode == 2:
        pointer = intcode[pointer] + offset

    return pointer

def read(pointer, offset, mode):
    return intcode[write(pointer, offset, mode)]

intcode = defaultdict(int)

with open('11.txt', 'r') as f:
    for i, elem in enumerate(list(map(int, f.read().split(',')))):
        intcode[i] = elem

x = 0
y = 0

grid = {}

grid['0,0'] = 0

dire = [[0,1], [0,-1], [1,0], [-1,0]]
d = 1

pointer = 0
offset = 0

while True:
    opcode = f'{intcode[pointer]:05}'

    mode = list(map(int, opcode[:3]))[::-1]
    inst = int(''.join(opcode[3:]))

    # adds
    if inst == 1:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        intcode[c] = a + b

        pointer += 4

    # multiplies
    elif inst == 2:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        intcode[c] = a * b

        pointer += 4

    # input
    elif inst == 3:
        a = write(pointer+1, offset, mode[0])




        intcode[a] = input('> ')

        pointer += 2

    # output
    elif inst == 4:
        a = write(pointer+1, offset, mode[0])

        if intcode[a] == 0:
            grid[f'{x + dire[d-1][0]},{y + dire[d-1][1]}'] = 1

            end = sum([grid[f'{x + dire[dd][0]},{y + dire[dd][1]}'] for dd in range(1, 5)]

            if end == 3:
                #dead end 


        elif intcode[a] == 1:
            x += dire[d][0]
            y += dire[d][1]

            grid[f'{x},{y}'] = 0

            d = + =





        elif intcode[a] == 2:
            #run djstra

        pointer += 2

    # jump-if-true
    elif inst == 5:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])

        if a:
            pointer = b
        else:
            pointer += 3

    # jump-if-false
    elif inst == 6:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])

        if a:
            pointer += 3
        else:
            pointer = b

    # less than
    elif inst == 7:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        if a < b:
            intcode[c] = 1
        else:
            intcode[c] = 0

        pointer += 4

    # equals
    elif inst == 8:
        a = read(pointer+1, offset, mode[0])
        b = read(pointer+2, offset, mode[1])
        c = write(pointer+3, offset, mode[2])

        if a == b:
            intcode[c] = 1
        else:
            intcode[c] = 0

        pointer += 4

    # relative
    elif inst == 9:
        a = read(pointer+1, offset, mode[0])

        offset += a

        pointer += 2

    elif inst == 99:
        break