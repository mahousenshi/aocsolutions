with open('5.txt', 'r') as f:
    intcode = list(map(int, f.read().split(',')))

p = 0

while True:
    opcode = f'{intcode[p]:05}'

    mode = list(map(int, opcode[:3]))[::-1]
    inst = int(''.join(opcode[3:]))

    # adds
    if inst == 1:
        a = intcode[p+1] if mode[0] else intcode[intcode[p+1]]
        b = intcode[p+2] if mode[1] else intcode[intcode[p+2]]
        c = intcode[p+3]

        intcode[c] = a + b
        p += 4

    # multiplies
    elif inst == 2:
        a = intcode[p+1] if mode[0] else intcode[intcode[p+1]]
        b = intcode[p+2] if mode[1] else intcode[intcode[p+2]]
        c = intcode[p+3]

        intcode[c] = a * b
        p += 4

    # input
    elif inst == 3:
        a = intcode[p+1]

        intcode[a] = int(input('> '))
        p += 2

    # output
    elif inst == 4:
        a = intcode[p+1] if mode[0] else intcode[intcode[p+1]]

        print(a)
        p += 2

    elif inst == 99:
        break
