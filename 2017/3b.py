n = 265149

p = [0, 0]
coords = {str((0, 0)): 1}
d = 3

s = 1

while s < n:
    d = (d + 1) % 4

    if d == 0:
        x = 1
        y = 0
    
    if d == 1:
        x = 0
        y = 1

    if d == 2:
        x = -1
        y = 0

    if d == 3:
        x = 0
        y = -1
    
    if str((p[0] + x, p[1] + y)) in coords.keys():
        d -= 2

        if d < -1:
            d = 2
    else:
        p[0] += x
        p[1] += y

        s = 0

        for xx in [-1, 0, 1]:
            for yy in [-1, 0, 1]:
                if str((p[0] + xx, p[1] + yy)) in coords.keys():
                    s += coords[str((p[0] + xx, p[1] + yy))]

        coords[str((p[0], p[1]))] = s

print(s)
