import re

with open('25.txt', 'r') as f:
    txt = f.readlines()

state = 'A'
steps = 12399302
states = {}

l = 2

for i, s in enumerate('ABCDEF'):
    states[s] = []
    l += 2

    for j in range(2):
        t = []
        l += 1

        for k in range(3):
            if  k == 0:
                t.append(int(re.search('\d', txt[l]).group(0)))
            elif k == 1:
                if re.search('left', txt[l]):
                    t.append(-1)
                else:
                    t.append(1)
            else:
                t.append(txt[l].split()[-1][:-1])

            l += 1

        states[s].append(t)

tape = {}
cursor = 0

while steps:
    status = tape.setdefault(str(cursor), 0)

    t = states[state][status]

    tape[str(cursor)] = t[0]
    cursor += t[1]
    state = t[2]

    steps -= 1

print(sum(tape.values()))
