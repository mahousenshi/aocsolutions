with open('22.txt', 'r') as f:
    g = f.readlines()

grid = {}

for y, l in enumerate(g):
    for x, c in enumerate(l):
        if c == '#':
            grid[str([x, y])] = c

p = [12, 12]
d = 0
i = 0

for n in range(10000):
    s = str(p)
    status = grid.setdefault(s, '.')

    if status == '.':
        d = (d - 1) % 4
        grid[s] = '#'
        i += 1
    else:
        d = (d + 1) % 4
        grid[s] = '.'

    if d == 0:
        p[1] -= 1
    elif d == 1:
        p[0] += 1
    elif d == 2:
        p[1] += 1
    elif d == 3:
        p[0] -= 1

print(i)