# part b is to check how many primes between
# 108400 125400

n = 0

for i in range(108400, 125400 + 1, 17):
    f = 1

    for j in range(2, i):
        if i % j == 0:
            n += 1
            #composto
            f = 0
            break

print(n)
