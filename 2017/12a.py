def trans(n):
    for i in pipes[n]:
        if i not in nodes:
            nodes.append(i)
            trans(i)

pipes = {}

with open('12.txt', 'r') as f:
    lines = f.readlines()

for line in lines:
    line = line.split('<->')
    pipes[int(line[0])] = [int(i) for i in line[1].split(', ')]

nodes = [0]
trans(0)

print(len(nodes))
