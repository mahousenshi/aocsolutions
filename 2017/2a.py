s = 0

with open('2.txt', 'r') as f:
    lines = f.readlines()

for l in lines:
    l = [int(i) for i in l.split()]

    s += max(l) - min(l)

print(s)