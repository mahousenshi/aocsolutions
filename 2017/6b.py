e = [0, 5, 10, 0, 11, 14, 13, 4, 11, 8, 8, 7, 1, 4, 12, 11]

status = []

while len(status) == len(set(status)):
    m = max(e)
    i = e.index(m)

    e[i] = 0

    while m:
        i= (i + 1) % len(e)
        e[i] += 1
        m -= 1

    status.append(str(e))

print(len(status) - status.index(status[-1]) - 1)
