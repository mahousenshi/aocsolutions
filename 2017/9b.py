with open('9.txt', 'r') as f:
    line = f.read()

g = 0
e = 0

s = 0

for c in line:
    if e:
        e = 0
        continue

    if c == '<' and not g:
        g = 1
        continue

    if g:
        s += 1

        if c == '!':
            e = 1
            s -= 1

        if c == '>':
            g = 0
            s -= 1

print(s)
