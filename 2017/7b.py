import re

class Tree(object):
    def __init__(self, name):
        self.name = name
        self.value = 0
        self.branch = []

    def add(self, Tree):
        self.branch.append(Tree)

    def __str__(self):
        return self.name

    def __repr__(self):
        return str(self)

def searchBranch(name, tree):
    i = len(tree.branch) - 1

    while i + 1 and name != tree.branch[i].name:
        i -= 1

    return i

def searchTree(name, tree):
    if searchBranch(name, tree) + 1:
        return tree

    for b in tree.branch:
        t = searchTree(name, b)

        if t:
            return t

    return None

def addBranches(branches, tree):
    for branch in branches:
        i = searchBranch(branch, root)

        if i + 1:
            tree.add(root.branch.pop(i))
        else:
            tree.add(Tree(branch))

def sumTree(tree):
    t = []

    if len(tree.branch):
        for b in tree.branch:
            t.append(sumTree(b))

        if len(set(t)) != 1:
            print(tree.branch)
            print([j.value for j in tree.branch])
            print(t)

        return tree.value + sum(t)

    return tree.value

p = re.compile('(\w+) \((\d+)\)( -> (.*))?')

with open('7.txt', 'r') as f:
    lines = f.readlines()

root = Tree('root')
root.value = 0

for l in lines:
    l = p.search(l).groups()
    name = l[0]
    value = int(l[1])

    if l[3]:
        branches = l[3].split(', ')
    else:
        branches = []

    # Procura pela arvore para saber se galho ja esta instanciado
    t = searchTree(name, root)

    if t:
        i = searchBranch(name, t)

        t.branch[i].value = value
        addBranches(branches, t.branch[i])
    else:
        # O branch ainda é orfão
        b = Tree(name)

        b.value = value
        addBranches(branches, b)

        root.add(b)

sumTree(root)
