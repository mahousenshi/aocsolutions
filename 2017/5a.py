s = 0

with open('5.txt', 'r') as f:
    lines = f.readlines()

m = [int(i) for i in lines]
i = 0

while i >= 0 and i < len(m):
    m[i] += 1
    i += m[i] - 1
    
    s += 1

print(s)
