from math import sqrt
from textwrap import wrap

def rotate(s):
    r = ''
    if len(s) == 4:
        for i in [2, 3]:
            r += s[i::-2]
    else:
        for i in [6, 7, 8]:
            r += s[i::-3]

    return r

def trans(s):
    return s[2::-1] + s[5:2:-1] + s[:5:-1]

def iter(s):
    d = int(sqrt(len(s)))
    t = ''

    if d % 2:
        for i, a in enumerate(wrap(s, d)):
            if i % 3 == 0:
                aa = wrap(a, 3)
            elif i % 3 == 1:
                bb = wrap(a, 3)
            else:
                cc = wrap(a, 3)

                b = [rules[''.join(c)] for c in zip(aa, bb, cc)]
                for j in range(4):
                    for e in b:
                        t += e[4*j:4*(j+1)]
    else:
        for i, a in enumerate(wrap(s, d)):
            if i % 2 == 0:
                aa = wrap(a, 2)
            else:
                bb = wrap(a, 2)

                b = [rules[''.join(c)] for c in zip(aa, bb)]
                for j in range(3):
                    for e in b:
                        t += e[3*j:3*(j+1)]

    return t

with open('21.txt', 'r') as f:
    patterns = f.readlines()

rules = {}

for pattern in patterns:
    pattern = pattern.split('=>')

    a = pattern[0].replace('/', '').strip()
    b = pattern[1].replace('\n', '').replace('/', '').strip()
    s = a

    rules[a] = b

    for i in range(4):
        a = rotate(a)
        rules[a] = b

    if len(a) > 4:
        a = trans(a)
        s = a

        for i in range(4):
            a = rotate(a)
            rules[a] = b

s = '.#...####'

for i in range(5):
    s = iter(s)

print(s.count('#'))
