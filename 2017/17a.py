spins = 371
s = 0

buffer = [0]

for i in range(1, 2018):
    s = ((s + spins) % i) + 1
    buffer = buffer[:s] + [i] + buffer[s:]

print(buffer[s+1])
