with open('9.txt', 'r') as f:
    line = f.read()

g = 0
e = 0

l = 0
s = 0

for c in line:
    if e:
        e = 0
        continue

    if c == '<':
        g = 1
        continue

    if g:
        if c == '!':
            e = 1

        if c == '>':
            g = 0
    else:
        if c == '(':
            l += 1

        if c == ')':
            s += l
            l -= 1

print(s)
