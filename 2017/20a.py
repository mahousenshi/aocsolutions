import re

with open('20.txt', 'r') as f:
    swarm = f.readlines()

distance = []

r = re.compile('p=<(-?\d+),(-?\d+),(-?\d+)>, v=<(-?\d+),(-?\d+),(-?\d+)>, a=<(-?\d+),(-?\d+),(-?\d+)>')

for particle in swarm:
    particle = list(r.search(particle).groups())

    p = list(map(int, particle[:3]))
    v = list(map(int, particle[3:6]))
    a = list(map(int, particle[6:]))

    for i in range(1000):
        v = list(map(sum, zip(v, a)))
        p = list(map(sum, zip(p, v)))

    distance.append(sum(map(abs, p)))

print(distance.index(min(distance)))
