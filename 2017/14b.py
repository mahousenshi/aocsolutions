def knotharsh(inp):
    dx = [ord(i) for i in inp] + [17, 31, 73, 47, 23]

    l = [i for i in range(256)]

    s = 0
    i = 0

    for k in range(64):
        for d in dx:
            j = i + d

            l = l * 3
            l = (l[:i] + l[i:j][::-1] + l[j:])[256:]
            l = (l[:i] + l[i:j][::-1] + l[j:])[:256]

            i = (i + d + s) % 256
            s += 1

    h = ''

    for i in range(16):
        d = 0

        for j in l[i * 16:(i + 1) * 16]:
            d = d ^ j
        
        h += '{:0>2}'.format(hex(d)[2:])

    return h

def hex2bin(c):
    if c in 'abcdef':
        c = {'a': '10', 'b': '11', 'c': '12', 'd': '13', 'e': '14', 'f': '15'}[c]

    return '{:0>4b}'.format(int(c))

def trans(n):
    for i in pipes[n]:
        if i not in nodes:
            nodes.append(i)
            trans(i)

inp = 'hfdlxzhv'
s = ''
ls = []

for i in range(128):
    t = inp + '-' + str(i)
    h = knotharsh(t)

    for c in h:
        s += hex2bin(c)

    ls.append(s)

i = 0
j = 0

pipes = {}

for c in s:
    if c == '1':
        n = i * 128 + j
        g = []

        if i and s[n - 128] == '1':
            g.append(n - 128)

        if j and s[n - 1] == '1':
            g.append(n - 1)

        if j != 127 and s[n + 1] == '1':
            g.append(n + 1)

        if i != 127 and s[n + 128] == '1':
            g.append(n + 128)

        pipes[n] = g

    j += 1

    if j == 128:
        j = 0
        i += 1

nodes = []
su = 0

for i in pipes.keys():
    if i not in nodes:
        su += 1
        trans(i)

print(su)
