n = 265149

p = [0, 0]
coords = [(0,0)]
d = 3

while len(coords) < n:
    d = (d + 1) % 4

    if d == 0:
        x = 1
        y = 0
    
    if d == 1:
        x = 0
        y = 1

    if d == 2:
        x = -1
        y = 0

    if d == 3:
        x = 0
        y = -1
    
    if (p[0] + x, p[1] + y) in coords:
        d -= 2

        if d < -1:
            d = 2

    else:
        p[0] += x
        p[1] += y

        coords.append(tuple(p))

print(p)
