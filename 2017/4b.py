s = 0

with open('4.txt', 'r') as f:
    lines = f.readlines()

for l in lines:
    l = [''.join(i) for i in map(sorted, l.split())]

    if len(l) == len(set(l)):
        s += 1

print(s)
