from collections import defaultdict

def safeIsDigit(s):
    if s[-1].isdigit():
        return int(s)
    else:
        return registers[s]

with open('18.txt', 'r') as f:
    instructions = f.readlines()

registers = defaultdict(int)

n = 0
played = []

while n < len(instructions):
    instruction = instructions[n].strip().split()

    if 'snd' in instruction:
        a = safeIsDigit(instruction[1])

        played.append(a)

    if 'set' in instruction:
        a = instruction[1]
        b = safeIsDigit(instruction[2])

        registers[a] = b

    if 'add' in instruction:
        a = instruction[1]
        b = safeIsDigit(instruction[2])

        registers[a] += b

    if 'mul' in instruction:
        a = instruction[1]
        b = int(instruction[2])

        registers[a] *= b

    if 'mod' in instruction:
        a = instruction[1]
        b = safeIsDigit(instruction[2])

        registers[a] %= b
    
    if 'rcv' in instruction:
        a = instruction[1]
        
        if registers[a]:
            print(played[-1])
            exit()

    if 'jgz' in instruction:
        a = safeIsDigit(instruction[1])
        b = safeIsDigit(instruction[2])

        if a:
            n += b
            continue

    n += 1
