def search(port, brigdes, power):
    choices = []

    for brigde in brigdes:
        if port in brigde:
            choices.append(brigde)

    if len(choices):
        for choice in choices:
            nbrigdes = brigdes[:]
            nbrigdes.pop(nbrigdes.index(choice))

            if choice.index(port):
                nport = choice[0]
            else:
                nport = choice[1]

            search(nport, nbrigdes, power + port + nport)
    else:
        vals.append(power)

with open('24.txt', 'r') as f:
    b = [list(map(int, i.split('/'))) for i in f.readlines()]

vals = []

search(0, b, 0)

print(max(vals))
