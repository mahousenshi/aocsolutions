with open('8.txt', 'r') as f:
    lines = f.readlines()

registers = {}

for line in lines:
    line = line.split()

    a = registers.setdefault(line[0], 0)
    b = registers.setdefault(line[4], 0)

    f = 0

    if line[5] == '>':
        if b > int(line[6]):
            f = 1

    if line[5] == '<':
        if b < int(line[6]):
            f = 1

    if line[5] == '>=':
        if b >= int(line[6]):
            f = 1

    if line[5] == '<=':
        if b <= int(line[6]):
            f = 1

    if line[5] == '==':
        if b == int(line[6]):
            f = 1

    if line[5] == '!=':
        if b != int(line[6]):
            f = 1

    if f:
        if line[1] == 'inc':
            registers[line[0]] += int(line[2])
        else:
            registers[line[0]] -= int(line[2])
    
print(max(registers.values()))
