def trans(n):
    for i in pipes[n]:
        if i not in nodes:
            nodes.append(i)
            trans(i)

pipes = {}

with open('12.txt', 'r') as f:
    lines = f.readlines()

for line in lines:
    line = line.split('<->')
    pipes[int(line[0])] = [int(i) for i in line[1].split(', ')]

nodes = []
s = 0

for i in pipes.keys():
    if i not in nodes:
        s += 1
        trans(i)

print(s)
