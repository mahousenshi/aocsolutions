
with open('13.txt', 'r') as f:
    lines = f.readlines()

layers = {}

for line in lines:
    [a, b] = line.split(':')

    layers[int(a)] = int(b)

d = 0
f = 1

while f:
    f = 0

    for layer in layers.keys():
        if (layer + d) % (layers[layer] * 2 - 2) == 0:
            f = 1
            break
    
    if f:
        d += 1
    
print(d)
