import itertools

s1 = 0
s2 = 0

with open('2.txt', 'r') as f:
    lines = f.readlines()

for l in lines:
    l = [int(i) for i in l.split()]

    s1 += max(l) - min(l)

    comb = itertools.combinations(l, 2)

    for (a, b) in comb:
        if b > a:
            c = b
            b = a
            a = c

        if a % b == 0:
            print(a, b)
            s2 += a // b
            continue

print(s)