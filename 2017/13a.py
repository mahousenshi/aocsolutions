with open('13.txt', 'r') as f:
    lines = f.readlines()

s = 0

for line in lines:
    [a, b] = line.split(':')

    if int(a) % (int(b) * 2 - 2) == 0:
        s += int(a) * int(b)

print(s)
