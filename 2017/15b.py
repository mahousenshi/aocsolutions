#Generator A starts with 512
#Generator B starts with 191

a = 512
b = 191

fa = 16807
fb = 48271

j = 0

for i in range(5000000):
    a = (a * fa) % 2147483647
    while a % 4:
        a = (a * fa) % 2147483647
    
    b = (b * fb) % 2147483647
    while b % 8:
        b = (b * fb) % 2147483647

    ba = '{:0>16b}'.format(a)
    bb = '{:0>16b}'.format(b)

    if ba[-16:] == bb[-16:]:
        j += 1

print(j)
