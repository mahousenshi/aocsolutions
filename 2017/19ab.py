with open('19.txt', 'r') as f:
    labyrinth = f.read()

labyrinth = labyrinth.replace('\n', ' ')

n = labyrinth.find('|') + 202
d = 3

# 1 
#4 2 
# 3

s = ''
t = 1

while d + 1:
    if d == 1:
        while labyrinth[n] != '+':
            if labyrinth[n] == ' ':
                d = -1
                break

            if labyrinth[n] not in '|+-':
                s += labyrinth[n]

            t += 1
            n -= 202

        if d + 1:
            if labyrinth[n + 1] != ' ':
                d = 2
                n += 1
            else:
                d = 4
                n -= 1

            t += 1

    if d == 2:
        while labyrinth[n] != '+':
            if labyrinth[n] == ' ':
                d = -1
                break

            if labyrinth[n] not in '|+-':
                s += labyrinth[n]

            t += 1
            n += 1

        if d + 1:
            if labyrinth[n - 202] != ' ':
                d = 1
                n -= 202
            else:
                d = 3
                n += 202

            t += 1

    if d == 3:
        while labyrinth[n] != '+' and d + 1:
            if labyrinth[n] == ' ':
                d = -1
                break

            if labyrinth[n] not in '|+-':
                s += labyrinth[n]

            t += 1
            n += 202

        if d + 1:
            if labyrinth[n + 1] != ' ':
                d = 2
                n += 1
            else:
                d = 4
                n -= 1

            t += 1

    if d == 4:
        while labyrinth[n] != '+' and d + 1:
            if labyrinth[n] == ' ':
                d = -1
                break

            if labyrinth[n] not in '|+-':
                s += labyrinth[n]

            t += 1
            n -= 1

        if d + 1:
            if labyrinth[n - 202] != ' ':
                d = 1
                n -= 202
            else:
                d = 3
                n += 202

            t += 1

print(s)
print(t)