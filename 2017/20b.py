import re

class Particle(object):
    def __init__(self, p, v, a):
        self.position = p
        self.velocity = v
        self.acceleration = a
        self.status = 1

    def update(self):
        self.velocity = list(map(sum, zip(self.velocity, self.acceleration)))
        self.position = list(map(sum, zip(self.position, self.velocity)))

with open('20.txt', 'r') as f:
    swarm = f.readlines()

r = re.compile('p=<(-?\d+),(-?\d+),(-?\d+)>, v=<(-?\d+),(-?\d+),(-?\d+)>, a=<(-?\d+),(-?\d+),(-?\d+)>')

particules = []

for particle in swarm:
    particle = list(r.search(particle).groups())

    p = list(map(int, particle[:3]))
    v = list(map(int, particle[3:6]))
    a = list(map(int, particle[6:]))

    particules.append(Particle(p, v, a))

while True:
    # Collision checks
    for i, p1 in enumerate(particules):
        if p1.status:
            for j, p2 in enumerate(particules):
                if p2.status and p1.position == p2.position and i != j:
                    p1.status = 0
                    p2.status = 0

    c = 0
    for p in particules:
        if p.status:
            p.update()
            c += 1

    print(c)
