from collections import defaultdict
import asyncio
from asyncio.queues import Queue

with open('18.txt', 'r') as f:
    instructions = f.readlines()

c = [0, 0]

async def core(inst, process, send, recv):
    registers = defaultdict(int)
    registers['p'] = process
    n = 0

    def val(s):
        try:
            return int(s)
        except Exception:
            return registers[s]

    while n < len(inst):
        inst = inst[n].strip().split()

        if 'snd' in inst:
            a = val(inst[1])

            send.put_nowait(a)
            c[process] += 1

        if 'set' in inst:
            a = inst[1]
            b = val(inst[2])

            registers[a] = b

        if 'add' in inst:
            a = inst[1]
            b = val(inst[2])

            registers[a] += b

        if 'mul' in inst:
            a = inst[1]
            b = int(inst[2])

            registers[a] *= b

        if 'mod' in inst:
            a = inst[1]
            b = val(inst[2])

            registers[a] %= b

        if 'rcv' in inst:
            a = inst[1]

            registers[a] = await recv.get()

        if 'jgz' in inst:
            a = val(inst[1])
            b = val(inst[2])

            if a:
                n += b
                continue

        n += 1

async def deadlock(q1, q2):
    while not q1.empty() or not q2.empty():
        await asyncio.sleep(1)

loop = asyncio.get_event_loop()

q0 = Queue()
q1 = Queue()

t0 = loop.create_task(core(instructions, 0, q1, q0))
t1 = loop.create_task(core(instructions, 1, q0, q1))

loop.run_until_complete(loop.create_task(deadlock(q0, q1)))

print(c)
