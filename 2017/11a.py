def addCord(a, b):
    return list(map(sum, zip(a,b)))

with open('11.txt', 'r') as f:
    line = f.read().split(',')

coord = [0,0,0]

dirs = {
    'n': [-1, 1 , 0],
    'ne': [0, 1, -1],
    'se': [1, 0, -1],
    's': [1, -1 , 0],
    'sw': [0, -1 , 1],
    'nw': [-1, 0, 1]
}

for c in line:
    coord = addCord(coord, dirs[c])

print(sum(map(abs, coord))//2)
