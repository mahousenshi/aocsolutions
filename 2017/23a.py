from collections import defaultdict

def safeIsDigit(s):
    if s[-1].isdigit():
        return int(s)
    else:
        return registers[s]

with open('23.txt', 'r') as f:
    instructions = f.readlines()

registers = defaultdict(int)

n = 0
m = 0

while n < len(instructions):
    instruction = instructions[n].strip().split()

    if 'set' in instruction:
        a = instruction[1]
        b = safeIsDigit(instruction[2])

        registers[a] = b

    if 'sub' in instruction:
        a = instruction[1]
        b = safeIsDigit(instruction[2])

        registers[a] -= b

    if 'mul' in instruction:
        m += 1
        a = instruction[1]
        b = safeIsDigit(instruction[2])

        registers[a] *= b

    if 'jnz' in instruction:
        a = safeIsDigit(instruction[1])
        b = safeIsDigit(instruction[2])

        if a:
            n += b
            continue

    n += 1

print(m)
