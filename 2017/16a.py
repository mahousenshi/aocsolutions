def spin(s, n):
    return s[-n:] + s[:-n]

def partner(s, a, b):
    return s.replace(a, 'x').replace(b, 'y' ).replace('x', b).replace('y', a)

def exchange(s, a, b):
    return partner(s, s[a], s[b])

with open('16.txt', 'r') as f:
    moves = f.read().split(',')

alpha = 'abcdefghijklmnop'

for m in moves:
    if m[0] == 's':
        alpha = spin(alpha, int(m[1:]))

    if m[0] == 'x':
        [a, b] = list(map(int, m[1:].split('/')))
        alpha = exchange(alpha, a, b)

    if m[0] == 'p':
        alpha = partner(alpha, m[1], m[3])

print(alpha)
