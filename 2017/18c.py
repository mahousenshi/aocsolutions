from collections import defaultdict
import asyncio
from asyncio.queues import Queue

with open('18.txt', 'r') as fh:
    gameInput = [line.split() for line in fh.readlines()]

count ={0:0, 1:0}

async def prog(gameInput, process, send, recv):

    reg = defaultdict(int)
    reg['p'] = process
    pc = 0

    def val(value):
        try:
            return int(value)
        except Exception:
            return int(reg[value])

    while 0<=pc<len(gameInput):
        inst = gameInput[pc]
        x = val(inst[1])
        if inst[0] == 'snd':
            send.put_nowait(x)
            count[process] += 1
        elif inst[0] == 'rcv':
            reg[inst[1]] = await recv.get()
        else:
            y = val(inst[2])
            if inst[0] == 'set':
                reg[inst[1]] = y
            elif inst[0] == 'add':
                reg[inst[1]] += y
            elif inst[0] == 'mul':
                reg[inst[1]] *= y
            elif inst[0] == 'mod':
                reg[inst[1]] %= y
            elif inst[0] == 'jgz':
                if x > 0:
                    pc += y
                    continue
        pc += 1


async def block_detect(q1, q2):
    while not q1.empty() or not q2.empty():
        await asyncio.sleep(1)



loop = asyncio.get_event_loop()

q0 = Queue()
q1 = Queue()

f0 = prog(gameInput, 0, q1, q0)
f1 = prog(gameInput, 1, q0, q1)

t0 = loop.create_task(f0)
t1 = loop.create_task(f1)
x = loop.create_task(block_detect(q0, q1))

loop.run_until_complete(x)
print(count)