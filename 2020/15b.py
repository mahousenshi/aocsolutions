inp = [0,3,6]

turns = {n: t for t, n in enumerate(inp[:-1], start=1)}
spoken = inp[-1]

triplets = [3, 1, 16]
cycle = [0,3,6]

for turn in range(len(inp)+1, 200):
    if spoken in turns:
        last_spoken = turns[spoken]
        turns[spoken] = turn - 1
        spoken = turns[spoken] - last_spoken

    else:
        turns[spoken] = turn - 1
        spoken = 0

    cycle.append(spoken)

print(cycle)
