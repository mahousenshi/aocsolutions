from collections import defaultdict

with open('24.txt', 'r') as f:
    lines = [l.strip() for l in f.readlines()]

rose ={
    'ne': ( 1,  0, -1),
    'e':  ( 1, -1,  0),
    'se': ( 0, -1,  1),
    'sw': (-1,  0,  1),
    'w':  (-1,  1,  0),
    'nw': ( 0,  1, -1),
}

floor = defaultdict(int)

for line in lines:
    title = (0, 0, 0)
    sn = ''

    for c in line:
        if c in ['s', 'n']:
            sn = c

        else:
            title = tuple(a + b for a, b in zip(title, rose[f'{sn}{c}']))
            sn = ''

    floor[title] = 0 if floor[title] else 1

print(sum(floor.values()))