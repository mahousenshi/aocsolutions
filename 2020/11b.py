from copy import deepcopy

def print_grid(grid):
    for l in grid:
        print(''.join(l))

with open('11.txt', 'r') as f:
    new = [list(l.strip()) for l in f.readlines()]

h = len(new)
w = len(new[0])

c = 0
while True:
    old = deepcopy(new)

    for x in range(0, h):
        for y in range(0, w):
            if old[x][y] == '.':
                continue

            vision = ''

            for dx, dy in [(-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)]:
                i = 1
                while x+(dx*i) >= 0 and x+(dx*i) < h and y+(dy*i) >= 0 and y+(dy*i) < w:
                    if old[x+(dx*i)][y+(dy*i)] != '.':
                        vision += old[x+(dx*i)][y+(dy*i)]
                        break
                    i += 1

            if old[x][y] == 'L' and vision.count('#') == 0:
                new[x][y] = '#'

            elif old[x][y] == '#' and vision.count('#') >= 5:
                new[x][y] = 'L'

    if new == old:
        break

print(sum(line.count('#') for line in new))