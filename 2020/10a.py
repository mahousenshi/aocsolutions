with open('10.txt', 'r') as f:
    numbers = [0] + sorted([int(l) for l in f.readlines()])

numbers.append(max(numbers) + 3)
ones = 0
threes = 0

for i in range(1, len(numbers)):
    if numbers[i] - numbers[i-1] == 1
        ones += 1
    else:
        threes += 1

print(ones * threes)