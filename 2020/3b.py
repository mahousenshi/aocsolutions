import re

with open('3.txt', 'r') as f:
    lines = [l.strip() for l in f.readlines()]

width = len(lines[0])
slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]

total = 1

for dx, dy in slopes:
    x = -dx
    trees = 0

    for line in lines[::dy]:
        x = (x + dx) % width

        if line[x] == '#':
            trees += 1

    total *= trees

print(total)