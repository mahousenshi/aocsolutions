from collections import defaultdict

with open('10.txt', 'r') as f:
    numbers = [0] + sorted([int(l) for l in f.readlines()])

limit = max(numbers) + 3
numbers.append(limit)

nodes = defaultdict(list)
poss = defaultdict(int)

for n in numbers:
    p = 0
    if n+1 in numbers:
        nodes[str(n)].append(str(n+1))
        p += 1

    if n+2 in numbers:
        nodes[str(n)].append(str(n+2))
        p += 1

    if n+3 in numbers:
        nodes[str(n)].append(str(n+3))
        p += 1

    poss[str(n)] = p

replace = []
replace_for = []
for k in nodes:
    if len(nodes[k]) == 1:
        replace.append(k)
        replace_for.append(nodes[k][0])

for r, rf in zip(replace, replace_for):

    for k in nodes:
        if r in nodes[k]:
            for i, e in enumerate(nodes[k]):
                if nodes[k][i] == r:
                    nodes[k][i] = rf

    del nodes[r]

print(nodes)

def count_branches(node, porra):
    for n in node:
        if n == str(limit):
            porra += 1

        else:
            porra = count_branches(nodes[n], porra)

    return porra

porra = count_branches(nodes['0'], porra)
print(porra)