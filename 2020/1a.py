from itertools import combinations

with open('1.txt', 'r') as f:
    numbers = sorted([int(n) for n in f.readlines()])

for a, b in combinations(numbers, 2):
    if a + b == 2020:
        print(a, b)
        print(a * b)
        break