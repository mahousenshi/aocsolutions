from collections import defaultdict
import re

def bag_search(aaaa):
    for bag in aaaa:
        if bag not in shiny:
            shiny.append(bag)
            bag_search(bags[bag])

with open('7.txt', 'r') as f:
    rules = [l.strip() for l in f.readlines()]

bags = defaultdict(list)

for rule in rules:
    bag = re.match(r'(.*) bags contain (.*)', rule)
    
    color = bag.group(1)

    if bag.group(2) != 'no other bags.':
        for insides in bag.group(2)[:-1].split(', '):
            inside = re.match(r'\d (.*) bag(s|)', insides)

            bags[inside.group(1)].append(color)

shiny = []
bag_search(bags['shiny gold'])

print(len(shiny))
