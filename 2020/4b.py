import re

with open('4.txt', 'r') as f:
    passaports = f.read().split('\n\n')

def validate(key, value):
    if key == 'byr':
        if 1920 <= int(value) <= 2002:
            return(True)

    elif key == 'iyr':
        if 2010 <= int(value) <= 2020:
            return(True)

    elif key == 'eyr':
        if 2020 <= int(value) <= 2030:
            return(True)

    elif key == 'hgt':
        val = re.match(r'(\d+)(in|cm)', value)

        if val:
            value = int(val.group(1))
            unit = val.group(2)

            if unit == 'cm':
                if 150 <= value <= 193:
                    return(True)

            elif unit == 'in':
                if 59 <= value <= 76:
                    return(True)

    elif key == 'hcl':
        if re.match(r'^#[a-f0-9]{6}$', value):
            print(value)
            return(True)

    elif key == 'ecl':
        if value in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
            return(True)

    elif key == 'pid':
        if re.match(r'^[0-9]{9}$', value):
            return(True)

    return(False)

valid_passports = 0

for passaport in passaports:
    valid_passports += 1 if sum([1 for prop in passaport.split() if validate(prop[:3], prop[4:])]) == 7 else 0

print(valid_passports)