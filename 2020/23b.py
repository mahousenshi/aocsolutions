class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
        self.prev = None

def search_node(data, node):
    while node.data != data:
        node = node.prev

    return node

def print_nodes(node):
    data = [node.data]
    node = node.prev

    while node.data != data[0]:
        data.append(node.data)
        node = node.prev

    print(' -> '.join(str(n) for n in data))

inp = '952438716'

temp = Node(0)
old = temp

for cup in [int(n) for n in inp] + list(range(10,1000001)):
    new = Node(cup)
    new.prev = old 
    old.next = new
    old = new

old.next = temp.next
temp.next.prev = old

node = temp.next
for i in range(10000000):
    current = node.data

    picks = [node.next.data, node.next.next.data, node.next.next.next.data]

    destination = current - 1 if current != 1 else 1000000
    while destination in picks:
        destination = destination - 1 if destination != 1 else 1000000

    dest = search_node(destination, node)
    pick0 = node.next
    pick2 = node.next.next.next

    node.next = pick2.next
    pick2.next = dest.next
    dest.next = pick0
    
    node = node.next

final = search_node(1, node)
print(final.next.data * final.next.next.data)
