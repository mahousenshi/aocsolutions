with open('13.txt', 'r') as f:
    timestamp, bus_ids = f.readlines()

timestamp = int(timestamp)
bus_ids = [int(bus_id) for bus_id in bus_ids.split(',') if bus_id.isdigit()]

wait = {}

for bus_id in bus_ids:
    wait[bus_id - (timestamp % bus_id)] = bus_id

print(min(wait) * wait[min(wait)])