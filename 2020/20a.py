import re

with open('20.txt', 'r') as f:
    lines = [l.strip() for l in f.readlines()]

squares = {}

for line in lines:
    code_r = re.match(r'Tile (\d+):', line)

    if code_r is None:
        if line:
            squares[code].append(line)
    
    else:
        code = code_r.group(1)
        squares[code] = []

print(len(squares))