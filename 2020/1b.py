from itertools import combinations

with open('1.txt', 'r') as f:
    numbers = sorted([int(n) for n in f.readlines()])

for a, b, c in combinations(numbers, 3):
    if a + b + c == 2020:
        print(a, b, c)
        print(a * b * c)
        break