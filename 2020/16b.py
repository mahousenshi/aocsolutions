import re

with open('16fields.txt', 'r') as f:
    lines = [l.strip() for l in f.readlines()]

rules = {}

for line in lines:
    field, s1, e1, s2, e2 = re.match(r'(.*): (\d+)-(\d+) or (\d+)-(\d+)', line).groups()
    rules[field] = list(range(int(s1), int(e1)+1)) + list(range(int(s2), int(e2)+1))

with open('16nearby.txt', 'r') as f:
    numbers = list(set([int(l) for l in f.read().replace('\n', ',').split(',')]))

invalid_numbers = []

for n in numbers:
    if any(n in rules[rule] for rule in rules):
        continue
    else:
        invalid_numbers.append(n)

with open('16nearby.txt', 'r') as f:
    tickets = [l.strip() for l in f.readlines()]

valid_tickets = []

for ticket in tickets:
    ticket = [int(n) for n in ticket.split(',')]

    if any(n in invalid_numbers for n in ticket):
        pass
    else:
        valid_tickets.append(ticket)

# transpose
valid_tickets_t = [list(i) for i in zip(*valid_tickets)]
unknow_fields = list(rules.keys())

matrix = []
for j, numbers in enumerate(valid_tickets_t):
    line = []
    for field in unknow_fields:

        f = 0
        if all(n in rules[field] for n in numbers):
            f = 1

        line.append(f)

    matrix.append(line)

real_fields = ['' for _ in range(20)]

while sum(sum(line) for line in matrix):
    for l, line in enumerate(matrix):
        if sum(line) == 1:
            n = line.index(1)

            real_fields[l] = unknow_fields[n]
            break

    for line in matrix:
        line[n] = 0

my_ticket = valid_tickets[-1]

n = 1
for rules, number in zip(real_fields, my_ticket):
    if rules.startswith('departure'):
        n *= number

print(n)