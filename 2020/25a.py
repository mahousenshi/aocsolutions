with open('25.txt', 'r') as f:
    card_pk, door_pk = [int(n) for n in f.readlines()]

def loops_size(pk):
    loops = 1
    sb = 7
    while sb != pk:
        sb = (sb * 7) % 20201227
        loops += 1
    
    return loops

def transform(pk, loops):
    val = 1
    for _ in range(loops):
        val = (val * pk) % 20201227
    
    return val

print(transform(card_pk, loops_size(door_pk)))
