from collections import deque

with open('22.txt', 'r') as f:
    decks = f.read().split('\n\n')

p1 = deque([int(n) for n in decks[0].split('\n')[:0:-1]])
p2 = deque([int(n) for n in decks[1].split('\n')[:0:-1]])

while len(p1) and len(p2):
    if p1[-1] > p2[-1]:
        p1.rotate()
        p1.appendleft(p2.pop())
    
    else:
        p2.rotate()
        p2.appendleft(p1.pop())

if p1:
    print(sum(n*i for i, n in enumerate(p1, start=1)))
else:
    print(sum(n*i for i, n in enumerate(p2, start=1)))
