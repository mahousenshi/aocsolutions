from itertools import combinations

with open('9.txt', 'r') as f:
    numbers = [int(l) for l in f.readlines()]

for i, n in enumerate(numbers[25:]):
    for a, b in combinations(sorted(numbers[i:i+25]), 2):
        if a + b == n:
            break
    
    else:
        invalid = n
        break

f = 0
for r in range(2, len(numbers)):
    for i in range(0, len(numbers) - r + 1):
        if sum(numbers[i:i+r]) == invalid:
            print(min(numbers[i:i+r]) + max(numbers[i:i+r]))
            f = 1
            break
    
    if f:
        break