from collections import Counter
import re

with open('16fields.txt', 'r') as f:
    lines = [l.strip() for l in f.readlines()]

rules = {}

for line in lines:
    field, s1, e1, s2, e2 = re.match(r'(.*): (\d+)-(\d+) or (\d+)-(\d+)', line).groups()
    rules[field] = list(range(int(s1), int(e1)+1)) + list(range(int(s2), int(e2)+1))

with open('16nearby.txt', 'r') as f:
    numbers = Counter(int(l) for l in f.read().replace('\n', ',').split(','))

invalid_numbers = []

for n in numbers:
    if any(n in rules[rule] for rule in rules):
        continue
    else:
        invalid_numbers.append(n)

print(sum(n * numbers[n] for n in invalid_numbers))
