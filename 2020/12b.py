import re
from collections import deque

with open('12.txt', 'r') as f:
    route = [l.strip() for l in f.readlines()]

waypoint = deque([1, 10, 0, 0])

rose = {'N': 0, 'E': 1, 'S': 2, 'W': 3}
position = [0, 0, 0, 0]

for action in route:
    act, qtd = re.match(r'(\w)(\d+)', action).groups()

    if act in rose:
        waypoint[rose[act]] += qtd

    if act == 'F':
        for i in range(4):
            position[i] += waypoint[i] * qtd 

    if act == 'R':
        waypoint.rotate(qtd // 90)
    
    if act == 'L':
        waypoint.rotate(-qtd // 90)
    
print(abs(position[0]-position[2]) + abs(position[1]-position[3]))
