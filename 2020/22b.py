from collections import deque

with open('22.txt', 'r') as f:
    decks = f.read().split('\n\n')

p1 = deque([int(n) for n in decks[0].split('\n')[:0:-1]])
p2 = deque([int(n) for n in decks[1].split('\n')[:0:-1]])

def combat(p1, p2):
    # Start game
    decks = []

    r = 1
    while len(p1) and len(p2):
        # anti inf loop
        if p1+p2 in decks:
            p2 = []

        else:
            decks.append(p1+p2)
            c1 = p1.pop()
            c2 = p2.pop()

            if max(c1, c2) > min(len(p1), len(p2)):
                if c1 > c2:
                    p1.appendleft(c1)
                    p1.appendleft(c2)

                else:
                    p2.appendleft(c2)
                    p2.appendleft(c1)

            else:
                np1, _ = combat(p1.copy(), p2.copy())

                if np1:
                    p1.appendleft(c1)
                    p1.appendleft(c2)

                else:
                    p2.appendleft(c2)
                    p2.appendleft(c1)
        r += 1
    return p1, p2

np1, np2 = combat(p1, p2)

if np1:
    print(sum(n*i for i, n in enumerate(np1, start=1)))
else:
    print(sum(n*i for i, n in enumerate(np2, start=1)))
