import re

with open('3.txt', 'r') as f:
    lines = [l.strip() for l in f.readlines()]

width = len(lines[0])

x = -3
trees = 0

for line in lines:
    x = (x + 3) % width

    if line[x] == '#':
        trees += 1

print(trees)