import re

def bitmask(binary, mask):
    new_binary = ''
    masked = ''

    for m, b in zip(mask, binary):
        if m == '0':
            new_binary += b
            masked += '0'
        
        if m == '1':
            new_binary += '1'
            masked += '0'
        
        if m == 'X':
            new_binary += '0'
            masked += 'X'
    
    return new_binary, masked

with open('14.txt', 'r') as f:
    lines = [l.strip() for l in f.readlines()]

mem = {}

for line in lines:
    mask_m = re.match(r'mask = (.*)', line)
    mem_m = re.match(r'mem\[(\d+)\] = (\d+)', line)

    if mask_m:
        mask = mask_m.group(1)

    else:
        floating, masked = bitmask(f'{int(mem_m.group(1)):>036b}', mask)      

        #mem[mem_m.group(1)] = bitmask(f'{int(mem_m.group(2)):>036b}', mask)

#print(sum(mem.values()))