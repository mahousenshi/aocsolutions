with open('5.txt', 'r') as f:
    lines = [l.strip() for l in f.readlines()]

plane = ['1' for r in range(128 * 8)]

for line in lines:
    plane[int(line[:-3].replace('F', '0').replace('B', '1'), 2) * 8 + int(line[-3:].replace('L', '0').replace('R', '1'), 2)] = '0'

print(''.join(plane).find('010') + 1)