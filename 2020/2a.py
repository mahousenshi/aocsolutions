import re

with open('2.txt', 'r') as f:
    pwds = f.readlines()

valids_pwd = 0
for pwd in pwds:
    pwd_m = re.match(r'(\d+)-(\d+) (\w): (\w+)', pwd)

    low, high, letter, pw = pwd_m.groups()
    
    times = pw.count(letter)

    if int(low) <= times <= int(high):
        valids_pwd += 1

print(valids_pwd)