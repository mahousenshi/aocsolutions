import re

with open('21.txt', 'r') as f:
    lines = [l.strip() for l in f.readlines()]

allergens_d = {}

for line in lines:
    ingredients, allergens = re.search(r'(.*)\(contains (.*)\)', line).groups()

    ingredients = set(ingredients.split())

    for allergen in allergens.split(', '):
        if allergen in allergens_d:
            allergens_d[allergen] = ingredients.intersection(allergens_d[allergen])

        else:
            allergens_d[allergen] = ingredients

allergens_final = {}

while allergens_d:
    for allergen in allergens_d:
        if len(allergens_d[allergen]) == 1:
            ingredient = allergens_d.pop(allergen)
            allergens_final[allergen] = list(ingredient)[0]
            break

    for allergen in allergens_d:
        allergens_d[allergen] = allergens_d[allergen].difference(ingredient)

print(','.join(allergens_final[key] for key in sorted(allergens_final)))