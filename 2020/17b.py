with open('17.txt', 'r') as f:
    lines = [list(l.strip()) for l in f.readlines()]

def neighbors(point):
    x, y, z, w = point
    points = []
    
    for dx in [-1, 0, 1]:
        for dy in [-1, 0, 1]:
            for dz in [-1, 0, 1]:
                for dw in [-1, 0, 1]:
                    points.append((x+dx, y+dy, z+dz, w+dw))

    return points

actives = []

for x, line in enumerate(lines):
    for y, c in enumerate(line):
        if c == '#':
            actives.append((x, y, 0, 0))

for _ in range(6):
    candidates = []

    for point in actives:
        candidates += neighbors(point)
       
    candidates = list(set(candidates))

    new_actives = []
    for point in candidates:
        active = 0

        for npoint in neighbors(point):
            if npoint != point and npoint in actives:
                active += 1
        
        if point in actives and (active == 2 or active == 3):
            new_actives.append(point)

        if point not in actives and active == 3:
            new_actives.append(point)

    actives = new_actives
    print(len(actives))
