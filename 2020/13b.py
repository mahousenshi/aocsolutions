from math import gcd
from itertools import combinations

with open('13.txt', 'r') as f:
    bus_ids = [int(bus_id) for bus_id in f.readlines()[1].split(',') if bus_id.isdigit()]

print(bus_ids)

for a, b in combinations(bus_ids, 2):
    print(gcd(a,b))
