from itertools import combinations

with open('9.txt', 'r') as f:
    numbers = [int(l) for l in f.readlines()]

for i, n in enumerate(numbers[25:]):
    for a, b in combinations(sorted(numbers[i:i+25]), 2):
        if a + b == n:
            break
    
    else:
        print(n)
        break
