import re

def toggle(line):
    if 'nop' in line:
        line = line.replace('nop', 'jmp')
    
    elif 'jmp' in line:
        line = line.replace('jmp', 'nop')

    return line

def execute(program):
    pointer = 0
    accumulator = 0
    status = 1

    visited = []

    while pointer < len(program):
        if pointer in visited:
            status = 0
            break
        else:
            visited.append(pointer)

        comm = re.match(r'(\w+) ((\+|-)(\d+))', program[pointer])

        if comm.group(1) == 'acc':
            accumulator += int(comm.group(2))
            pointer += 1

        elif comm.group(1) == 'nop':
            pointer += 1

        elif comm.group(1) == 'jmp':
            pointer += int(comm.group(2))

    return accumulator, status

with open('8.txt', 'r') as f:
    program = [l.strip() for l in f.readlines()]

gold = program.copy()

for i, line in enumerate(program):
    program[i] = toggle(line)
    
    if program[i] != gold[i]
        accumulator, status = execute(gold)
        
        if status:
            print(accumulator)
            break

        program[i] = toggle(line)