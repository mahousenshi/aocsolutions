from collections import defaultdict

def neighbors(title):
    neighborhood = []

    for neighbor in [(1, 0, -1), (1, -1, 0), (0, -1, 1), (-1, 0, 1), (-1, 1, 0), (0, 1, -1)]:
        neighborhood.append(tuple(a + b for a, b in zip(title, neighbor)))

    return neighborhood

with open('24.txt', 'r') as f:
    lines = [l.strip() for l in f.readlines()]

rose ={
    'ne': ( 1,  0, -1),
    'e':  ( 1, -1,  0),
    'se': ( 0, -1,  1),
    'sw': (-1,  0,  1),
    'w':  (-1,  1,  0),
    'nw': ( 0,  1, -1),
}

floor = defaultdict(int)

for line in lines:
    title = [0, 0, 0]
    sn = ''

    for c in line:
        if c in ['s', 'n']:
            sn = c

        else:
            title = tuple(a + b for a, b in zip(title, rose[f'{sn}{c}']))
            sn = ''

    floor[title] = 0 if floor[title] else 1

blacks = [title for title in floor if floor[title]]
for _ in range(100):
    candidates = []
    for title in blacks:
        candidates += neighbors(title)

    candidates = list(set(candidates + blacks))

    new_blacks = []
    for title in candidates:
        black = 0

        for ntitle in neighbors(title):
            if ntitle in blacks:
                black += 1

        if title in blacks and (black in [1, 2]):
            new_blacks.append(title)

        if title not in blacks and black == 2:
            new_blacks.append(title)

    blacks = new_blacks

print(len(blacks))
