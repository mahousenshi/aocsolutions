import re

with open('12.txt', 'r') as f:
    route = [l.strip() for l in f.readlines()]

rose = {'N': 0, 'E': 1, 'S': 2, 'W': 3}
position = [0, 0, 0, 0]

boat = 1

for action in route:
    act, qtd = re.match(r'(\w)(\d+)', action).groups()

    if act in rose:
        position[rose[act]] += qtd

    if act == 'F':
        position[boat] += qtd

    if act == 'R':
        boat = (boat + qtd // 90) % 4

    if act == 'L':
        boat = (boat - qtd // 90) % 4

print(abs(position[0]-position[2]) + abs(position[1]-position[3]))