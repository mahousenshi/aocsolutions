from collections import Counter
import re

with open('21.txt', 'r') as f:
    lines = [l.strip() for l in f.readlines()]

allergens_d = {}
ingredients_c = Counter()

for line in lines:
    ingredients, allergens = re.search(r'(.*)\(contains (.*)\)', line).groups()

    ingredients = set(ingredients.split())
    ingredients_c.update(ingredients)

    for allergen in allergens.split(', '):
        if allergen in allergens_d:
            allergens_d[allergen] = ingredients.intersection(allergens_d[allergen])

        else:
            allergens_d[allergen] = ingredients

finds = set()
for allergen in allergens_d:
    finds = finds.union(allergens_d[allergen])

print(sum(ingredients_c[ingredient] for ingredient in ingredients_c if ingredient not in finds))