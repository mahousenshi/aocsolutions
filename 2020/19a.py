import re

with open('19rules.txt', 'r') as f:
    rules = [l.strip() for l in f.readlines()]

rules_d = {}

for rule in rules:
    n, ru = re.match(r'(\d+):(.*)', rule).groups()
    
    rules_d[n] = ru.split('|')

rules_d['2'] = ['a']
rules_d['118'] = ['b']

for key in rules_d:
    for i, s in enumerate(rules_d[key]):
        if ' 2 ' in s:
            rules_d[key][i].replace(' 2 ', 'a')

for key in rules_d:
    print(key, rules_d[key])

