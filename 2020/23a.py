from collections import deque

inp = '389125467'

cups = deque([int(n) for n in inp])
picks = deque()

for i in range(10):
    current = cups[0]
    cups.rotate(-1)

    for _ in range(3):
        picks.append(cups.popleft())

    destination = current - 1 if current != 1 else 9
    while destination in picks:
        destination = destination - 1 if destination != 1 else 9

    n = cups.index(destination) + 1
    for _ in range(3):
        cups.insert(n, picks.pop())

cups.rotate(-cups.index(1))
cups.popleft()

print(''.join(str(n) for n in cups))