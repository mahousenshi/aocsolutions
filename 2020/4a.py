import re

with open('4.txt', 'r') as f:
    passaports = f.read().split('\n\n')

props_needed = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']

valid_passports = 0

for passaport in passaports:
    valid_passports += 1 if sum([1 for prop in passaport.split() if prop[:3] in props_needed]) == 7 else 0

print(valid_passports)