from collections import defaultdict

with open('10.txt', 'r') as f:
    numbers = [0] + sorted([int(l) for l in f.readlines()])

limit = max(numbers) + 3

nodes = defaultdict(list)
poss = defaultdict(int)

for n in numbers:
    p = 0
    if n+1 in numbers:
        nodes[str(n)].append(str(n+1))
        p += 1

    if n+2 in numbers:
        nodes[str(n)].append(str(n+2))
        p += 1

    if n+3 in numbers:
        nodes[str(n)].append(str(n+3))
        p += 1

    poss[str(n)] = p

while len
