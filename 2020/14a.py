import re

def bitmask(binary, mask):
    return ''.join(a if b == 'X' else b for a, b in zip(binary, mask))

with open('14.txt', 'r') as f:
    lines = [l.strip() for l in f.readlines()]

mem = {}

for line in lines:
    mask_m = re.match(r'mask = (.*)', line)
    mem_m = re.match(r'mem\[(\d+)\] = (\d+)', line)

    if mask_m:
        mask = mask_m.group(1)

    else:
        mem[mem_m.group(1)] = int(bitmask(f'{int(mem_m.group(2)):>036b}', mask), 2)

print(sum(mem.values()))