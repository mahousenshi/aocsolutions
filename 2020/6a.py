from collections import Counter

with open('6.txt', 'r') as f:
    groups = f.read().split('\n\n')

print(sum(len(Counter(group.replace('\n', ''))) for group in groups))