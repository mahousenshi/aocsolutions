import re

with open('2.txt', 'r') as f:
    pwds = f.readlines()

valids_pwd = 0
for pwd in pwds:
    pwd_m = re.match(r'(\d+)-(\d+) (\w): (\w+)', pwd)

    p1, p2, letter, pw = pwd_m.groups()

    pw = {i: p for i, p in enumerate(pw)}

    p1 = int(p1)-1
    p2 = int(p2)-1

    times = 0

    if p1 < len(pw) and pw[p1] == letter:
        times += 1

    if p2 < len(pw) and pw[p2] == letter:
        times += 1

    if times == 1:
        valids_pwd += 1

print(valids_pwd)