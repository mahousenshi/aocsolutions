import re

with open('8.txt', 'r') as f:
    program = [l.strip() for l in f.readlines()]

pointer = 0
accumulator = 0

visited = []

while True:
    if pointer in visited:
        break
    else:
        visited.append(pointer)

    comm = re.match(r'(\w+) ((\+|-)(\d+))', program[pointer])

    if comm.group(1) == 'acc':
        accumulator += int(comm.group(2))
        pointer += 1

    elif comm.group(1) == 'nop':
        pointer += 1

    elif comm.group(1) == 'jmp':
        pointer += int(comm.group(2))

print(accumulator)