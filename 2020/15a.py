inp = [19,20,14,0,9,1]

turns = {n: t for t, n in enumerate(inp[:-1], )}
spoken = inp[-1]

turn = len(inp)
for turn in range(len(inp), 2021):
    if spoken in turns:
        last_spoken = turns[spoken]
        turns[spoken] = turn
        spoken = turns[spoken] - last_spoken

    else:
        turns[spoken] = turn
        spoken = 0

print(spoken)