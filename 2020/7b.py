from collections import defaultdict
import re
with open('7.txt', 'r') as f:
    rules = [l.strip() for l in f.readlines()]

bags = {}

for rule in rules:
    bag = re.match(r'(.*) bags contain (.*)', rule)

    color = bag.group(1)

    if color not in bags:
        bags[color] = {}

    if bag.group(2) != 'no other bags.':
        for contents in bag.group(2)[:-1].split(', '):
            content = re.match(r'(\d) (.*) bag(s|)', contents)

            qtd = content.group(1)
            inside_color = content.group(2)

            bags[color][inside_color] = int(qtd)

def search_bags(color, qtd):
    print(color)
    print(len(color) * '=')
    print(qtd)
    print(sum(bags[color].values()))
    print('')

    for inside_color in bags[color]:
        search_bags(inside_color, bags[color][inside_color])



    '''
    else:
        print(sum(bags[color].values()))
        print(bags[color])
    '''

search_bags('shiny gold', 0)


