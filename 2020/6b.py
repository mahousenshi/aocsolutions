from collections import Counter

with open('6.txt', 'r') as f:
    groups = f.read().split('\n\n')

total = 0
for group in groups:
    answers = Counter(group.replace('\n', ''))
    group_size = len(group.split('\n'))

    total += sum(1 for key in answers if answers[key] == group_size)

print(total)