from copy import deepcopy

def print_grid(grid):
    for l in grid:
        print(''.join(l))

with open('11.txt', 'r') as f:
    new = [list(l.strip()) for l in f.readlines()]

h = len(new)
w = len(new[0])

while True:
    old = deepcopy(new)

    for x in range(0, h):
        for y in range(0, w):
            if old[x][y] == '.':
                continue

            neigh = ''

            for dx, dy in [(-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)]:
                if x+dx >= 0 and x+dx < h and y+dy >= 0 and y+dy < w:
                    neigh += old[x+dx][y+dy]

            if old[x][y] == 'L' and neigh.count('#') == 0:
                new[x][y] = '#'

            elif old[x][y] == '#' and neigh.count('#') >= 4:
                new[x][y] = 'L'

    if new == old:
        break

print(sum(line.count('#') for line in new))