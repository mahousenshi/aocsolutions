with open('18.txt', 'r') as f:
    cubes_f = [s.strip() for s in f.readlines()]

cubes = []

for cube in cubes_f:
    x, y, z = [int(n) for n in cube.split(',')]
    cubes.append((x, y, z))

faces = len(cubes) * 6

for cube in cubes:
    x, y, z = cube

    if (x+1, y, z) in cubes:
        faces -= 1

    if (x, y+1, z) in cubes:
        faces -= 1

    if (x, y, z+1) in cubes:
        faces -= 1

    if (x-1, y, z) in cubes:
        faces -= 1

    if (x, y-1, z) in cubes:
        faces -= 1

    if (x, y, z-1) in cubes:
        faces -= 1

print(faces)
