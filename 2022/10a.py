with open('10.txt', 'r') as f:
    instructions = [s.strip() for s in f.readlines()][::-1]

x = 1
probe = 20
strength = 0

v = 0
wait = 0

for cycle in range(1, 221):
    if wait == 0:
        x += v

        instruction = instructions.pop()
        if instruction == 'noop':
            wait = 1
            v = 0
        else:
            wait = 2
            v = int(instruction.split()[1])
    wait -= 1

    if cycle == probe:
        strength += x * cycle
        probe += 40

print(strength)
