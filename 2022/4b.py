from re import findall

with open('4.txt', 'r') as f:
    pairs = [s.strip() for s in f.readlines()]

s = 0

for pair in pairs:
    limits = [int(n) for n in findall(r'\d+', pair)]

    a = set(range(limits[0], limits[1]+1))
    b = set(range(limits[2], limits[3]+1))

    s += 1 if a & b else 0

print(s)
