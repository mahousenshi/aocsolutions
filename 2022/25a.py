def SNAFU2decimal(s):
    n = 0
    for i, c in enumerate(s[::-1]):
        if c == '-':
            n += -1 * 5 ** i

        elif c == '=':
            n += -2 * 5 ** i

        else:
            n += int(c) * 5 ** i

    return n


def decimal2base(n, base):
    s = ''

    if n:
        while n > 0:
            n, t = divmod(n, base)
            s += str(t)
    else:
        s = '0'

    return s[::-1]


def decimal2SNAFU(n):
    s = ''
    n = [0] + [int(k) for k in decimal2base(n, 5)]

    i = len(n) - 1
    while i:
        c = n[i]

        if c == 3:
            n[i-1] += 1
            s += '='

        elif c == 4:
            n[i-1] += 1
            s += '-'

        elif c == 5:
            n[i-1] += 1
            s += '0'

        else:
            s += str(c)

        i -= 1

    if n[0]:
        s += '1'

    return s[::-1]


with open('25.txt', 'r') as f:
    numbers = [s.strip() for s in f.readlines()]

print(decimal2SNAFU(sum(map(SNAFU2decimal, numbers))))
