with open('3.txt', 'r') as f:
    bags = [s.strip() for s in f.readlines()]

s = 0

for bag in bags:
    a = set(bag[:len(bag)//2])
    b = set(bag[len(bag)//2:])

    common = list(a & b)[0]

    offset = 38 if common.isupper() else 64

    # print(f'{common} -> {ord(common.upper()) - offset}')
    s += ord(common.upper()) - offset

print(s)
