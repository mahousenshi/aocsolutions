with open('8t.txt', 'r') as f:
    grid = [[int(n) for n in s.strip()] for s in f.readlines()]

y, x, = 1, 1
ly, lx = len(grid), len(grid[0])

visible = 0

for x in range(1, lx-1):
    for y in range(1, ly-1):
        tree = grid[x][y]

        # Top
        if tree > max(grid[tx][y] for tx in range(x)):
            visible += 1
            continue

        # Down
        if tree > max(grid[tx][y] for tx in range(x+1, lx)):
            visible += 1
            continue

        # Left
        if tree > max(grid[x][ty] for ty in range(y)):
            visible += 1
            continue

        # Right
        if tree > max(grid[x][ty] for ty in range(y+1, ly)):
            visible += 1
            continue

visible += lx * 2 + ly * 2 - 4

print(visible)
