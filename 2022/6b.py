with open('6.txt', 'r') as f:
    datastream = f.read().strip()

d = 14

for i in range(d, len(datastream)):
    if len(set(datastream[i-d:i])) == d:
        break

print(i)
