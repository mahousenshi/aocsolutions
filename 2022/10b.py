def draw(x, cycle):
    return '#' if x <= cycle <= x+2 else '.'


with open('10.txt', 'r') as f:
    instructions = [s.strip() for s in f.readlines()][::-1]

x = 1
EOL = 40
offset = 0

v = 0
wait = 0

line = ''
lines = []

for cycle in range(1, 241):
    if wait == 0:
        x += v

        instruction = instructions.pop()
        if instruction == 'noop':
            wait = 1
            v = 0
        else:
            wait = 2
            v = int(instruction.split()[1])
    wait -= 1

    line += draw(x, cycle-offset)

    if cycle == EOL:
        EOL += 40
        offset += 40
        lines.append(line)
        line = ''

for line in lines:
    print(line)
