from re import findall


def manhattan(p, q):
    return abs(p[0] - q[0]) + abs(p[1] - q[1])


with open('15.txt', 'r') as f:
    sensors_report = [s.strip() for s in f.readlines()]

sensors = []
beacons = []

for i, beacon in enumerate(sensors_report):
    sx, sy, bx, by = [int(n) for n in findall(r'(?:-|)\d+', beacon)]

    d = manhattan((sx, sy), (bx, by))

    sensors.append({
        'x': sx,
        'y': sy,
        'range': d
    })
    beacons.append((bx, by))

y = 2000000

points = set()

for sensor in sensors:
    d = abs(y - sensor['y'])

    if d <= sensor['range']:
        a = sensor['x'] - sensor['range'] + d
        b = sensor['x'] + sensor['range'] - d

        for x in range(a, b+1):
            if (x, y) not in beacons:
                points.add((x, y))

print(len(points))
