with open('2.txt', 'r') as f:
    rounds = [s.strip() for s in f.readlines()]

shape = {
    'X': 1,
    'Y': 2,
    'Z': 3,
}

outcome = {
    'A': {
        'X': 3,
        'Y': 6,
        'Z': 0,
    },
    'B': {
        'X': 0,
        'Y': 3,
        'Z': 6,
    },
    'C': {
        'X': 6,
        'Y': 0,
        'Z': 3,
    }
}

score = 0

for hand in rounds:
    mine = hand[2]
    rival = hand[0]

    score += shape[mine] + outcome[rival][mine]

print(score)
