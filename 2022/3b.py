with open('3.txt', 'r') as f:
    bags = [s.strip() for s in f.readlines()]

s = 0

for i in range(0, len(bags), 3):
    first = set(bags[i])
    second = set(bags[i+1])
    third = set(bags[i+2])

    common = list(first & second & third)[0]

    offset = 38 if common.isupper() else 64

    # print(f'{common} -> {ord(common.upper()) - offset}')
    s += ord(common.upper()) - offset

print(s)
