from re import compile
from math import prod


class Monkey:
    def __init__(self, items, operator, operand, divisible, true, false):
        self.items = items
        self.operator = operator
        self.operand = operand
        self.divisible = divisible
        self.true = true
        self.false = false
        self.inspect = 0


with open('11.txt', 'r') as f:
    monkies_init = [s.strip() for s in f.readlines()] + ['']

# Monkey bussiness
monkies = []

s = ''
monkey_re = compile(r'Monkey \d+: Starting items: ([\d+, ]+[\d+]) Operation: new = old (\+|\*) (\w+|\d+) Test: divisible by (\d+) If true: throw to monkey (\d+) If false: throw to monkey (\d+)')

for monkey in monkies_init:
    if monkey == '':
        items, operator, operand, divisible, true, false = monkey_re.match(s).groups()

        items = [int(i) for i in items.split(', ')]
        divisible, true, false = map(int, [divisible, true, false])

        monkies.append(Monkey(items, operator, operand, divisible, true, false))

        s = ''
        continue

    s += monkey + ' '

for _ in range(20):
    for monkey in monkies:
        monkey.items = monkey.items[::-1]
        while monkey.items:
            monkey.inspect += 1
            item = monkey.items.pop()

            if monkey.operator == '+':
                if monkey.operand.isnumeric():
                    worry = item + int(monkey.operand)
                else:
                    worry = item + item
            else:
                if monkey.operand.isnumeric():
                    worry = (item * int(monkey.operand))
                else:
                    worry = item * item

            worry = worry // 3

            if worry % monkey.divisible == 0:
                monkies[monkey.true].items.append(worry)
            else:
                monkies[monkey.false].items.append(worry)

print(prod(sorted([monkey.inspect for monkey in monkies])[-2:]))
