with open('1.txt', 'r') as f:
    snacks = [s.strip() for s in f.readlines()]

elves = []
elf = 0

for snack in snacks:
    if snack == "":
        elves.append(elf)
        elf = 0
    else:
        elf += int(snack)

print(sum(elves.sort(reverse=True)[0:3]))
