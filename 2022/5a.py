from re import findall

with open('5.txt', 'r') as f:
    operations = [s.rstrip() for s in f.readlines()]

# Find the empty line before the crane operations
sep = operations.index('')

# Creating the stacks container
stacks = [[] for n in findall(r'\d+', operations[sep-1])]

# Adding the boxes on the stacks
for stack in operations[sep-2::-1]:
    for i in range(0, len(stack), 4):
        box = stack[i:i+4][1]

        if box != ' ':
            stacks[i//4].append(box)

# Crane operations
for operation in operations[sep+1:]:
    times, origin, destiny = [int(n) for n in findall(r'\d+', operation)]

    for _ in range(times):
        stacks[destiny-1].append(stacks[origin-1].pop())

print(''.join([s[-1] for s in stacks]))
