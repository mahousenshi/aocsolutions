with open('8.txt', 'r') as f:
    grid = [[int(n) for n in s.strip()] for s in f.readlines()]

y, x, = 1, 1
ly, lx = len(grid), len(grid[0])

scenic = []

for x in range(1, lx-1):
    for y in range(1, ly-1):
        tree = grid[x][y]
        top, down, left, right = 0, 0, 0, 0

        # Top
        for t in reversed([grid[tx][y] for tx in range(x)]):
            top += 1
            if t >= tree:
                break

        # Down
        for t in [grid[tx][y] for tx in range(x+1, lx)]:
            down += 1
            if t >= tree:
                break

        # Left
        for t in reversed([grid[x][ty] for ty in range(y)]):
            left += 1
            if t >= tree:
                break

        # Right
        for t in [grid[x][ty] for ty in range(y+1, ly)]:
            right += 1
            if t >= tree:
                break

        scenic.append(top * down * left * right)

print(max(scenic))
