from re import split
from itertools import cycle

with open('21.txt', 'r') as f:
    monkey_ops = [s.strip() for s in f.readlines()]

monkeys = {}

for monkey in monkey_ops:
    key, value = monkey.split(':')

    if value.strip().isnumeric():
        monkeys[key] = int(value)
    else:
        monkeys[key] = value.strip()

for key, value in cycle(monkeys.items()):
    if isinstance(value, str):
        a, op, b = split(r' (\+|-|\*|\/) ', value)

        if isinstance(monkeys[a], int) and isinstance(monkeys[b], int):
            if op == '+':
                monkeys[key] = monkeys[a] + monkeys[b]
            elif op == '-':
                monkeys[key] = monkeys[a] - monkeys[b]
            elif op == '*':
                monkeys[key] = monkeys[a] * monkeys[b]
            elif op == '/':
                monkeys[key] = monkeys[a] // monkeys[b]

    if isinstance(monkeys['root'], int):
        break

print(monkeys['root'])
