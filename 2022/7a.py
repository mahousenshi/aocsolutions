class Tree:
    def __init__(self, name, parent):
        self.name = name
        self.size = 0
        self.parent = parent
        self.children = []


def transverse_tree(branch, s):
    # TODO sum using this function
    # print(f'{s*" "}- {branch.name} ({branch.size})')
    sizes.append(branch.size)

    for child in branch.children:
        transverse_tree(child, s+1)


with open('7.txt', 'r') as f:
    commands = [s.strip() for s in f.readlines()]

branch = Tree('/', None)

for command in commands[1:]:
    if '$ cd' in command:
        name = command.split()[2]

        if name == '..':
            branch = branch.parent

        else:
            for child in branch.children:
                if child.name == name:
                    branch = child
                    break

    elif '$ ls' in command:
        continue

    else:
        size, name = command.split()

        if size.isnumeric():
            size = int(size)
            branch.size += size

            branch_parent = branch.parent
            while branch_parent is not None:
                branch_parent.size += size
                branch_parent = branch_parent.parent

        else:
            new_branch = Tree(name, branch)
            branch.children.append(new_branch)

while branch.parent is not None:
    branch = branch.parent

sizes = []
transverse_tree(branch, 0)

print(sum(size if size <= 100000 else 0 for size in sizes))
