class Knot:
    def __init__(self, name):
        self.name = name
        self.knot = None
        self.x = 0
        self.y = 0
        self.visited = [(0, 0)]

    def move(self, direction):
        if direction == 'U':
            self.y += 1

        elif direction == 'UL':
            self.x += 1
            self.y += 1

        elif direction == 'L':
            self.x += 1

        elif direction == 'DL':
            self.x += 1
            self.y -= 1

        elif direction == 'D':
            self.y -= 1

        elif direction == 'DR':
            self.x -= 1
            self.y -= 1

        elif direction == 'R':
            self.x -= 1

        elif direction == 'UR':
            self.x -= 1
            self.y += 1

        if self.knot is not None:
            off = (self.x - self.knot.x, self.y - self.knot.y)

            if off in offset:
                self.knot.move(offset[off])

        if self.knot is None and (self.x, self.y) not in self.visited:
            self.visited.append((self.x, self.y))


offset = {
    (0, -2): 'D',
    (1, -2): 'DL',
    (2, -2): 'DL',
    (2, -1): 'DL',
    (2, 0): 'L',
    (2, 1): 'UL',
    (2, 2): 'UL',
    (1, 2): 'UL',
    (0, 2): 'U',
    (-1, 2): 'UR',
    (-2, 2): 'UR',
    (-2, 1): 'UR',
    (-2, 0): 'R',
    (-2, -1): 'DR',
    (-2, -2): 'DR',
    (-1, -2): 'DR',
}

with open('9.txt', 'r') as f:
    steps = [s.strip() for s in f.readlines()]

head = Knot('head')
tail = Knot('tail')
head.knot = tail

for step in steps:
    d, s = step.split()

    for _ in range(int(s)):
        head.move(d)

print(len(tail.visited))
