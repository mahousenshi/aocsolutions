with open('2.txt', 'r') as f:
    rounds = [s.strip() for s in f.readlines()]

shape = {
    'A': {
        'X': 3,
        'Y': 1,
        'Z': 2,
    },
    'B': {
        'X': 1,
        'Y': 2,
        'Z': 3,
    },
    'C': {
        'X': 2,
        'Y': 3,
        'Z': 1,
    }
}

outcome = {
    'X': 0,
    'Y': 3,
    'Z': 6,
}

score = 0

for hand in rounds:
    out = hand[2]
    rival = hand[0]

    score += shape[rival][out] + outcome[out]

print(score)
