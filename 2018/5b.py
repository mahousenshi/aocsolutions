from collections import Counter
from re import sub

with open('5.txt', 'r') as f:
    inp = f.read()

units = []

for unitp in Counter(inp.lower()).keys():
    new = sub('{}|{}'.format(unitp, unitp.upper()), '', inp)

    while True:
        old = new

        for unit in Counter(new.lower()).keys():
            new = sub('{}|{}'.format(unit + unit.upper(), unit.upper() + unit), '', new)

        if old == new:
            break
    
    print(len(new))
    units.append(len(new))

print('----')
print(min(units))