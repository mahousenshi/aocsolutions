from collections import defaultdict
import re

def mapInt(l):
    return list(map(int, l))

def manhattan(a, b):
    return abs(a[0] - b[0]) + abs(a[1] - b[1]) + abs(a[2] - b[2])

with open('23.txt', 'r') as f:
    inp = f.readlines()

bots = []
maxr = 0
maxi = 0

for i, bot in enumerate(inp):
    x, y, z, r = mapInt(re.findall(r'-?\d+', bot))

    if r > maxr:
        maxr = r
        maxi = i

    bots.append({
        'x': x,
        'y': y,
        'z': z,
    })

maxb = [bots[maxi]['x'], bots[maxi]['y'], bots[maxi]['z']]
print(maxb)
conn = 0

for bot in bots:
    m = manhattan([bot['x'], bot['y'], bot['z']], maxb)
    print(manhattan([bot['x'], bot['y'], bot['z']], maxb))

    if m <= maxr:
        conn += 1
    
print(conn)
