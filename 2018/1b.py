from itertools import cycle

with open('1.txt', 'r') as f:
    inp = f.readlines()

numbers = map(int, inp)

freq = 0
freqs = [0]

for n in cycle(numbers):
    freq += n

    if freq in freqs:
        print(freq)
        break
    else:
        freqs.append(freq)
