with open('8.txt', 'r') as f:
    inp = list(map(int, f.read().split()))

meta = []

def node(l):
    c = l[0]
    m = l[1]
    l = l[2:]

    while c:
        l = node(l)
        c -= 1
    
    meta.append(sum(l[:m]))
    return l[m:]

node(inp)

print(sum(meta))
