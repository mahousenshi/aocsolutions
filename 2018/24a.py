from operator import itemgetter
import re

def mapInt(l):
    return list(map(int, l))

units = []

with open('24a.txt', 'r') as f:
    inp = f.readlines()

for unit in inp:
    n, hp, damage, init = mapInt(re.findall(r'\d+', unit))

    immune = []
    weak = []
    
    st = unit.find('(')
    if st + 1:
        ed = unit.find(')')

        for special in unit[st+1:ed].split('; '):
            if special.find('immune') + 1:
                special = special.replace('immune to ', '')
                immune = special.split(', ')

            if special.find('weak') + 1:
                special = special.replace('weak to ', '')
                weak = special.split(', ')

        unit = unit[:st-1] + unit[ed+1:]

    units.append({
        'units': n,
        'hp': hp,
        'damage': damage,
        'init': init,
        'power': n * damage,
        'immune': immune,
        'weak': weak,
        'dtype': unit.split()[13],
        'clan': 'immune'
    })

with open('24b.txt', 'r') as f:
    inp = f.readlines()

for unit in inp:
    n, hp, damage, init = mapInt(re.findall(r'\d+', unit))

    immune = []
    weak = []
    
    st = unit.find('(')
    if st + 1:
        ed = unit.find(')')

        for special in unit[st+1:ed].split('; '):
            if special.find('immune') + 1:
                special = special.replace('immune to ', '')
                immune = special.split(', ')

            if special.find('weak') + 1:
                special = special.replace('weak to ', '')
                weak = special.split(', ')
        
        unit = unit[:st-1] + unit[ed+1:]
        
    units.append({
        'units': n,
        'hp': hp,
        'damage': damage,
        'init': init,
        'power': n * damage,
        'immune': immune,
        'weak': weak,
        'dtype': unit.split()[13],
        'clan': 'immune'
    })

# 

# Target phase
units.sort(key=itemgetter('power', 'init'), reverse=True)

targets = units.copy()
attack = []

for unit in units:
    dtype = unit['dtype']

    poss = {}
    poss['i'] = -1
    poss['damage'] = 0
    poss['power'] = 0
    poss['init'] = 0

    for i, target in enumerate(targets):
        damage = 0

        if unit['clan'] == target['clan']:
            continue
        
        else:
            if dtype in target['immune']:
                damage = 0
            else:
                damage = unit['power']

            if dtype in target['weak']:
                damage *= 2

        if poss['damage'] > damage:
            continue
        
        if poss['damage'] == damage:
            if poss['power'] > target['power']:
                continue

            if poss['power'] == target['power']:
                if poss['init'] > target['init']:
                    continue

        poss['i'] = i
        poss['damage'] = damage
        poss['power'] = target['power']
        poss['init'] = target['init']

    if poss['i'] + 1:
        attack.append(target.pop(i))

# Attack phase

    


