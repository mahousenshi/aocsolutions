from collections import Counter

def manhattan(a, b):
    return abs(a[0] - b[0]) + abs(a[1] - b[1])

with open('6.txt', 'r') as f:
    inp = f.readlines()

coords = [list(map(int, coord.split(','))) for coord in inp]

x = [coord[0] for coord in coords]
y = [coord[1] for coord in coords]

grid = {}

for i in range(min(x), max(x) + 1):
    for j in range(min(y), max(y) + 1):
        coord = '{},{}'.format(i, j)

        d = [manhattan([i, j], c) for c in coords]

        if d.count(min(d)) > 1:
            grid[coord] = '.'
        else:
            grid[coord] = str(d.index(min(d)))

finite = [str(n) for n in range(len(coords))]

for i in range(min(x), max(x) + 1):
    for j in [min(y), max(y)]:
        coord = '{},{}'.format(i, j)

        if grid[coord] in finite:
            finite.remove(grid[coord])
        
for i in [min(x), max(x)]:
    for j in range(min(y), max(y) + 1):
        coord = '{},{}'.format(i, j)

        if grid[coord] in finite:
            finite.remove(grid[coord])

print(max([Counter(grid.values())[key] for key in finite]))
