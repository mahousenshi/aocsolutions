from collections import Counter
from itertools import chain

def mapInt(l):
    return list(map(int, l))

with open('18.txt', 'r') as f:
    inp = f.readlines()

grid = [list(s.replace('\n', '')) for s in inp]

d = len(grid[0])

values = []
valuesa = []
ir = 0

for i in range(1, 1000):
    ngrid = [[] for _ in range(d)]

    for x, l in enumerate(grid):
        for y, c in enumerate(l):
            neigh = []

            for dx in range(-1, 2):
                for dy in range(-1, 2):
                    ddx = x + dx
                    ddy = y + dy

                    if 0 <= ddx < d and 0 <= ddy < d:
                        neigh.append([ddx, ddy])

            if c == '.':
                if Counter([grid[dx][dy] for dx, dy in neigh])['|'] >= 3:
                    ngrid[x].append('|')
                else:
                    ngrid[x].append('.')
        
            if c == '|':
                if Counter([grid[dx][dy] for dx, dy in neigh])['#'] >= 3:
                    ngrid[x].append('#')
                else:
                    ngrid[x].append('|')
        
            if c == '#':
                area = Counter([grid[dx][dy] for dx, dy in neigh])

                if area['#'] > 1 and area['|'] >= 1:
                    ngrid[x].append('#')
                else:
                    ngrid[x].append('.')

    grid = ngrid
    a = []

    e = Counter([i for i in chain.from_iterable(grid)])  
    value = e['|'] * e['#']
    
    if value in values:

        if value in valuesa:
            break
        else:

            if ir == i - 1 and ir != 382:

                print(i, value)
                valuesa.append(value)
            
            ir = i
    else:
        values.append(value)

d = 1000000000 % len(valuesa)

n = 0 
while len(valuesa) * n + d < ir:
    n += 1

print(len(valuesa) * n + d)