inp = list(map(int, '170641'))
l = len(inp)

rec = [3, 7]
a, b = 0, 1

while True:
    new = str(rec[a] + rec[b])
    rec += list(map(int, new))

    a += 1 + rec[a]
    a %= len(rec)

    b += 1 + rec[b]
    b %= len(rec)

    if len(new) > 1 and rec[-(l+1):-1] == inp:
        print(len(rec[:-(l+1)]))
        break

    if rec[-l:] == inp:
        print(len(rec[:-l]))
        break
