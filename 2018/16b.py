import re

def mapInt(l):
    return list(map(int, l))

#---

def addr(r, a, b):
    return r[a] + r[b]

def addi(r, a, b):
    return r[a] + b

#---

def mulr(r, a, b):
    return r[a] * r[b]

def muli(r, a, b):
    return r[a] * b

#---

def banr(r, a, b):
    return r[a] & r[b]

def bani(r, a, b):
    return r[a] & b

#---

def borr(r, a, b):
    return r[a] | r[b]

def bori(r, a, b):
    return r[a] | b

#---

def setr(r, a, b):
    return r[a]

def seti(r, a, b):
    return a

#---

def gtir(r, a, b):
    return int(a > r[b])

def gtri(r, a, b):
    return int(r[a] > b)

def gtrr(r, a, b):
    return int(r[a] > r[b])

#---

def eqir(r, a, b):
    return int(a == r[b])

def eqri(r, a, b):
    return int(r[a] == b)

def eqrr(r, a, b):
    return int(r[a] == r[b])

#---

with open('16a.txt', 'r') as f:
    t = f.read().split('\n')
    chunks = [t[i:i+4] for i  in range(0, len(t), 4)]

instructions = [
    addr,addi, mulr, muli, banr, bani, borr, bori,
    setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr,
]

opcodes = [[f.__name__ for f in instructions] for _ in range(16)]

for i in chunks:
    before = mapInt(re.findall(r'\d', i[0]))
    n, a, b, c = mapInt(i[1].split())
    after = mapInt(re.findall(r'\d', i[2]))

    for f in instructions:
        if f.__name__ in opcodes[n]:
            t = before.copy()

            t[c] = f(before, a, b)

            if t != after:
                opcodes[n].remove(f.__name__)

    if len(opcodes[n]) == 1:
        u_opcode = opcodes[n][0]

        for m, opcode in enumerate(opcodes):
            if u_opcode in opcode and n != m:
                opcode.remove(u_opcode)

instructions = {f.__name__: f for f in instructions }
opcodes = [opcode[0] for opcode in opcodes]

with open('16b.txt', 'r') as f:
    lines = f.readlines()

register = [0 for _ in range(4)]

for line in lines:
    n, a, b, c = mapInt(line.split())
    register[c] = instructions[opcodes[n]](register, a, b)

print(register[0])