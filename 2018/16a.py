import re

def mapInt(l):
    return list(map(int, l))

#---

def addr(r, a, b):
    return r[a] + r[b]

def addi(r, a, b):
    return r[a] + b

#---

def mulr(r, a, b):
    return r[a] * r[b]

def muli(r, a, b):
    return r[a] * b

#---

def banr(r, a, b):
    return r[a] & r[b]

def bani(r, a, b):
    return r[a] & b

#---

def borr(r, a, b):
    return r[a] | r[b]

def bori(r, a, b):
    return r[a] | b

#---

def setr(r, a, b):
    return r[a]

def seti(r, a, b):
    return a

#---

def gtir(r, a, b):
    return int(a > r[b])

def gtri(r, a, b):
    return int(r[a] > b)

def gtrr(r, a, b):
    return int(r[a] > r[b])

#---

def eqir(r, a, b):
    return int(a == r[b])

def eqri(r, a, b):
    return int(r[a] == b)

def eqrr(r, a, b):
    return int(r[a] == r[b])

#---

with open('16a.txt', 'r') as f:
    t = f.read().split('\n')
    chunks = [t[i:i+4] for i  in range(0, len(t), 4)]

instructions = [
    addr, addi, mulr, muli, banr, bani, borr, bori,
    setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr,
]

cc = 0

for i in chunks:
    before = mapInt(re.findall(r'\d', i[0]))
    n, a, b, c = mapInt(i[1].split())
    after = mapInt(re.findall(r'\d', i[2]))

    bh = 0

    for f in instructions:
        t = before.copy()

        t[c] = f(before, a, b)

        if t == after:
            bh += 1

        if bh >= 3:
            cc += 1
            break
        
    
print(cc)