from collections import defaultdict

with open('3.txt', 'r') as f:
    inp = f.readlines()

coords = defaultdict(int)

for elf in inp:
    elf = elf.split()

    x, y = map(int, elf[2][:-1].split(','))
    l, h = map(int, elf[3].split('x'))
    
    for i in range(l):
        for j in range(h):
            coords['{},{}'.format(x + i, y + j)] += 1

c = 0 

for coord in coords:
    if coords[coord] > 1:
        c += 1

print(c)