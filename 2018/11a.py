sn = 3628

def powerLevel(x, y, sn):
    rackID = (x + 10)    
    pl = (rackID * y + sn) * rackID

    if pl < 100:
        pl = 0
    else:
        pl = int(str(pl)[-3])
    
    return pl - 5

coords = [[-1, -1], [-1, 0], [-1, 1], [0, -1], [0, 0], [0, 1], [1, -1], [1, 0], [1, 1]]
coord = ''
m = 0

for x in range(1, 301):
    for y in range(1, 300):
        s = 0

        for dx, dy in coords:
            s += powerLevel(x+dx, y-dy, sn)

        if s > m:
            m = s
            coord = '{},{}'.format(x-1, y-1)
            break

print(coord)