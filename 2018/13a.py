from collections import Counter
from astar import astar

import re

def crashed(carts):
    return len(carts) - len(Counter([c['p'] for c in carts]))

def toYX(p):
    return '{},{}'.format(p % 150, p // 150)

with open('13.txt', 'r') as f:
    grid = f.read().replace('\n', '')

carts = []

for p in [m.start() for m in re.finditer(r'[<>v\^]', grid)]:
    if grid[p] == '^':
        d = 0
    
    elif grid[p] == '>':
        d = 1
    
    elif grid[p] == 'v':
        d = 2
    
    elif grid[p] == '<':
        d = 3

    carts.append({
        'p': p,
        't': 0,
        'd': d
    })

grid = grid.replace('>', '-').replace('<', '-').replace('^', '|').replace('v', '|')
ps = [-150, 1, 150, -1]
b = -1

while not b + 1:
    for cart in carts:
        p, t, d = [cart['p'], cart['t'], cart['d']]

        pos = p + ps[d]
        cart['p'] = pos

        if crashed(carts):
            b = pos
            break
    
        c = grid[pos]
        dc = 0

        if c == '/':
            if d == 0 or d == 2:
                dc = 1
            else:
                dc = -1

        if c == '\\':
            if d == 0 or d == 2:
                dc = -1
            else:
                dc = 1
        
        if c == '+':
            if t == 0:
                dc = -1
            elif t == 2:
                dc = 1

            cart['t'] = (t + 1) % 3  

        cart['d'] = (d + dc) % 4

    carts = sorted(carts, key=lambda k: k['p'])

print(toYX(b))