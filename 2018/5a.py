from collections import Counter
from re import sub

with open('5.txt', 'r') as f:
    inp = f.read()

while True:
    old = inp

    for unit in Counter(inp.lower()).keys():
        inp = sub('{}|{}'.format(unit + unit.upper(), unit.upper() + unit), '', inp)

    if old == inp:
        break

print(len(inp))