with open('20.txt', 'r') as f:
    path = f.read().strip()

s = path[1:-1]
p = ''

def search(s):
    if s.find('(') + 1:
        st = 0
        i = 0
        options = []
        l = 0
        

        while i < len(s):
            if s[i] == '(':
                l += 1

            elif s[i] == ')':
                l -= 1
            
            elif s[i] == '|' and l == 0:
                options.append(s[st:i])
                st = i + 1

            i += 1

        options.append(s[st:i])

        for n, o in enumerate(options):
            print(o)

            if o.find('(') + 1:
                st = 0
                i = 0
                f = 1
                l = 0

                while i < len(o):
                    
                    if o[i] == '(':
                        l += 1

                        if l and f:
                            st = i + 1
                            f = 0

                    elif o[i] == ')':
                        l -= 1

                        if l == 0 and f == 0:
                            o = o[:st - 1] + search(o[st:i]) + o[i+1:]
                            print(o)
                            i = -1
                            f = 1

                    i += 1

                options[n] = o

        w = options[0]
        m = len(w)
        
        for way in options[1:]:
            if len(way) > m:
                m = len(way)
                w = way
        
        return w

    else:
        s = s.split('|')

        w = s[0]
        m = len(w)
        
        for way in s[1:]:
            if len(way) > m:
                m = len(way)
                w = way
        
        print(w)
        return w

v = search(s)

print(v)
print(len(v))