from collections import Counter
from itertools import chain

def mapInt(l):
    return list(map(int, l))

with open('18.txt', 'r') as f:
    inp = f.readlines()

grid = [list(s.replace('\n', '')) for s in inp]

d = len(grid[0])

for _ in range(10):
    s = ''
    ngrid = [[] for __ in range(d)]

    for x, l in enumerate(grid):
        for y, c in enumerate(l):
            neigh = []

            for dx in range(-1, 2):
                for dy in range(-1, 2):
                    ddx = x + dx
                    ddy = y + dy

                    if 0 <= ddx < d and 0 <= ddy < d:
                        neigh.append([ddx, ddy])

            area = Counter([grid[dx][dy] for dx, dy in neigh])

            if c == '.':
                if area['|'] >= 3:
                    ngrid[x].append('|')
                else:
                    ngrid[x].append('.')
        
            if c == '|':
                if area['#'] >= 3:
                    ngrid[x].append('#')
                else:
                    ngrid[x].append('|')
        
            if c == '#':
                if area['#'] > 1 and area['|'] >= 1:
                    ngrid[x].append('#')
                else:
                    ngrid[x].append('.')

    grid = ngrid

e = Counter([i for i in chain.from_iterable(grid)])
print(e['|'] * e['#'])
