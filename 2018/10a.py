from collections import defaultdict
import re

with open('10.txt', 'r') as f:
    inp = f.readlines()

points = []
vel = []

for p in inp:
    p = list(map(int, re.findall(r'-?\d+', p)))

    points.append(p[:2])
    vel.append(p[2:])

px = []
py = []

for i, [dx, dy] in enumerate(vel):
    px.append(points[i][0])
    py.append(points[i][1])

dpx = abs(max(px) - min(px))
dpy = abs(max(py) - min(py))

while dpx > 50 and dpy > 50:
    px = []
    py = []

    for i, [dx, dy] in enumerate(vel):
        points[i][0] += dx
        points[i][1] += dy

        px.append(points[i][0])
        py.append(points[i][1])
    
    dpx = max(px) - min(px) 
    dpy = max(py) - min(py)

while True:
    px = []
    py = []

    for i, [dx, dy] in enumerate(vel):
        points[i][0] += dx
        points[i][1] += dy

        px.append(points[i][0])
        py.append(points[i][1])
    
    dpx = max(px) - min(px) 
    dpy = max(py) - min(py)

    s = ''

    for y in range(min(py), max(py)+1):
        for x in range(min(px), max(px)+1):
            if [x, y] in points:
                s += '#'
            else:
                s += '.'
            
        s += '\n'

    print(s)

    if input('> '):
        break
