with open('12.txt', 'r') as f:
    inp = [s.strip() for s in f.readlines()]

pat = '##.......#.######.##..#...#.#.#..#...#..####..#.##...#....#...##..#..#.##.##.###.##.#.......###....#'

rules = {s.split(' => ')[0]: s.split(' => ')[1] for s in inp}

k = 0

sums = []
diffs = [0]

i = 0
while True:
    pat = "...." + pat + "...."
    npat = ''

    for n in range(2, len(pat) - 2):
        npat += rules[pat[n-2:n+3]]

    pat = npat

    s = 0
    for j, c in enumerate(pat):

        if c == '#':
            s += j - 2 * (i+1)

    if i:
        diff = s - sums[-1]

        if diff == diffs[-1]:
            break

        diffs.append(diff)

    sums.append(s)
    i += 1

print((50000000000 - i) * diffs[-1] + sums[-1])
