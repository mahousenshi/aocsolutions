from collections import Counter

def manhattan(a, b):
    return abs(a[0] - b[0]) + abs(a[1] - b[1])

with open('6.txt', 'r') as f:
    inp = f.readlines()

coords = [list(map(int, coord.split(','))) for coord in inp]

x = [coord[0] for coord in coords]
y = [coord[1] for coord in coords]

d = 10000 - 1

sx = max(x) - min(x)
sy = max(y) - min(y)

c = 0

for i in range(sx - d, sx + d + 1):
    for j in range(sy - d, sy + d + 1):
        s = 0

        for coord in coords:
            s += manhattan([i, j], coord)

            if s >= d:
               break

        if s < d:
            c += 1

print(c)