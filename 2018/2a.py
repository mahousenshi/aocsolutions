from collections import Counter

with open('2.txt', 'r') as f:
    inp = f.readlines()

twos = 0
threes = 0

for ids in inp:
    check = Counter(ids)

    if 2 in check.values():
        twos += 1
    
    if 3 in check.values():
        threes += 1

print(twos * threes)