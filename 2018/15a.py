from collections import defaultdict
import re

def findGlobins():
    return [e for u in units if u['type'] != 'G']

def findElves():
    return [e for u in units if u['type'] == 'E']

def findEnemies(t):
    if t == 'G':
        return findElves()
    else:
        return findGlobins()

with open('15.txt', 'r') as f:
    inp = [l.strip('\n') for l in f.readlines()]

grid = []
units = []

for x, l in enumerate(inp):
    for y in [m.start() for m in re.finditer(r'[GE]', l)]:
        units.append({
            'x': x,
            'y': y,
            'hp': 200,
            'type': l[y],
            'target': [0, 0]
        })
    
    line = list(l)
    grid.append(line)

neig = [(0, -1), (0, 1), (-1, 0), (1, 0)]

for u in units:
    
    
    