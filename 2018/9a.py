from collections import deque

inp = '425 players; last marble is worth 70848 points'.split()

players = [0 for i in range(int(inp[0]))]

circle = deque([])

for i in range(int(inp[-2])):
    if i and i % 23 == 0:
        circle.rotate(7)

        players[i % len(players)] += i + circle.pop()

        circle.rotate(-1)
    else:
        circle.rotate(-1)
        circle.append(i)

print(max(players))