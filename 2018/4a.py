import re

with open('4.txt', 'r') as f:
    inp = f.readlines()

inp.sort()

guards = {}
s = -1
gid = ''

for log in inp:
    if log.find('Guard') + 1:
        if s != -1:
            for i in range(s, 60):
                guards[gid][i] += 1

        gid = log.split()[3]

        if gid not in guards:
            guards[gid] = [0 for i in range(60)]

        s = -1
   
    if log.find('asleep') + 1:
        s = int(re.search(r':(\d+)', log)[1])

    if log.find('wakes') + 1:
        e = int(re.search(r':(\d+)', log)[1])

        for i in range(s, e):
            guards[gid][i] += 1

        s = -1

top = 0
gid = ''

for guard in guards:
    if sum(guards[guard]) > top:
        top = sum(guards[guard])
        gid = guard

print(guards[gid].index(max(guards[gid])) * int(gid[1:]))