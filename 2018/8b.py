with open('8.txt', 'r') as f:
    inp = list(map(int, f.read().split()))

meta = []

def node(l):
    c = l[0]
    m = l[1]
    l = l[2:]

    if c:
        cv = [0]

        while c:
            l, v = node(l)
            c -= 1

            cv.append(v)

        v = 0
        cl = len(cv)

        for i in l[:m]:
            if i < cl:
                v += cv[i]
                
    else:
        v = sum(l[:m])

    meta.append(v)
    return (l[m:], v)

node(inp)

print(meta[-1])
