from collections import defaultdict

depth = 8112
target = [13, 743]

coords = {}

coords['0,0'] = 0
coords['13,743'] = 0

def calcGeo(x, y):
    c = '{},{}'.format(x, y)

    if c in coords:
        i = coords[c]
    else:
        if x == 0 and y == 0:
            i = 0

        elif x == target[0] and y == target[1]:
            i = 0

        elif y == 0:
            i = x * 16807

        elif x == 0:
            i = y * 48271

        else:
            i = calcEro(x - 1, y) * calcEro(x, y - 1)
        
        coords[c] = i

    return i

def calcEro(x, y):
    return (calcGeo(x, y) % 20183 + depth ) % 20183

s = 0

for x in range(target[0] + 1):
    s += calcEro(x, 0) % 3

for y in range(target[1] + 1):
    s += calcEro(0, y) % 3

for x in range(1, target[0] + 1):
    for y in range(1, target[1] + 1):
        s += calcEro(x, y) % 3
    
print(s)