from itertools import product

def powerLevel(x, y):
    rackID = (x + 10)    
    pl = (rackID * y + sn) * rackID

    if pl < 100:
        pl = 0
    else:
        pl = int(str(pl)[-3])
    
    return pl - 5

sn = 3628

grid = []

for x in range(1, 301):
    grid.append([powerLevel(x, y) for y in range(1, 301)])

coord = ''
m = 0

for l in range(1, 301):
    print(l)

    # Max at 11!!
    for x in range(301 - l):
        for y in range(301 - l):
            s = 0

            for dx, dy in product(range(x, x + l), range(y, y + l)):
                s += grid[dx][dy]

            if s > m:
                m = s
                coord = '{},{},{}'.format(x+1,y+1,l)
                print(coord)

print(coord)
