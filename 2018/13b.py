from collections import Counter
import re

def toYX(p):
    return '{},{}'.format(p % 150, p // 150)

with open('13.txt', 'r') as f:
    grid = f.read().replace('\n', '')

carts = []

for p in [m.start() for m in re.finditer(r'[<>v\^]', grid)]:
    if grid[p] == '^':
        d = 0
    
    elif grid[p] == '>':
        d = 1
    
    elif grid[p] == 'v':
        d = 2
    
    elif grid[p] == '<':
        d = 3

    carts.append({
        'p': p,
        't': 0,
        'd': d,
        'a': 1
    })

grid = grid.replace('>', '-').replace('<', '-').replace('^', '|').replace('v', '|')
ps = [-150, 1, 150, -1]

while len(carts) - 1:
    for pp, cart in enumerate(carts):
        p, t, d = [cart['p'], cart['t'], cart['d']]

        pos = p + ps[d]
                
        for n, c in enumerate(carts):
            if c['a'] and c['p'] == pos:
                c['a'] = 0
                cart['a'] = 0

        if cart['a']:
            cart['p'] = pos
            c = grid[pos]
            dc = 0

            if c == '/':
                if d == 0 or d == 2:
                    dc = 1
                else:
                    dc = -1

            if c == '\\':
                if d == 0 or d == 2:
                    dc = -1
                else:
                    dc = 1
            
            if c == '+':
                if t == 0:
                    dc = -1
                elif t == 2:
                    dc = 1

                cart['t'] = (t + 1) % 3  

            cart['d'] = (d + dc) % 4
            
    carts = sorted([c for c in carts if c['a']], key=lambda k: k['p'])

print(toYX(carts[0]['p']))