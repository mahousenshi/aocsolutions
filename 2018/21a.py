from time import sleep

def mapInt(l):
    return list(map(int, l))

#---

def addr(r, a, b):
    return r[a] + r[b]

def addi(r, a, b):
    return r[a] + b

#---

def mulr(r, a, b):
    return r[a] * r[b]

def muli(r, a, b):
    return r[a] * b

#---

def banr(r, a, b):
    return r[a] & r[b]

def bani(r, a, b):
    return r[a] & b

#---

def borr(r, a, b):
    return r[a] | r[b]

def bori(r, a, b):
    return r[a] | b

#---

def setr(r, a, b):
    return r[a]

def seti(r, a, b):
    return a

#---

def gtir(r, a, b):
    return int(a > r[b])

def gtri(r, a, b):
    return int(r[a] > b)

def gtrr(r, a, b):
    return int(r[a] > r[b])

#---

def eqir(r, a, b):
    return int(a == r[b])

def eqri(r, a, b):
    return int(r[a] == b)

def eqrr(r, a, b):
    return int(r[a] == r[b])

#---

with open('21.txt', 'r') as f:
    program = [s for s in f.readlines()]
    
instructions = [
    addr,addi, mulr, muli, banr, bani, borr, bori,
    setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr,
]
instructions = {f.__name__: f for f in instructions }
register = [0 for _ in range(6)]

ip = int(program.pop(0).split()[1])
i = 0

while 0 <= register[ip] < len(program):
    if register[ip] == 28:
        print(register[3])
        break

    line = program[register[ip]].split() 

    opcode = line[0]
    a, b, c =  mapInt(line[1:])
    register[c] = instructions[opcode](register, a, b)

    register[ip] += 1
