from collections import defaultdict

with open('3.txt', 'r') as f:
    inp = f.readlines()

coords = defaultdict(list)
elves = []

for elf in inp:
    elf = elf.split()

    eid = elf[0].strip()
    elves.append(eid)

    x, y = map(int, elf[2][:-1].split(','))
    l, h = map(int, elf[3].split('x'))
    
    for i in range(l):
        for j in range(h):
            coords['{},{}'.format(x + i, y + j)].append(eid)

for coord in coords:
    eids = coords[coord]
    
    if len(eids) > 1:
        for eid in eids:
            if eid in elves:
                elves.remove(eid)

print(elves)   