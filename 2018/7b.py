from collections import defaultdict

with open('7.txt', 'r') as f:
    inp = f.readlines()

nodes = []
locks = defaultdict(list)

for inst in inp:
    inst = inst.split()
    
    nodes.append(inst[1])
    locks[inst[7]].append(inst[1])

free = list(nodes - locks.keys())
free.sort(reverse = True)

ticks = {}
for n in list(locks.keys()) + free:
    ticks[n] = ord(n) - 4

workers = ['' for i in range(5)]

t = 0
while sum(ticks.values()):
    for i, w in enumerate(workers):
        if w:
            ticks[w] -= 1

            if ticks[w] == 0:
                workers[i] = ''

                for l in locks:
                    if w in locks[l]:
                        locks[l].remove(w)

                        if not locks[l]:
                            free.append(l)

                free.sort(reverse = True)
   
    for i, w in enumerate(workers):
        if not w:
            if free:
                workers[i] = free.pop()

    t += 1

print(t-1)