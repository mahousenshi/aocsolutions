inp = 170641

rec = [3, 7]
a, b = 0, 1

for i in range(inp + 10):
    new = str(rec[a] + rec[b])
    rec += list(map(int, new))

    a += 1 + rec[a]
    a %= len(rec)

    b += 1 + rec[b]
    b %= len(rec)

print(''.join(map(str, rec[inp:inp+10])))