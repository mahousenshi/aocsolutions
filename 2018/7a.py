from collections import defaultdict

with open('7.txt', 'r') as f:
    inp = f.readlines()

nodes = []
locks = defaultdict(list)

for inst in inp:
    inst = inst.split()
    
    nodes.append(inst[1])
    locks[inst[7]].append(inst[1])

path = ''

free = list(nodes - locks.keys())
free.sort(reverse = True)

while free:
    node = free.pop()
    path += node

    for l in locks:
        if node in locks[l]:
            locks[l].remove(node)

            if not locks[l]:
                free.append(l)

    free.sort(reverse = True)

print(path)