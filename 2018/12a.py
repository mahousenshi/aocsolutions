with open('12.txt', 'r') as f:
    inp = [s.strip() for s in f.readlines()]

pat = '##.......#.######.##..#...#.#.#..#...#..####..#.##...#....#...##..#..#.##.##.###.##.#.......###....#'

rules = {s.split(' => ')[0]: s.split(' => ')[1] for s in inp}

k = 0

for i in range(20):
    pat = "...." + pat + "...."
    npat = ''

    for n in range(2, len(pat) - 2):
        npat += rules[pat[n-2:n+3]]

    pat = npat
    k += 1

s = 0
for i, c in enumerate(pat):

    if c == '#':
        s += i - 2*k
    
print(s)
