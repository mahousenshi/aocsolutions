from statistics import mean

with open('7.txt', 'r') as f:
    pos = [int(n) for n in f.readline().split(',')]

m = int(mean(pos))

print(sum([abs(n-m)*(abs(n-m)+1)//2 for n in pos]))