

words = 'acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab'.split()
words.sort(key=len)

segments = {'a': '', 'b': '', 'c': '', 'd': '', 'e': '', 'f': '', 'g': ''}

'''
words[0] = 1
words[1] = 7
words[2] = 3
words[3] = 2 3 5
words[4] = 2 3 5
words[5] = 2 3 5
words[6] = 0 6 9
words[7] = 0 6 9
words[8] = 0 6 9
words[9] = 8
'''

# a
for c in words[1]:
    if c not in words[0]:
        segments['a'] += c

# b
for c in words[2]:
    if c not in words[1]:
        segments['b'] += c

print(segments)

    


'ab', 'dab', 'eafb'