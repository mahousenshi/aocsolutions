with open('2.txt', 'r') as f:
    commands = [c.split() for c in f.readlines()]

z = 0
x = 0
aim = 0

for dir, units in commands:
    units = int(units)

    if dir == 'down':
        aim += units
    elif dir == 'up':
        aim -= units
    else:
        x += units
        z += aim * units

print(x*z)