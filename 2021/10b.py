from statistics import median

with open('10.txt', 'r') as f:
    lines = [l.strip() for l in f.readlines()]

closure = {'{': '}', '[': ']', '(': ')',  '<': '>'}
points = {')': 1, ']': 2, '}': 3,  '>': 4}
scores = []

for line in lines:
    open = []
    score = 0

    for c in line:
        if c in '([{<':
            open.append(c)
        
        else:
            if c != closure[open[-1]]:
                break

            else:
                open.pop()
    else:
        for c in reversed(open):
            score = 5 * score + points[closure[c]]
        
        scores.append(score)

print(median(scores))
