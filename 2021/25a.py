from copy import deepcopy

with open('25.txt', 'r') as f:
    grid = [list(l.strip()) for l in f.readlines()]

h = len(grid)
w = len(grid[0])

step = 0
while True:
    f = 1
    new_grid = deepcopy(grid)

    for x, line in enumerate(grid):
        for y, c in enumerate(line):

            if c == '>':
                if y+1 == w:
                    if line[0] == '.':
                        new_grid[x][y] = '.'
                        new_grid[x][0] = '>'
                        f = 0
                    else:
                        new_grid[x][y] = '>'

                else:
                    if line[y+1] == '.':
                        new_grid[x][y] = '.'
                        new_grid[x][y+1] = '>'
                        f = 0
                    else:
                        new_grid[x][y] = '>'

    grid = deepcopy(new_grid)

    for x, line in enumerate(grid):
        for y, c in enumerate(line):

            if c == 'v':
                if x+1 == h:
                    if grid[0][y] == '.':
                        new_grid[x][y] = '.'
                        new_grid[0][y] = 'v'
                        f = 0
                    else:
                        new_grid[x][y] = 'v'

                else:
                    if grid[x+1][y] == '.':
                        new_grid[x][y] = '.'
                        new_grid[x+1][y] = 'v'
                        f = 0
                    else:
                        new_grid[x][y] = 'v'

    grid = new_grid
    step += 1

    if f:
        print(step)
        break