import re

with open('17.txt', 'r') as f:
    xa, xb, ya, yb = [int(n) for n in re.findall(r'\d+', f.readline().strip())]

ya *= -1
yb *= -1

target = [(x, y) for x in range(xa, xb+1) for y in range(ya, yb+1)]

points = 0
for x in range(0, xb+1):
    for y in range(ya, 500):
        px, py = 0, 0 
        vx, vy = x, y

        lhmax = 0
        while True:
            px += vx
            py += vy

            # End
            if px > xb:
                break

            if ya > py:
                break

            if (px, py) in target:
                points += 1
                break

            #x
            if vx > 0:
                vx -= 1

            if vx < 0:
                vx += 1

            #y
            vy -= 1

print(points)