from statistics import median

with open('7.txt', 'r') as f:
    pos = [int(n) for n in f.readline().split(',')]

m = int(median(pos))

print(sum([abs(n-m) for n in pos]))