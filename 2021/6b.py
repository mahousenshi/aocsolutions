from collections import Counter

def reproduce(fish, day):
    fish += 1
    day -= 9

    while day > 0:
        fish = reproduce(fish, day)
        day -= 7
    
    return fish

with open('6.txt', 'r') as f:
    school = Counter(f.readline().split(','))

laternfish = 0

for day in school:
    print(day)
    laternfish += reproduce(0, 256+(9-int(day))) * school[day]

print(laternfish)