class Cave:
    def __init__(self, name):
        self.name = name
        self.links = []

    def issmall(self):
        if self.name.islower():
            return True

        return False

    def __repr__(self) -> str:
        return self.name

def caveWalk(cave, walk):
    walk.append(cave.name)

    if cave.name == 'end':
        ways.append(walk)
        print(','.join(walk))

    else:
        for link in cave.links:
            if link.issmall() and link.name in walk:
                continue

            caveWalk(link, walk)
            walk.pop()

with open('12.txt', 'r') as f:
    conns = [l.strip() for l in f.readlines()]

start = Cave('start')
caves = [start]

for conn in conns:
    tcaves = []
    for name in conn.split('-'):
        cave = next((cave for cave in caves if cave.name == name), None)

        if cave is None:
            cave = Cave(name)
            caves.append(cave)

        tcaves.append(cave)

    tcaves[0].links.append(tcaves[1])
    tcaves[1].links.append(tcaves[0])

ways = []
caveWalk(start, [])

print(len(ways))