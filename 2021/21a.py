import re
from collections import deque

with open('21.txt', 'r') as f:
    pos = [int(re.match(r'Player \d starting position: (\d)', l.strip()).group(1)) for l in f.readlines()]

score = [0, 0]
rolls = 0

dice = deque(list(range(1, 101)))

while True:
    npos = (pos[rolls%2] + dice[0] + dice[1] + dice[2]) % 10
    pos[rolls%2] = npos if npos else 10
    score[rolls%2] += pos[rolls%2]

    if score[rolls%2] >= 1000:

        print(score[(rolls+1)%2] * ((rolls+1)*3))
        break

    rolls += 1
    dice.rotate(-3)
