from collections import Counter
import re

with open('5.txt', 'r') as f:
    lines = f.readlines()

points = []

for line in lines:
    x1, y1, x2, y2 = [int(n) for n in re.findall(r'\d+', line)]

    dx = -1 if x2 - x1 < 0 else 1
    dy = -1 if y2 - y1 < 0 else 1

    if x1 == x2:
        for y in range(y1, y2+dy, dy):
            points.append(f'{x1},{y}')

    elif y1 == y2:
        for x in range(x1, x2+dx, dx):
            points.append(f'{x},{y1}')

print(sum([1 if v > 1 else 0 for v in Counter(points).values()]))