with open('3.txt', 'r') as f:
    reports = [l.strip() for l in f.readlines()]

def ones(l, p):
    c = 0

    for elem in l:
        c += elem[p] == '1'

    return c

def keep(l, p, k):
    new_l = []

    for e in l:
        if e[p] == k:
            new_l.append(e)

    return new_l

o2s = reports.copy()
p = 0
total = len(o2s)
while total > 1:
    c = ones(o2s, p)

    if c >= total/2:
        k = '1'
    else:
        k = '0'

    o2s = keep(o2s, p, k)

    total = len(o2s)
    p += 1

co2s = reports.copy()
p = 0
total = len(co2s)
while total > 1:
    c = ones(co2s, p)

    if c < total/2:
        k = '1'
    else:
        k = '0'

    co2s = keep(co2s, p, k)

    total = len(co2s)
    p += 1

print(int(o2s[0], 2) * int(co2s[0], 2))