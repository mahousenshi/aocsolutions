with open('4.txt', 'r') as f:
    lines = f.readlines()
lines.append('\n')

calls = [int(n) for n in lines[0].split(',')]

cards = []

f = 0
for line in lines[1:]:
    line = [int(n) for n in line.split()]

    if line:
        card['numbers'] += line
        card['rows'].append(line)

    else:
        if f:
            cards.append(card)
        f = 1

        card = {
            'numbers': [],
            'rows': [],
            'bingoed': 0
        }

# Calls
n = 0
for call in calls:
    print(f'Calling: {call}!!')

    for i, card in enumerate(cards):
        if card['bingoed']:
            continue

        if call in card['numbers']:
            print(f'Is on Card {i+1}!!')
            card['numbers'].remove(call)

            for r in card['rows']:
                if call in r:
                    r[r.index(call)] = -1

        #Check Card
        if n > 4:
            colluns = [[], [], [], [], []]

            for j, row in enumerate(card['rows']):
                row = row.copy()

                for k, collun in enumerate(row):
                    colluns[k].append(collun + 1)
                    row[k] += 1

                if sum(row) == 0:
                    print('Bingo!!')
                    print(f'Row: {j+1}, Score: {sum(card["numbers"])*call}')
                    card['bingoed'] = 1
                    score = sum(card["numbers"])*call

            for j, collun in enumerate(colluns):
                if sum(collun) == 0:
                    print('Bingo!!')
                    print(f'Collun: {j+1}, Score: {sum(card["numbers"])*call}')
                    card['bingoed'] = 1
                    score = sum(card["numbers"])*call

        n += 1

print(score)