from collections import Counter

class Cave:
    def __init__(self, name):
        self.name = name
        self.links = []
        self.visited = 0

    def issmall(self):
        if self.name.islower():
            return True

        return False

    def __repr__(self) -> str:
        return f'{self.name}({self.visited})'

def caveWalk(cave, walk):
    walk.append(cave.name)

    if cave.name == 'end':
        ways.append(walk)
        print(','.join(walk))

    else:
        cave.visited += 1

        double = 0

        for c in caves:
            if c.issmall() and c.visited == 2:
                double += 1

        if cave.issmall() and double == 2:
            pass

        else:
            # Só tem 1 duplo ou 0 duplo na lista
            candidates = []

            for link in cave.links:
                if link.issmall():
                    if double == 0:
                        candidates.append(link)

                    else:
                        if link.visited == 0:
                            candidates.append(link)

            for link in candidates:
                caveWalk(link, walk)
                link.visited -= 1
                walk.pop()

with open('12.txt', 'r') as f:
    conns = [l.strip() for l in f.readlines()]

start = Cave('start')
caves = [start]

for conn in conns:
    tcaves = []
    for name in conn.split('-'):
        cave = next((cave for cave in caves if cave.name == name), None)

        if cave is None:
            cave = Cave(name)
            caves.append(cave)

        tcaves.append(cave)

    tcaves[0].links.append(tcaves[1])
    tcaves[1].links.append(tcaves[0])

ways = []
caveWalk(start, [])

print(len(ways))