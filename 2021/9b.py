from math import prod

def neighbor(x, y):
    neigh = []

    if x != 0:
        neigh.append((-1, 0))

    if y != 0:
        neigh.append((0, -1))

    if y != w-1:
        neigh.append((0, 1))

    if x != h-1:
        neigh.append((1, 0))

    return neigh

def search(basin, x, y):
    basin.append((x, y))

    for dx, dy in neighbor(x, y):
        if map[x+dx][y+dy] != '9' and (x+dx, y+dy) not in basin:
            basin = search(basin, x+dx, y+dy)

    return basin

with open('9.txt', 'r') as f:
    map = [list(l.strip()) for l in f.readlines()]

h = len(map)
w = len(map[0])

basins = []

for x in range(0, h):
    for y in range(0, w):
        if map[x][y] == '9':
            continue

        for dx, dy in neighbor(x, y):
            if int(map[x][y]) >= int(map[x+dx][y+dy]):
                break
        else:
            basins.append(len(search([], x, y)))

print(prod(sorted(basins, reverse=True)[:3]))