with open('3.txt', 'r') as f:
    reports = [l.strip() for l in f.readlines()]

reports_t = ['' for _ in range(len(reports[0]))]

for report in reports:
    for i, c in enumerate(report):
        reports_t[i] += c

total = len(reports_t[0])
gamma = ''

for report in reports_t:
    ones = report.count("1")

    if ones > total/2:
        gamma += '1'
    else:
        gamma += '0'

epsilon = ''.join(['0' if b == '1' else '1' for b in gamma])
print(int(gamma, 2) * int(epsilon, 2))