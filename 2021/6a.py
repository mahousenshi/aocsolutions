with open('6.txt', 'r') as f:
    school = [int(n) for n in f.readline().split(',')]


with open('6.txt', 'r') as f:
    school = [int(n) for n in f.readline().split(',')]

lanterfishes = 0

for fish in school:
    branch = [fish]

    for days in range(80):
        add = 0

        for i, _ in enumerate(branch):       
            branch[i] -= 1
        
            if branch[i] == -1:
                branch[i] = 6
                add += 1
    
        branch += [8] * add

    lanterfishes += len(branch)

print(lanterfishes)