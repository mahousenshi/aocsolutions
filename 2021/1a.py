with open('1.txt', 'r') as f:
    numbers = [int(n) for n in f.readlines()]

old = numbers[0]
c = 0

for n in numbers[1:]:
    c += n > old
    old = n

print(c)