with open('1.txt', 'r') as f:
    numbers = [int(n) for n in f.readlines()]

old = sum(numbers[0:3])
c = 0

for i in range(1, len(numbers)-2):
    this = sum(numbers[i:i+3])
    c += this > old
    old = this

print(c)