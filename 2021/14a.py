from collections import Counter
import re

polymer = ''
pairs = {}

with open('14.txt', 'r') as f:
    polymer = f.readline().strip()
    f.readline()

    for line in f.readlines():
        line = re.match(r'(\w+) -> (\w)', line.strip())
        pairs[line.group(1)] = line.group(2)

for step in range(10):
    new_polymer = ''

    for i in range(len(polymer)-1):
        pair = polymer[i:i+2]
        new_polymer += f'{pair[0]}{pairs[pair]}'

    new_polymer += polymer[-1]
    polymer = new_polymer

frequency = Counter(polymer).most_common()
print(frequency[0][1] - frequency[-1][1])