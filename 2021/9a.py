with open('9.txt', 'r') as f:
    map = [list(l.strip()) for l in f.readlines()]

h = len(map)
w = len(map[0])

risk = 0

for x in range(0, h):
    for y in range(0, w):
        if map[x][y] == '9':
            continue
        
        neigh = []

        if x != 0:
            neigh.append((-1, 0))

        if y != 0:
            neigh.append((0, -1))

        if y != w-1:
            neigh.append((0, 1))

        if x != h-1:
            neigh.append((1, 0))
    
        for dx, dy in neigh:
            if int(map[x][y]) >= int(map[x+dx][y+dy]):
                break
        else:
            risk += int(map[x][y]) + 1

print(risk)