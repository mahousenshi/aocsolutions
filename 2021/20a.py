with open('20.txt', 'r') as f:
    alg = f.readline().strip()
    f.readline()
    light = [(x, y) for x, line in enumerate(f.readlines()) for y, c in enumerate(line.strip()) if c == "#"]

xa, xb, ya, yb = 0, 100, 0, 100

for step in range(2):
    if step % 2:
        light += [(x, y) for x in range(xa-2, xb+3) for y in range(ya-2, yb+3) if xa > x or x > xb or ya > y or y > yb]

    light = [(x, y) for x in range(xa-1, xb+2) for y in range(ya-1, yb+2) if alg[int(''.join(['1' if (x+dx, y+dy) in light else '0' for dx, dy in [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0,0), (0, 1), (1, -1), (1, 0), (1, 1)]]), 2)] == "#"]

    xa, xb, ya, yb = xa-1, xb+1, ya-1, yb+1

print(len(light))