with open('10t.txt', 'r') as f:
    lines = [l.strip() for l in f.readlines()]

closure = {'{': '}', '[': ']', '(': ')',  '<': '>'}
points = {')': 3, ']': 57, '}': 1197,  '>': 25137}
score = 0

for line in lines:
    open = []

    for c in line:
        if c in '([{<':
            open.append(c)
        
        else:
            if c != closure[open[-1]]:
                score += points[c]
                break
            
            else:
                open.pop()

print(score)

