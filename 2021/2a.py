with open('2.txt', 'r') as f:
    commands = [c.split() for c in f.readlines()]

z = 0
x = 0

for dir, units in commands:
    units = int(units)

    if dir == 'down':
        z += units
    elif dir == 'up':
        z -= units
    else:
        x += units

print(x*z)