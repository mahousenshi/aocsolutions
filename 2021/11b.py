def neighbor(x, y):
    neigh = []

    if x != 0:
        if y != 0:
            neigh.append((-1, -1))

        neigh.append((-1, 0))

        if y != w-1:
            neigh.append((-1, 1))

    if y != 0:
        neigh.append((0, -1))

    if y != w-1:
        neigh.append((0, 1))

    if x != h-1:
        if y != 0:
            neigh.append((1, -1))

        neigh.append((1, 0))

        if y != w-1:
            neigh.append((1, 1))

    return neigh

def iluminate(map, x, y):
    map[x][y] = 0

    for dx, dy in neighbor(x, y):
        if map[x+dx][y+dy]:
            map[x+dx][y+dy] += 1

            if map[x+dx][y+dy] > 9:
                iluminate(map, x+dx, y+dy) 

with open('11.txt', 'r') as f:
    map = [list(l.strip()) for l in f.readlines()]

h, w = 10, 10

for x in range(0, h):
    for y in range(0, w):
        map[x][y] = int(map[x][y])

step = 0

while True:
    for x in range(0, h):
        for y in range(0, w):
            map[x][y] += 1
    
    for x in range(0, h):
        for y in range(0, w):
            if map[x][y] > 9:
                iluminate(map, x, y)
   
    step += 1
    if sum([sum(line) for line in map]) == 0:
        break

print(step)