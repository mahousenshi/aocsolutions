import re

def pointsPrint(points):
    w, h = 0, 0
    for point in points:
        w = max(point[0], w)
        h = max(point[1], h)

    for y in range(h+1):
        line = ''
        for x in range(w+1):
            if [x, y] in points:
                line += '#'
            else:
                line += '.'

        print(line)

points = []
folds = []

with open('13t.txt', 'r') as f:
    flag = 1
    for line in f.readlines():
        if len(line) == 1:
            flag = 0
            continue
    
        line = line.strip()

        if flag:
            x, y = line.split(',')
            points.append([int(x), int(y)])

        else:
            line = re.match(r'fold along (\w)=(\d+)', line)
            folds.append([line.group(1), int(line.group(2))])

axis = 0 if folds[0][0] == 'x' else 1
value = folds[0][1]

for point in points:
    if point[axis] > value:
        point[axis] = 2*value - point[axis]

npoints = []

for point in points:
    if point not in npoints:
        npoints.append(point)

print(len(npoints))