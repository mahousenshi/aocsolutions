def neighbor(x, y):
    neigh = []

    if x != 0:
        neigh.append((-1, 0))

    if y != 0:
        neigh.append((0, -1))

    if y != w-1:
        neigh.append((0, 1))

    if x != h-1:
        neigh.append((1, 0))

    return neigh

cave = []
with open('15t.txt', 'r') as f:
    for l in f.readlines():
        cave.append([int(n) for n in list(l.strip())])

h = len(cave)
w = len(cave[0])
print(h, w)

cost = 0
mincost = 999999999999999999999999999999999999999999999999999
path = [(0, 0)]

while path:
    x, y = path[-1]
    #print(path)

    for dx, dy in neighbor(x, y):
        if (x+dx, y+dy) not in path:
            cost = sum([cave[xx][yy] for xx, yy in path] + cave[x][y])


            if x+dx == w-1 and y+dy == h-1:
                #print("END")
                
                mincost = min(mincost, cost)
            else:
                path.append((x+dx, y+dy))

print(mincost)


