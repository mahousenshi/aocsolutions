with open('18.txt', 'r') as f:
    lines = [s.strip() for s in f.readlines()]

walk = {
    'U': (-1, 0),
    'R': (0, 1),
    'D': (1, 0),
    'L': (0, -1)
}

border = []
x, y = 0, 0

for line in lines:
    d, steps, _ = line.split()

    dx, dy = walk[d]

    for i in range(int(steps)):
        x += dx
        y += dy
        border.append((x, y))

minx, miny, maxx, maxy = 0, 0, 0, 0

for x, y in border:
    minx = min(minx, x)
    miny = min(miny, y)
    maxx = max(maxx, x)
    maxy = max(maxy, y)

inside = [(-251, 29)]  # non generic
#inside = [(1, 1)]  # non generic
flood = []

while inside:
    x, y = inside.pop()

    flood.append((x, y))

    for d in walk:
        dx, dy = walk[d]

        if (x+dx, y+dy) not in border and (x+dx, y+dy) not in flood and (x+dx, y+dy) not in inside:
            inside.append((x+dx, y+dy))

    #print(inside)

print(len(flood) + len(border))
