from re import findall

with open('19.txt', 'r') as f:
    lines = [s.strip() for s in f.readlines()]

workflows = {}
flag = 1
parts = []

for line in lines:
    if not line:
        flag = 0
        continue

    if flag:
        name = line.split('{')[0]
        rules = line.split('{')[1].replace('}', '')

        workflows[name] = rules

        if len(name) == 1:
            print(name)

    else:
        parts.append({p: int(n) for p, n in zip(['x', 'm', 'a', 's'], findall(r'\d+', line.replace('{', '').replace('}', '')))})

values = []
for part in parts:
    rule = 'in'
    while rule not in ['A', 'R']:
        rule = workflows[rule]

        while ':' in rule:
            condition, consquences = rule.split(':', maxsplit=1)
            piece = condition[0]
            operator = condition[1]
            qtd = int(condition[2:])
            consquences = consquences.split(',', maxsplit=1)

            if operator == '>':
                if part[piece] > qtd:
                    rule = consquences[0]
                else:
                    rule = consquences[1]
            else:
                if part[piece] < qtd:
                    rule = consquences[0]
                else:
                    rule = consquences[1]

    if rule == 'A':
        values.append(sum(part.values()))

print(sum(values))
