from re import finditer

with open('3.txt', 'r') as f:
    schematic = [s.strip() for s in f.readlines()]

ends = len(schematic)

gears_ratio = []

for i, line in enumerate(schematic):
    for gear in finditer(r'\*', line):
        pos = gear.span()[0]

        numbers = []

        if i != 0:
            for number in finditer(r'\d+', schematic[i-1]):
                begin, end = number.span()

                if begin-1 <= pos <= end:
                    numbers.append(int(number[0]))

        for number in finditer(r'\d+', schematic[i]):
            begin, end = number.span()

            if end == pos or begin-1 == pos:
                numbers.append(int(number[0]))

        if i != ends-1:
            for number in finditer(r'\d+', schematic[i+1]):
                begin, end = number.span()

                if begin-1 <= pos <= end:
                    numbers.append(int(number[0]))

        if len(numbers) == 2:
            gears_ratio.append(numbers[0] * numbers[1])

print(sum(gears_ratio))
