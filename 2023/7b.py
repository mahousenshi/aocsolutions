from collections import Counter


def kind(hand):
    hand = Counter(hand)

    if 'J' in hand:
        if hand['J'] == 5:
            return 'g'

        jokers = hand['J']
        del hand['J']

        card = hand.most_common()[0][0]
        hand[card] += jokers

    'Five of a kind'
    if 5 in hand.values():
        return 'g'

    'Four of a kind'
    if 4 in hand.values():
        return 'f'

    'Full house'
    if 3 in hand.values() and len(hand) == 2:
        return 'e'

    'Three of a kind'
    if 3 in hand.values():
        return 'd'

    'Two pair'
    if 2 in hand.values() and len(hand) == 3:
        return 'c'

    'One pair'
    if 2 in hand.values():
        return 'b'

    'High card'
    return 'a'


strengths = {
    'A': 'm',
    'K': 'l',
    'Q': 'k',
    'T': 'j',
    '9': 'i',
    '8': 'h',
    '7': 'g',
    '6': 'f',
    '5': 'e',
    '4': 'd',
    '3': 'c',
    '2': 'b',
    'J': 'a',
}

with open('7.txt', 'r') as f:
    lines = [s.strip() for s in f.readlines()]

deck = []

for line in lines:
    hand, bid = line.split()

    deck.append({
        'hand': hand,
        'bid': int(bid),
        'order': kind(hand) + ''.join(strengths[card] for card in hand),
    })

print(sum([i*card['bid'] for i, card in enumerate(sorted(deck, key=lambda d: d['order']), start=1)]))
