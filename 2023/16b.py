def valid(x, y, d):
    if x < 0 or y > leny or x > lenx or y < 0:
        return 0

    return 1


def energized(i, j, d):
    rays = [(i, j, d)]
    visited = []

    print((i, j, d))

    while rays:
        x, y, d = rays.pop()

        if (x, y, d) in visited:
            continue

        visited.append((x, y, d))

        dx, dy = nextd[d]

        if grid[x][y] == '.' and valid(x+dx, y+dy, d):
            rays.append((x+dx, y+dy, d))

        if grid[x][y] == '/':
            if d == 0 and valid(x, y+1, 1):
                rays.append((x, y+1, 1))

            if d == 1 and valid(x-1, y, 0):
                rays.append((x-1, y, 0))

            if d == 2 and valid(x, y-1, 3):
                rays.append((x, y-1, 3))

            if d == 3 and valid(x+1, y, 2):
                rays.append((x+1, y, 2))

        if grid[x][y] == '\\':
            if d == 0 and valid(x, y-1, 3):
                rays.append((x, y-1, 3))

            if d == 1 and valid(x+1, y, 2):
                rays.append((x+1, y, 2))

            if d == 2 and valid(x, y+1, 1):
                rays.append((x, y+1, 1))

            if d == 3 and valid(x-1, y, 0):
                rays.append((x-1, y, 0))

        if grid[x][y] == '-':
            if d == 0 or d == 2:
                if valid(x, y+1, 1):
                    rays.append((x, y+1, 1))

                if valid(x, y-1, 3):
                    rays.append((x, y-1, 3))
            else:
                if valid(x+dx, y+dy, d):
                    rays.append((x+dx, y+dy, d))

        if grid[x][y] == '|':
            if d == 1 or d == 3:
                if valid(x-1, y, 0):
                    rays.append((x-1, y, 0))

                if valid(x+1, y, 2):
                    rays.append((x+1, y, 2))
            else:
                if valid(x+dx, y+dy, d):
                    rays.append((x+dx, y+dy, d))

    uniques = []
    for x, y, _ in visited:
        if (x, y) not in uniques:
            uniques.append((x, y))

    return len(uniques)


with open('16.txt', 'r') as f:
    grid = [list(s.strip()) for s in f.readlines()]

nextd = [(-1, 0), (0, 1), (1, 0), (0, -1)]
lenx = len(grid)-1
leny = len(grid[0])-1

grids = []
for i in range(len(grid)):
    grids.append(energized(i, 0, 1))
    grids.append(energized(i, len(grid[0])-1, 3))

for j in range(len(grid[0])):
    grids.append(energized(0, j, 2))
    grids.append(energized(len(grid)-1, j, 0))

print(max(grids))
