from re import findall

with open('12.txt', 'r') as f:
    lines = [s.strip() for s in f.readlines()]

arrangements = []

for line in lines:
    spring = line.split()[0]
    groups = line.split()[1]

    unks = spring.count('?')
    valid = 0

    for n in range(2**unks):
        var = f'{n:>0{unks}b}'.replace('0', '.').replace('1', '#')

        springt = spring[::]
        i = 0
        while i != unks:
            springt = springt.replace('?', var[i], 1)
            i += 1

        if groups == ','.join([str(len(g)) for g in findall(r'#+', springt)]):
            valid += 1

    arrangements.append(valid)

print(sum(arrangements))
