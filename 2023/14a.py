from re import finditer

with open('14.txt', 'r') as f:
    grid = [s.strip() for s in f.readlines()]

hgrid = [''.join([grid[a][b] for a in range(len(grid))]) for b in range(len(grid[0]))]
ngrid = []

for line in hgrid:
    start = 0
    nline = ''

    for end in [cube.span()[0] for cube in finditer(r'#', line)]+[len(line)]:
        if start:
            nline += '#'

        nline += 'O' * line.count('O', start, end)
        nline += '.' * line.count('.', start, end)
        start = end

    ngrid.append(nline)

print(sum(sum(i+1 for i, ch in enumerate(line[::-1]) if ch == 'O') for line in ngrid))
