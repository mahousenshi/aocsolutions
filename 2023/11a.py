from itertools import combinations

with open('11.txt', 'r') as f:
    grid = [s.strip() for s in f.readlines()]

tgrid = [''.join([grid[a][b] for a in range(len(grid))]) for b in range(len(grid[0]))]

galaxies = [(i, j) for i, line in enumerate(grid) for j, ch in enumerate(line) if ch == "#"]

distancies = []

for a, b in combinations(galaxies, 2):
    x1, y1 = a
    x2, y2 = b

    for line in grid[min(x1, x2):max(x1, x2)]:
        if '#' not in line:
            if x1 > x2:
                x1 += 1
            else:
                x2 += 1

    for line in tgrid[min(y1, y2):max(y1, y2)]:
        if '#' not in line:
            if y1 > y2:
                y1 += 1
            else:
                y2 += 1

    distancies.append(abs(x1 - x2) + abs(y1 - y2))

print(sum(distancies))
