with open('4.txt', 'r') as f:
    cards = [s.strip() for s in f.readlines()]

points = []

for card in cards:
    numbers = card.split('|')[1].split()
    winning = card.split(':')[1].split('|')[0].split()

    worth = len([w for w in winning if w in numbers])

    if worth:
        points.append(2**(worth-1))
    else:
        points.append(0)

print(sum(points))
