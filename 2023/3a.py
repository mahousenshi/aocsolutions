from re import finditer

with open('3.txt', 'r') as f:
    schematic = [s.strip() for s in f.readlines()]

numbers = []

ends = len(schematic)
endl = len(schematic[0])

for i, line in enumerate(schematic):
    for number in finditer(r'\d+', line):
        begin, end = number.span()

        begint = max(begin-1, 0)
        endt = min(end+1, endl)

        part_number = 0

        if i != 0 and len(schematic[i-1][begint:endt].replace('.', '')):
            part_number = 1

        if begin != 0 and line[begint] != '.':
            part_number = 1

        if endt != end and line[end] != '.':
            part_number = 1

        if i != ends-1 and len(schematic[i+1][begint:endt].replace('.', '')):
            part_number = 1

        if part_number:
            numbers.append(int(number[0]))

print(sum(numbers))
