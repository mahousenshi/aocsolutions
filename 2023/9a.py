with open('9.txt', 'r') as f:
    lines = [s.strip() for s in f.readlines()]


def next_value(sequence):
    new = []

    a = sequence[0]

    for b in sequence[1:]:
        new.append(b-a)
        a = b

    if any(new):
        return sequence[-1] + next_value(new)

    return sequence[-1]


print(sum([next_value(list(map(int, line.split()))) for line in lines]))
