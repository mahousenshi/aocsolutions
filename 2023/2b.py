from re import findall

with open('2.txt', 'r') as f:
    lines = [s.strip() for s in f.readlines()]

power = []

for line in lines:
    die = {
        'blue': 0,
        'green': 0,
        'red': 0
    }

    for die_set in line.split(":")[1].split(";"):
        for n, color in findall(r'(\d+) (\w*)', die_set):
            die[color] = max(die[color], int(n))

    prod = 1
    for color in die:
        prod *= die[color]

    power.append(prod)

print(sum(power))
