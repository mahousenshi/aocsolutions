from re import findall

with open('5.txt', 'r') as f:
    lines = [s.strip() for s in f.readlines()]

seeds = list(map(int, findall(r'\d+', lines[0])))

for line in lines[2:]:
    if "map:" in line:
        maps = []
        continue

    if line:
        maps.append(list(map(int, line.split())))
    else:
        for i, seed in enumerate(seeds):
            for d, s, r in maps:
                if s <= seed and s+r-1 >= seed:
                    seeds[i] = d + (seed-s)
                    break

for i, seed in enumerate(seeds):
    for d, s, r in maps:
        if s <= seed and s+r-1 >= seed:
            seeds[i] = d + (seed-s)

print(min(seeds))
