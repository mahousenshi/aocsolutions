with open('10.txt', 'r') as f:
    grid = [list(s.strip()) for s in f.readlines()]

x, y = 0, 0

walk = [
    {
        '7': [0, -1, 3],
        '|': [-1, 0, 0],
        'F': [0, 1, 1],
    },
    {
        'J': [-1, 0, 0],
        '-': [0, 1, 1],
        '7': [1, 0, 2],
    },
    {
        'L': [0, 1, 1],
        '|': [1, 0, 2],
        'J': [0, -1, 3],
    },
    {
        'F': [1, 0, 2],
        '-': [0, -1, 3],
        'L': [-1, 0, 0],
    },
]

for i, line in enumerate(grid):
    for j, ch in enumerate(line):
        if ch == 'S':
            x, y = i, j
            break

    if any([x, y]):
        break

if x != 0 and grid[x-1][y] in ['7', '|', 'F']:
    sx, sy, d = x-1, y, 0

if y != len(grid[0])-1 and grid[x][y+1] in ['7', '|', 'F']:
    sx, sy, d = x, y+1, 1

if x != len(grid)-1 and grid[x+1][y] in ['L', '|', 'J']:
    sx, sy, d = x+1, y, 2

if y != 0 and grid[x][y-1] in ['F', '-', 'L']:
    sx, sy, d = x, y-1, 3

steps = 1
while grid[sx][sy] != 'S':
    dx, dy, d = walk[d][grid[sx][sy]]

    sx += dx
    sy += dy

    steps += 1

print(steps//2)
