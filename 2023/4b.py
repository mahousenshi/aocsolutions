with open('4.txt', 'r') as f:
    cards = [s.strip() for s in f.readlines()]

qty = [1]*len(cards)

for i, card in enumerate(cards):
    numbers = card.split('|')[1].split()
    winning = card.split(':')[1].split('|')[0].split()

    for j in range(len([w for w in winning if w in numbers])):
        qty[i+j+1] += qty[i]

print(sum(qty))
