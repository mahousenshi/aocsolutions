from functools import reduce

with open('15.txt', 'r') as f:
    strings = f.readline().strip().split(',')

print(sum(reduce(lambda a, b: ((b+a)*17) % 256, [0]+list(map(ord, string))) for string in strings))
