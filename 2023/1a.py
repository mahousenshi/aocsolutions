from re import findall

with open('1.txt', 'r') as f:
    lines = [s.strip() for s in f.readlines()]

calibration = []

for line in lines:
    digits = list(map(int, findall(r'\d', line)))
    calibration.append(digits[0] * 10 + digits[-1])

print(sum(calibration))
