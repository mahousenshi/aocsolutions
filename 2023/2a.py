from re import match, findall

with open('2.txt', 'r') as f:
    lines = [s.strip() for s in f.readlines()]

ids = []

for line in lines:
    game_id = match(r'Game (\d+)', line)[1]

    for die_set in line.split(":")[1].split(";"):
        die = {
            'blue': 0,
            'green': 0,
            'red': 0
        }

        for n, color in findall(r'(\d+) (\w*)', die_set):
            die[color] += int(n)

        if sum(die.values()) > 39:
            break

        if die['blue'] > 14 or die['green'] > 13 or die['red'] > 12:
            break
    else:
        ids.append(int(game_id))

print(sum(ids))
