with open('21.txt', 'r') as f:
    grid = [s.strip() for s in f.readlines()]

walk = [(-1, 0), (0, 1), (1, 0), (0, -1)]

x, y = 0, 0
for i, line in enumerate(grid):
    for j, ch in enumerate(line):
        if ch == 'S':
            grid[i] = line.replace('S', '.')
            x, y = i, j
            break

    if any([x, y]):
        break

plots = [(x, y)]

for _ in range(64):
    visited = []

    while plots:
        x, y = plots.pop()

        for d in walk:
            dx, dy = d

            if grid[x+dx][y+dy] == '.' and (x+dx, y+dy) not in visited:
                visited.append((x+dx, y+dy))

    plots = visited

print(len(plots))
