from itertools import cycle
from re import findall

with open('8.txt', 'r') as f:
    lines = [s.strip() for s in f.readlines()]

nodes = {}

for line in lines[2:]:
    node, left, right = findall(r'\w+', line)

    nodes[node] = {
        'L':  left,
        'R':  right,
    }

steps = 0
node = 'AAA'

for path in cycle(lines[0]):
    node = nodes[node][path]
    steps += 1

    if node == 'ZZZ':
        break

print(steps)
