from collections import Counter


def kind(hand):
    hand = Counter(hand)

    'Five of a kind'
    if 5 in hand.values():
        return 'g'

    'Four of a kind'
    if 4 in hand.values():
        return 'f'

    'Full house'
    if 3 in hand.values() and len(hand) == 2:
        return 'e'

    'Three of a kind'
    if 3 in hand.values():
        return 'd'

    'Two pair'
    if 2 in hand.values() and len(hand) == 3:
        return 'c'

    'One pair'
    if 2 in hand.values():
        return 'b'

    'High card'
    return 'a'


strengths = {
    'A': 'm',
    'K': 'l',
    'Q': 'k',
    'J': 'j',
    'T': 'i',
    '9': 'h',
    '8': 'g',
    '7': 'f',
    '6': 'e',
    '5': 'd',
    '4': 'c',
    '3': 'b',
    '2': 'a',
}

with open('7.txt', 'r') as f:
    lines = [s.strip() for s in f.readlines()]

deck = []

for line in lines:
    hand, bid = line.split()

    deck.append({
        'hand': hand,
        'bid': int(bid),
        'order': kind(hand) + ''.join(strengths[card] for card in hand),
    })

print(sum([i*card['bid'] for i, card in enumerate(sorted(deck, key=lambda d: d['order']), start=1)]))
