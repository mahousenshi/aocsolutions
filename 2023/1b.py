from re import match

words_values = {
    'one': 1,
    'two': 2,
    'three': 3,
    'four': 4,
    'five': 5,
    'six': 6,
    'seven': 7,
    'eight': 8,
    'nine': 9
}

with open('1.txt', 'r') as f:
    lines = [s.strip() for s in f.readlines()]

re = r'\d|one|two|three|four|five|six|seven|eight|nine'

calibration = []

for line in lines:
    digits = []

    for i in range(len(line)):
        digit = match(re, line[i:])

        if digit is not None:
            if len(digit[0]) == 1:
                digits.append(int(digit[0]))
            else:
                digits.append(words_values[digit[0]])

    calibration.append(digits[0] * 10 + digits[-1])

print(sum(calibration))
