from collections import defaultdict
from functools import reduce


def hashmap(string):
    return reduce(lambda a, b: ((b+a)*17) % 256, [0]+list(map(ord, string)))


def boxlenght(string):
    box, length = string.split('=')

    return hashmap(box), int(length), box


def find_symbol(symbol, box):
    for i, lens in enumerate(boxes[box]):
        if lens['symbol'] == symbol:
            return i

    return -1


with open('15.txt', 'r') as f:
    strings = f.readline().strip().split(',')

boxes = defaultdict(list)

for string in strings:
    if string.endswith('-'):
        symbol = string[:-1]
        box = hashmap(symbol)

        pos = find_symbol(symbol, box)

        if pos+1:
            boxes[box].pop(pos)
    else:
        box, power, symbol = boxlenght(string)

        pos = find_symbol(symbol, box)

        if pos+1:
            boxes[box][pos]['power'] = power
        else:
            boxes[box].append({
                'symbol': symbol,
                'power': power,
            })

values = []
for box in boxes:
    for i, slot in enumerate(boxes[box]):
        values.append((box+1)*(i+1)*slot['power'])

print(sum(values))

