from re import findall

with open('6.txt', 'r') as f:
    lines = [s.strip() for s in f.readlines()]

time = list(map(int, findall(r'\d+', lines[0])))
distance = list(map(int, findall(r'\d+', lines[1])))

p = 1
for win in [sum([1 for i in range(1, t+1) if (t-i)*i > d]) for t, d in zip(time, distance)]:
    p *= win

print(p)
