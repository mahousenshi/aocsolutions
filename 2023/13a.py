with open('13.txt', 'r') as f:
    lines = [s.strip() for s in f.readlines()] + ['']


def mirror(grid):
    for i in range(1, len(grid)):
        front = grid[:i]
        back = grid[i:]

        lfront = len(front)
        lback = len(back)

        if lback > lfront:
            back = grid[i:i+lfront]
        else:
            front = grid[i-lback:i]

        if front == back[::-1]:
            return i

    return 0


numbers = []
grid = []

for line in lines:
    if line:
        grid.append(line)

    else:
        vgrid = [''.join([grid[a][b] for a in range(len(grid))]) for b in range(len(grid[0]))]
        numbers.append(100*mirror(grid)+mirror(vgrid))
        grid = []

print(sum(numbers))
