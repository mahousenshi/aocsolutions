from re import findall

with open('6.txt', 'r') as f:
    lines = [s.strip() for s in f.readlines()]

time = int(''.join(findall(r'\d+', lines[0])))
distance = int(''.join(findall(r'\d+', lines[1])))

print(sum([1 for i in range(1, time+1) if (time-i)*i > distance]))
