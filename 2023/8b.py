from itertools import cycle
from re import findall
from math import lcm

with open('8.txt', 'r') as f:
    lines = [s.strip() for s in f.readlines()]

nodes = {}
a_nodes = []

for line in lines[2:]:
    node, left, right = findall(r'\w+', line)

    nodes[node] = {
        'L':  left,
        'R':  right,
    }

    if node.endswith('A'):
        a_nodes.append(node)

a_steps = []

for node in a_nodes:
    steps = 0

    for path in cycle(lines[0]):
        node = nodes[node][path]
        steps += 1

        if node.endswith('Z'):
            a_steps.append(steps)
            break

print(lcm(*a_steps))
